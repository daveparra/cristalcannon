// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_EndAnimTeleport.h"
#include "CC_WizzardLizard.h"
#include "Components/SkeletalMeshComponent.h"


void UCC_EndAnimTeleport::Notify(USkeletalMeshComponent * MeshComp, UAnimSequenceBase * Animation)
{
	Super::Notify(MeshComp, Animation);
	AActor* AnimationOwner = MeshComp->GetOwner();
	if (ACC_WizzardLizard* Character = Cast<ACC_WizzardLizard>(AnimationOwner))
	{
		Character->EndTeleporting = true;
	}
}
