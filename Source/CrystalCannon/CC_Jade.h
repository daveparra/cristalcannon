// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CC_Character.h"
#include "AKAudioEvent.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystem.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemComponent.h"
#include "CC_Jade.generated.h"


//class ACC_Jade_Primary;
/**
 * 
 */

UCLASS(config=Game)
class CRYSTALCANNON_API ACC_Jade : public ACC_Character
{
	GENERATED_BODY()
	
public:
	virtual void Tick(float DeltaTime) override;
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Abilities")
	UAkAudioEvent* atksound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Abilities")
	UAkAudioEvent* SecondSound;

	UPROPERTY(EditAnywhere, Category = "Abilities")
	UParticleSystem* PrimaryFiringParticle;

	UPROPERTY(EditAnywhere, Category = "Abilities")
	float PrimaryParticleScale = 10.f;

	UPROPERTY(EditAnywhere, Category = "Abilities")
	float AOERadius;

	UPROPERTY(EditAnywhere, Category = "Abilities")
	float AOEDamage;

	UPROPERTY(EditAnywhere, Category = "Abilities")
	float PulseInterval = 0.5;

	UPROPERTY(EditAnywhere, Category = "Abilities")
	float PulseCount = 4.f;
	
	UPROPERTY(EditAnywhere)
	UParticleSystem* ParticleFXRain;

	UPROPERTY()
	UParticleSystemComponent* ArrowRainParticle;

	FTimerHandle CallbackHandler;

	float Counter = 0;
	float PulseCounter = 0;
	bool PulseActivated = false;
protected:

	virtual void EnablePrimary() override;
	virtual void EnableSecondary() override;
	virtual void DisableSecondary() override;
	bool firstTime = false;
	void ApplyAOEDamage();
	void SpawnPrimary() override;
	void SpawnSecondary() override;
	void SpawnParticle(UParticleSystem* SpawningParticle);
	bool ShootOnce = false;
};
