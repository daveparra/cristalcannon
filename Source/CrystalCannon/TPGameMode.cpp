#include "TPGameMode.h"
#include "Engine.h"
#include "CC_CharacterExample.h"
#include "UObject/ConstructorHelpers.h"
#include "CC_CharacterExample.h"

ATPGameMode::ATPGameMode(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnObject(TEXT("/Game/DavidTest/DavidPlayer"));
	if (PlayerPawnObject.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnObject.Class;
	}
}

void ATPGameMode::StartPlay()
{
	Super::StartPlay();

	//StartMatch();

	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("HELLO WORLD"));
	}
}
