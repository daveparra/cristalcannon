// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CC_Unit.h"
#include "Components/StaticMeshComponent.h"
#include "TimerManager.h"
#include "TargetMarker.h"
#include "Components/SphereComponent.h"
#include "Runtime/UMG/Public/Components/WidgetComponent.h"
#include "CC_Character.generated.h"

/**
 * 
 */
class UParticleSystem;
class CC_Spawner;
class UUserWidget;

UCLASS()
class CRYSTALCANNON_API ACC_Character : public ACC_Unit
{
	GENERATED_BODY()


protected:

	bool PauseSpawned = false;

	UUserWidget* Pause;

	UCharacterMovementComponent* CharMovComp;
	
	APlayerController* PC;//PlayerController

	float SecondaryDelay = 2;

	UPROPERTY(EditAnywhere, Category = "Abilites")
	float SprintSpeed = 1000.f;

	UPROPERTY(EditAnywhere, Category = "Abilites")
	float PrimaryShortCooldown;

	UPROPERTY(EditAnywhere, Category = "Abilities")
	float ReviveRadius;

	UPROPERTY(EditAnywhere, Category = "Abilities")
	UStaticMeshComponent* AimDecal;

	UPROPERTY(EditAnywhere, Category = "Spawner")
	TSubclassOf<AActor> SpawnerClass;

	UPROPERTY(EditAnywhere, Category = "Abilities")
	TSubclassOf<AActor> ReviveFlag;

	UPROPERTY(EditAnywhere, Category = "Abilities")
	TSubclassOf<AActor> RespawnActor;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseLookUpRate;

	UPROPERTY(EditAnywhere, Category = "Abilites")
	float SlowValue;//Value by which the character is slowed
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Abilites")
	float RechargeRate = 15;

	UPROPERTY(EditAnywhere, Category = "Abilities")
	TSubclassOf<AActor> PrimaryAbility;

	UPROPERTY(EditAnywhere, Category = "Abilities")
		TSubclassOf<AActor> SecondaryAbility;

	UPROPERTY(EditAnywhere, Category = "Abilities")
		TSubclassOf<AActor> DumbActor;

	AActor* Dumb;

	UPROPERTY(EditAnywhere, Category = "Abilities")
	TSubclassOf<ATargetMarker> TargetMark;

	UPROPERTY(EditAnywhere, Category = "Abilities")
	TSubclassOf<UUserWidget> PauseMenu;

	UPROPERTY(EditAnywhere, Category = "Abilities")
	TSubclassOf<UUserWidget> ReviveWidget;

	UPROPERTY(EditAnywhere, Category = "Abilities")
	float FireDistance = 100.f;

	UPROPERTY(EditAnywhere, Category = "SlwoMo")
	float SlowMoDuration = 0.25;

	UPROPERTY(EditAnywhere, Category = "SlowMo")
	float SlowMoValue = 0.2;

	UPROPERTY(EditAnywhere, Category = "Abilities")
	int32 TapCounter = 10;


	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float side;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float forward;

	float DefaultWalkSpeed = 600.f;
	

	AActor* Primary;//Spawned Actor (primary ability)
	AActor* Secondary;//Spawned Actor (secondary ability)
	AActor* RevFlagPtr;//Spawned Actor (revive flag)
	AActor* ReviveCirclePtr;
	ACC_Character* LastRevivedChar;
	ATargetMarker* SpawnedMark;

	//On Death Character transform 
	FTransform OnDeathTransform;

public:

	ACC_Character();

	void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		float HoldTimer;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UArrowComponent* FirstTitty;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UArrowComponent* SecondTitty;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UArrowComponent* ThirdTitty;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UArrowComponent* MinLocation;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UArrowComponent* Shoot;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UArrowComponent* Charge;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UWidgetComponent* UIDisplay;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UWidgetComponent* ReviveDisplay;



	UPROPERTY(EditAnywhere, BlueprintReadWrite ,Category = "Abilites")
	float SecondaryCooldown;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float ChargeDistance = 1;

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	ACC_Character* Reviver;

	/** Called for side to side input */
	void MoveRight(float Value);
	void TurnAtRate(float Rate);
	void LookUpAtRate(float Rate);
	//Fire Primary Ability
	virtual void EnablePrimary();
	//Stop primary
	virtual void DisablePrimary();
	//Fire Secondary Ability               
	virtual void EnableSecondary();
	//Stop secondary
	virtual void DisableSecondary();

	virtual void SpawnPrimary();

	virtual void SpawnSecondary();

	UFUNCTION()
	void SpawnPause();

	UFUNCTION()
	void UnSpawnPause();

	UFUNCTION()
		void CallStepsForCharacter();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FootSteps")
	class UAkAudioEvent* footsteps;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FootSteps")
		FName Surface;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FootSteps")
		FName Character;

	void EnableSprint();

	void DisableSprint();

	void AttachSecondary();

	void ResetWorldTime();

	//Detaches secondary from player
	UFUNCTION(BlueprintCallable)
	void DetatchSecondary();
	//Revives all dead players in the overlap sphere
	virtual void Revive();
	
	void StartRevive();

	void EndRevive();

	virtual void DamageSelf();
	
	virtual void KillCharacter();

	virtual void ResetCharacter();

	void SpawnDeathFlag();
	//Apply Damage override
	virtual void ApplyDamage(float Damage) override;

	//Returns character to original movement speed
	void ResetSpeed(); 

	void SpawnMarker();

	void DestroyMark();

	void ApplyMarkerMovement();

	//Slow down movement speed of character (Speed/Value)
	void SlowCharacter(float Value);

	//Cooldowns
	virtual void PShortCooldownEnd();
	void SCooldownEnd();
	

	float XAxis = 0.0f;
	float YAxis = 0.0f;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float LXAxis = 0.0f;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float LYAxis = 0.0f;

	bool isFirstTitty = false;
	bool isSecondTitty = false;
	bool isThirdTitty = false;

	bool isMarking = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool inReviveRange = false;

	bool isReviving = false;

	float Value;
	float DTime;//Copy of DeltaTime
	int32 PlayerIndex;
	int32 TapCurrent = 0;

	UPROPERTY()
	UParticleSystemComponent* DeadParticle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float ReviveHoldDuration;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UParticleSystem* ParticleFX1;

	//variables for straffing and the movement
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		bool IsWalking;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		bool IsLeft;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		bool IsRight;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		bool IsBack;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		bool isShooting;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		bool isSprinting;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		bool isCharge;

	bool CanFirePrimary = true;
	bool isButtonPressed = false;
	bool CanFireSecondary = true;
	bool isTargeting = false;	
protected:
	//Setup controller mapping
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void PlayerReviveEvent(ACC_Character* Healer, ACC_Character* Revived);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void PlayerDeathEvent();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void MisfireEvent();

	FRotator FirstROt;
	bool Flag;
	bool stop1, stop2;
	FTimerHandle CallbackHandlerShort;
	FTimerHandle CallbackHandlerSecondary;
};
