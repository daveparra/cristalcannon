// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_Wheel.h"
#include "Runtime/Engine/Classes/GameFramework/RotatingMovementComponent.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
ACC_Wheel::ACC_Wheel()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->AttachTo(RootComponent);

	Movement = CreateDefaultSubobject<URotatingMovementComponent>(TEXT("RotatingMovement"));
	Movement->SetUpdatedComponent(RootComponent);
}

// Called when the game starts or when spawned
void ACC_Wheel::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ACC_Wheel::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

