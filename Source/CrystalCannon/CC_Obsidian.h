// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CC_Character.h"
#include "CC_Obsidian.generated.h"

/**
 *
 */
UCLASS()
class CRYSTALCANNON_API ACC_Obsidian : public ACC_Character
{
	GENERATED_BODY()
	
public:

	ACC_Obsidian();

	virtual void BeginPlay() override;
	//virtual void PShortCooldownEnd() override;

	UPROPERTY(EditAnywhere, Category = "Abilities")
	UParticleSystemComponent* BeamEmitterComponent;

	UPROPERTY(EditAnywhere, Category = "Abilities")
	UParticleSystem* BeamEmitterSystem;

	//UPROPERTY(EditAnywhere, Category = "Abilities")
	//float  ChainRadius = 400.f;
	//	
	//UPROPERTY(EditAnywhere, Category = "Abilities")
	//int32 Jumps = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Abilities")
		class UAkAudioEvent* atksound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Abilities")
		UAkAudioEvent* SecondSound;

	AActor* LightningTarget;//Raycast target

	FVector TargetLocation;

private:
	virtual void Tick(float DeltaTime) override;
	virtual void EnablePrimary() override;
	virtual void EnableSecondary() override;
	virtual void DisableSecondary() override;
	void SpawnSecondary() override;
	void SpawnPrimary() override;
	//Obsidian's attack and particle
	//void ShootRaycast();

	//void ShootParticleBeam(FVector Point1, FVector Point2);

	void CastOverlapSphere(FVector Location, int32 ChainTargets);
	TArray<AActor*> SortArray(TArray<AActor*> Actors);

};

