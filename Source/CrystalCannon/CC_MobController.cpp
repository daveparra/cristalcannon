// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_MobController.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/PawnSensingComponent.h."
#include "CC_MobEnemie.h"
#include "Components/ArrowComponent.h"
#include "Engine/GameEngine.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/NavigationSystem/Public/NavigationSystem.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "CC_Character.h"

ACC_MobController::ACC_MobController()
{
	PrimaryActorTick.bCanEverTick = true;
	PawnSensor = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("Pawn Sensor"));
	SetPerceptionComponent(*CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("AIPerception Component")));
	PawnSensor->SensingInterval = .25f; // 4 times per second
	PawnSensor->bOnlySensePlayers = false;
	PawnSensor->SetPeripheralVisionAngle(45.f);
	PawnSensor->SightRadius = 500.0f;

	PawnSensor->OnSeePawn.AddDynamic(this, &ACC_MobController::OnSeePawn);
	PawnSensor->OnHearNoise.AddDynamic(this, &ACC_MobController::OnHearNoise);
}

void ACC_MobController::GetRandomLocation()
{
	UNavigationSystemV1* NavSys = UNavigationSystemV1::GetCurrent(GetWorld());
	if (!NavSys)
	{
		return;
	}
	FVector Origin = GetPawn()->GetActorLocation();
	bool BSucces = NavSys->K2_GetRandomPointInNavigableRadius(GetWorld(), Origin, Result, MovementRadius);
}

void ACC_MobController::BeginPlay()
{
	Super::BeginPlay();
	//GetRandomLocation();

}

void ACC_MobController::Possess(APawn * Pawn)
{
	Super::Possess(Pawn);
}

void ACC_MobController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	ACC_MobEnemie* Character = Cast<ACC_MobEnemie>(GetPawn());
	if (Character->GetCurrentHealth() < 0.0f)
	{
		Character->SpawnDeathParticle();

		Character->Destroy();

		Destroy();
	}
	else
	{
		if (DistanceToPlayer > AISightRadius)
		{
			bIsPlayerDetected = false;
			MyPawn = nullptr;
			bNotGoing = false;
			Stop = true;
		}

		if (MyPawn != nullptr && bNotGoing)
		{
			//if(Cast<ACC_Character>(MyPawn))
			//{
				ACC_Character* myPawn = Cast<ACC_Character>(MyPawn);
				if (myPawn)
				{
					GoingToEnemie();
					TimerDamge -= DeltaSeconds;
					DistanceToPlayer = GetPawn()->GetDistanceTo(MyPawn);
					if (DistanceToPlayer < 120.0f && TimerDamge < 0.0f && Stop == false && myPawn->GetCurrentHealth() > 5.0f) {
						TimerDamge = DamageTimer;
						myPawn->ApplyDamage(Character->Damage);
					}
					else if (myPawn->GetCurrentHealth() < 5.0f)
					{
						bIsPlayerDetected = false;
						MyPawn = nullptr;
						bNotGoing = false;
						Stop = true;
					}
					else if (DistanceToPlayer > 90.0f && DistanceToPlayer < AISightRadius)
					{
						if (TittyNumber == 1)
						{
							myPawn->isFirstTitty = false;
							Stop = true;
						}
						else if (TittyNumber == 2)
						{
							myPawn->isSecondTitty = false;
							Stop = true;
						}
						else if (TittyNumber == 3)
						{
							myPawn->isThirdTitty = false;
							Stop = true;
						}
					}
				}
			//}
		}
		if (bIsPlayerDetected == false)
		{
			MoveToLocation(Result);
			TimerMove -= DeltaSeconds;
			if (TimerMove < 0)
			{
				GetRandomLocation();
				TimerMove = MR;
			}
		}
		else if (bIsPlayerDetected == true && bNotGoing == false)
		{
			//just for the now player 1
			MoveToActor(MyPawn, 5.0f);
			DistanceToPlayer = GetPawn()->GetDistanceTo(MyPawn);
			bNotGoing = true;
		}
	}
}


void ACC_MobController::GoingToEnemie()
{
	if (Stop)
	{
		ACC_Character* myPawn = Cast<ACC_Character>(MyPawn);
		if (myPawn)
		{
			if (myPawn->isFirstTitty == false)
			{
				MoveToActor(myPawn);
				MoveToLocation(myPawn->FirstTitty->GetComponentLocation());
				myPawn->isFirstTitty = true;
				TittyNumber = 1;
			}
			else if (myPawn->isSecondTitty == false)
			{
				MoveToActor(MyPawn);
				MoveToLocation(myPawn->SecondTitty->GetComponentLocation());
				myPawn->isSecondTitty = true;
				TittyNumber = 2;
			}
			else if (myPawn->isThirdTitty == false)
			{
				MoveToActor(myPawn);
				MoveToLocation(myPawn->ThirdTitty->GetComponentLocation());
				myPawn->isThirdTitty = true;
				TittyNumber = 3;
			}
		
			Stop = false;
		}
	}
}

void ACC_MobController::OnHearNoise(APawn * OtherActor, const FVector & Location, float Volume)
{

}

void ACC_MobController::OnSeePawn(APawn *OtherPawn)
{
	/*FString message = TEXT("Saw Actor ") + OtherPawn->GetName();
	if (GEngine)
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, message);*/
	MyPawn = OtherPawn;
	ACC_Character* myPawn = Cast<ACC_Character>(MyPawn);
	if (myPawn != nullptr && bNotGoing == false && bIsPlayerDetected == false)
	{
				//if(!(Character->NonAgressive))
		DistanceToPlayer = GetPawn()->GetDistanceTo(MyPawn);
		bIsPlayerDetected = true;

	}

	
}

FRotator ACC_MobController::GetControlRotation() const
{
	if (GetPawn() == nullptr)
	{
		return FRotator(0.0f, 0.0f, 0.0f);
	}

	return FRotator(0.0f, GetPawn()->GetActorRotation().Yaw, 0.0f);

}


