// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_Topaz.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "CC_Enemy.h"
#include "DrawDebugHelpers.h"
#include "Engine/GameEngine.h"
#include "CC_Spawner.h"
#include "CC_Camera.h"
#include "AKGameplayStatics.h"

void ACC_Topaz::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ACC_Topaz::EnablePrimary()
{
	if (!this->isAlive) return;
	isShooting = true;
	if (this->CanFirePrimary)
	{
		
		SpawnPrimary();
		Super::EnablePrimary();
	}
}

void ACC_Topaz::EnableSecondary()
{
	if (!this->isAlive) return;

	if (this->CanFireSecondary)
	{

		//Super::SpawnSecondary();//spawns aggro circle5
		isCharge = true;
		//SlowCharacter(this->SlowValue);//slows down speed while aggro circle is active
		if (CanFireSecondary)
		{
			this->AimDecal->SetVisibility(true);
		}
		CanFirePrimary = false;//prevents the player from firing the primary projectile while the secondary ability is active
	}
}

void ACC_Topaz::DisableSecondary()
{
	if (!this->isAlive) return;

	if (this->CanFireSecondary)
	{
		GroundSlamParticle = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ParticleFXSlam, FVector(GetActorLocation().X, GetActorLocation().Y, GetActorLocation().Z + ParticleOffset), GetActorLocation().Rotation(), FVector(1.2, 1.2, 1.2));
		SpawnSecondary();
		UAkGameplayStatics::PostEvent(SecondSound, Secondary);

		ApplyFFEOnPlayerHit();
		ApplyShakeOnDeath();

		this->CanFirePrimary = true;

		isCharge = false;

		Super::EnableSecondary();//Starts cooldown sets canfiresecondary to false
		this->AimDecal->SetVisibility(false);
	}

}

void ACC_Topaz::SpawnPrimary()
{
	Super::SpawnPrimary();
	if (Primary && Primary->GetName() != "None")
	{
		UAkGameplayStatics::PostEvent(atksound, Primary);
	}
	isShooting = true;
}

void ACC_Topaz::SpawnSecondary()
{
	Super::SpawnSecondary(); 
}
