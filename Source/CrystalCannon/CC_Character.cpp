// Fill out your copyright notice in the Description page of Project Settings.
#pragma once

#include "CC_Character.h"
#include "CC_Obsidian.h"
#include "Engine.h"
#include "CC_Camera.h"
#include "CC_GameInstance.h"
#include "CC_Enemy.h"
#include "DrawDebugHelpers.h"
#include "Camera/CameraComponent.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Runtime/Engine/Classes/Kismet/KismetMathLibrary.h"
#include "Components/ArrowComponent.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
#include "AKGameplayStatics.h"

ACC_Character::ACC_Character()
	:Super()
{

	HoldTimer = 0;

	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;


	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	Value = 2.5;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = false; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate

	ReviveDisplay = CreateDefaultSubobject<UWidgetComponent>(TEXT("XforRevive"));
	ReviveDisplay->AttachTo(RootComponent);
}

//grabbing all the variables from the bp, for getting all the places to go...
void ACC_Character::BeginPlay()
{
	Super::BeginPlay();
	CurrentHealth = MaxHealth;
	USkeletalMeshComponent* myMesh = GetMesh();
	FirstROt = myMesh->GetComponentRotation();
	FirstTitty = Cast<UArrowComponent>(GetDefaultSubobjectByName(TEXT("Arrow1")));
	SecondTitty = Cast<UArrowComponent>(GetDefaultSubobjectByName(TEXT("Arrow2")));
	ThirdTitty = Cast<UArrowComponent>(GetDefaultSubobjectByName(TEXT("Arrow3")));
	MinLocation = Cast<UArrowComponent>(GetDefaultSubobjectByName(TEXT("Arrow4")));
	Shoot = Cast<UArrowComponent>(GetDefaultSubobjectByName(TEXT("Shooting")));
	Charge = Cast<UArrowComponent>(GetDefaultSubobjectByName(TEXT("Charging")));
	UIDisplay = Cast<UWidgetComponent>(GetDefaultSubobjectByName(TEXT("PlayerHUD")));
	AimDecal = Cast<UStaticMeshComponent>(GetDefaultSubobjectByName(TEXT("Aim")));


	ReviveDisplay->SetVisibility(false);
	//this->ReviveDisplay->SetHiddenInGame(true);

	//this->ReviveDisplay->SetVisibility(false);

	DefaultWalkSpeed = this->GetCharacterMovement()->MaxWalkSpeed;
}

void ACC_Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	

	FVector2D ActorForward;


	//ActorForward = FVector2D(Charge->GetComponentLocation().ForwardVector.X, Charge->GetComponentLocation().ForwardVector.Y);
	ActorForward = FVector2D(GetActorForwardVector().X, GetActorForwardVector().Y);

	FVector2D ControllStick;

	ControllStick = FVector2D(LYAxis, LXAxis);
	ControllStick.Normalize();
	float dot;
	float angle;
		//dot = FMath::Atan2(ControllStick.Y, ControllStick.X) - FMath::Atan2(ActorForward.Y, ActorForward.X);
		dot = FVector2D::DotProduct(ActorForward, ControllStick);
		angle = acos(dot);
		/*if (ActorForward.Y == -1 && ControllStick.X != 0 || ActorForward.Y == 1 && ControllStick.X != 0)
		{
			angle *= -1;
			dot = 0.f;
		}
		else */if(ControllStick.Y != 0 && ActorForward.Y == 0) angle *= FVector2D::CrossProduct(ActorForward, ControllStick);
		side = sin(angle);
		forward = cos(angle);
	/*	if (this->PlayerIndex == 0)
		{
			FString TheFloatStr = FString::SanitizeFloat(dot);
			FString TheAngleStr = FString::SanitizeFloat(FMath::RadiansToDegrees(angle));
			GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, "Dot: " + 
				TheFloatStr + "ActorForward:" + ActorForward.ToString() + "ControllStick" +
				ControllStick.ToString() + "Angle" + TheAngleStr);

		}*/
	//}

	if (LXAxis == 0 && LYAxis == 0) 
	{
		forward = 0.0f;
		side = 0.0f;
	}

	if (!CanFireSecondary)
	{
		SecondaryCooldown += DeltaTime;
		if (SecondaryCooldown > 8) {
			SCooldownEnd();
			SecondaryCooldown = 8;
		}
	}

	DTime = DeltaTime;

	//Checks for player health and kills player
	if (this->isAlive && this->CurrentHealth <= 0)
	{
		this->isAlive = false;
		GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, "Kill Called");
		this->KillCharacter();
	}

	//Movement
	if (stop1 && stop2) {
		IsWalking = false;
	}
	//setting all the variables for the mesh depending on the input of the player
	USkeletalMeshComponent* myMesh = GetMesh();
	if ((XAxis != 0.0f) || (YAxis != 0.0f)) {
		Flag = true;
		//bUseControllerRotationYaw = true;
		FVector Target = FVector(-1 * YAxis, XAxis, 0.0f);
		FRotator NewRot = Target.Rotation();
		SetActorRotation(NewRot);
		//myMesh->SetWorldRotation(FMath::Lerp(oldRotation, NewRot, 0.1f));
	}
	else {
		//bUseControllerRotationYaw = false;
		if (Flag) {
			IsRight = false;
			IsLeft = false;
			IsWalking = false;
			IsBack = false;
			Flag = false;
			//myMesh->SetWorldRotation(FirstROt);
		}
	}

	//Moves target marker
	if (isMarking)
	{
		ApplyMarkerMovement();
	}

	//Hold to revive
	if (isReviving && inReviveRange)
	{
		HoldTimer += DeltaTime;

		//GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, FString::FromInt(HoldTimer));
		if (HoldTimer >= ReviveHoldDuration)
		{
			isReviving = false;
			HoldTimer = 0;
			Revive();

			UWorld* world = this->GetWorld();
			UCC_GameInstance* Instance = world->GetGameInstance<UCC_GameInstance>();

			Instance->UpdateRevives(Reviver->PlayerIndex);

		}
	}
	else if (HoldTimer > 0) HoldTimer -= DeltaTime;

	if (TapCurrent >= TapCounter)
	{
		Revive();
		TapCurrent = 0;
	}

}

void ACC_Character::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	check(PlayerInputComponent);
	PlayerInputComponent->BindAxis("MoveForward", this, &ACC_Character::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ACC_Character::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("FaceNorth", this, &ACC_Character::LookUpAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("FaceEast", this, &ACC_Character::TurnAtRate);

	//Ability & Debug bindings
	PlayerInputComponent->BindAction("Primary", IE_Pressed, this, &ACC_Character::EnablePrimary);
	PlayerInputComponent->BindAction("Primary", IE_Released, this, &ACC_Character::DisablePrimary);
	PlayerInputComponent->BindAction("Secondary", IE_Pressed, this, &ACC_Character::EnableSecondary);
	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &ACC_Character::EnableSprint);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &ACC_Character::DisableSprint);
	PlayerInputComponent->BindAction("Secondary", IE_Released, this, &ACC_Character::DisableSecondary);
	PlayerInputComponent->BindAction("Revive", IE_Pressed, this, &ACC_Character::StartRevive);
	PlayerInputComponent->BindAction("Revive", IE_Released, this, &ACC_Character::EndRevive);
	PlayerInputComponent->BindAction("DamageSelf", IE_Pressed, this, &ACC_Character::DamageSelf);

	PlayerInputComponent->BindAction("Pause", IE_Released, this, &ACC_Character::SpawnPause);

	CharMovComp = this->GetCharacterMovement();
	DefaultWalkSpeed = this->CharMovComp->GetMaxSpeed();
}
//if this function get call it will do damage to whatever actor
void ACC_Character::ApplyDamage(float Damage)
{
	Super::ApplyDamage(Damage);
	this->ApplyFFEOnPlayerHit();
	
	SpawnBlood();

	UWorld* world = this->GetWorld();
	UCC_GameInstance* Instance = world->GetGameInstance<UCC_GameInstance>();

	Instance->UpdateDamageTaken(this->PlayerIndex, Damage);

	//GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, "PlayerHitFFE");
}
//getting values from right stick
void ACC_Character::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	XAxis = Rate;
}
//getting values from right stick
void ACC_Character::LookUpAtRate(float Rate)
{
	if (!isAlive) return;

	YAxis = Rate;
}

void ACC_Character::ApplyMarkerMovement()
{
	FVector Direction(-YAxis, XAxis, 0.0f);
	if (SpawnedMark)
	{
		SpawnedMark->AddMovementInput(Direction, Direction.Size()*100, true);
		GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, Direction.ToString());
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, TEXT("MarkerNull"));
	}

}

void ACC_Character::MoveForward(float Value)
{
	if (!isAlive) return;

	LYAxis = Value;

	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
		IsWalking = true;
		stop1 = false;

	}
	else {
		stop1 = true;
	}
}

void ACC_Character::MoveRight(float Value)
{
	if (!isAlive) return;

	LXAxis = Value;

	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
		IsWalking = true;
		stop2 = false;
	}
	else {
		stop2 = true;
	}
}
//change values of speed depending on the ability in use
void ACC_Character::SlowCharacter(float Value)
{
	this->CharMovComp->MaxWalkSpeed = CharMovComp->GetMaxSpeed() / Value;
}
//change speed to normal
void ACC_Character::ResetSpeed()
{
	this->CharMovComp->MaxWalkSpeed = DefaultWalkSpeed;
}
//debug function purpose
void ACC_Character::DamageSelf()
{
	if (!isAlive) return;

	FString HealthString = FString::FromInt(this->CurrentHealth);
	GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, HealthString);
	this->ApplyDamage(10); 
}

void ACC_Character::StartRevive()
{
	if (!isAlive)
	{
		TapCurrent++;
	}
	else
	{
		TArray<TEnumAsByte<EObjectTypeQuery>> Types;
		TArray<AActor*> ActorsToIgnore;
		TArray<AActor*> OutActors;

		UWorld* World = this->GetWorld();

		ActorsToIgnore.Add(this);
		bool canRevive = UKismetSystemLibrary::SphereOverlapActors(World, this->GetActorLocation(), ReviveRadius, Types, ACC_Character::StaticClass(), ActorsToIgnore, OutActors);

		DrawDebugSphere(World, this->GetActorLocation(), ReviveRadius, 26, FColor(181, 0, 0), true, -1);

		if (canRevive)
		{
			for (AActor* Actor : OutActors)
			{
				ACC_Character* RevivingChar = Cast<ACC_Character>(Actor);
				if (!(RevivingChar->isAlive))
				{
					RevivingChar->isReviving = true;
					LastRevivedChar = RevivingChar;
				}
			}
		}
	}

}

void ACC_Character::EndRevive()
{
	if (LastRevivedChar)
	{
		LastRevivedChar->isReviving = false;
	}
}
//if you get closer to a player and he is dead, the flag will disappear and the player will be reset for normal values
void ACC_Character::Revive()
{
	ResetCharacter();
	ReviveCirclePtr->Destroy();
}
//function to be call if the health is set to 0 
void ACC_Character::KillCharacter()
{
	UWorld* world = this->GetWorld();
	UCC_GameInstance* Instance = world->GetGameInstance<UCC_GameInstance>();
	Instance->UpdateDeaths(this->PlayerIndex);

	PlayerDeathEvent();

	UWorld* World = this->GetWorld();

	//Spawn Params
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnInfo.Owner = this;
	SpawnInfo.Instigator = this;

	//Spawn Location
	FVector SpawnLocation = this->GetActorLocation();
	
	//Spawn Rotation
	FRotator SpawnRotation = this->GetActorRotation();

	if (World == nullptr)
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, "NULL @ Character Kill Character");
	}
	else
	{
		ReviveCirclePtr = World->SpawnActor(RespawnActor, &SpawnLocation, &SpawnRotation, SpawnInfo);
	}
	
	//this->DisableInput(this->PC);
	FVector Location = GetActorLocation();
	DeadParticle = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ParticleFX1, FVector(Location.X, Location.Y, Location.Z - 50.f), GetActorRotation(), FVector(1.2f, 1.2f, 1.2f));
	UCC_GameInstance* GI = Cast<UCC_GameInstance>(this->GetGameInstance());
	GI->IncrementDeadPlayerCount();
	this->isAlive = false;
	this->GetCharacterMovement()->StopMovementImmediately();
	GetCapsuleComponent()->SetCollisionProfileName(TEXT("Dead"));
	//this->ReviveDisplay->SetWidgetClass(ReviveWidget);
	this->ReviveDisplay->SetVisibility(true);
	this->UIDisplay->SetVisibility(false);
	
	UGameplayStatics::SetGlobalTimeDilation(GetWorld(), SlowMoValue);
	
	FTimerHandle Callback;
	GetWorldTimerManager().SetTimer(Callback, this, &ACC_Character::ResetWorldTime, SlowMoDuration, false);
	
	GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, "Slowdown Time");

	this->GetMesh()->SetVisibility(false);
	SpawnDeathFlag();
	//OnDeathTransform = this->GetActorTransform();
	//PC = UGameplayStatics::GetPlayerController(GetWorld(), this->PlayerIndex);
	//this->GetMesh()->SetCollisionProfileName("Ragdoll");
	//this->GetMesh()->SetSimulatePhysics(true);
	//GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, "CharacterKilled");


}
//revive player to set the player to alive
void ACC_Character::ResetCharacter()
{
	PlayerReviveEvent(Reviver,this);

	UCC_GameInstance* GI = Cast<UCC_GameInstance>(this->GetGameInstance());
	GI->DecrementDeadPlayerCount();
	this->GetCapsuleComponent()->SetRelativeRotation(FRotator(0, 0, 0));
	this->isAlive = true;
	this->CurrentHealth = 0;
	this->ApplyHealing(this->MaxHealth);
	this->ReviveDisplay->SetVisibility(false);
	this->UIDisplay->SetVisibility(true);
	if(this->GetMesh())	this->GetMesh()->SetVisibility(true);
	GetCapsuleComponent()->SetCollisionProfileName(TEXT("Players"));
	DeadParticle->DestroyComponent();

	//this->GetMesh()->SetSimulatePhysics(false);
	//this->GetMesh()->SetCollisionProfileName("BlockAllDynamic");
	//this->GetMesh()->SetRelativeLocation(FVector(this->GetActorRotation().Vector().X, this->GetActorRotation().Vector().Y, -90.f));
	//this->GetMesh()->SetRelativeRotation(FRotator(0, 270, 0));
	//this->SetActorRelativeTransform(OnDeathTransform);
	//this->GetMesh()->AttachTo(this->GetRootComponent());
	//PC = UGameplayStatics::GetPlayerController(GetWorld(), this->PlayerIndex);
	//GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, "Revivied");
	//this->EnableInput(this->PC);

	RevFlagPtr->Destroy();

}

//when it get kill a flag for revive will be spawn 
void ACC_Character::SpawnDeathFlag()
{
	UWorld* World = this->GetWorld();

	//Spawn Params
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnInfo.Owner = this;
	SpawnInfo.Instigator = this;

	//Spawn Location
	FVector SpawnLocation = this->GetActorLocation();
	//SpawnLocation += FVector(0, 0, 1000);
	//SpawnLocation.Z += 40.f;

	//Spawn Rotation
	FRotator SpawnRotation = this->GetActorRotation();

	if (World == nullptr)
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, "NULL @ Character Spawn Death Flag");
	}
	else
	{
		RevFlagPtr = World->SpawnActor(ReviveFlag, &SpawnLocation, &SpawnRotation, SpawnInfo);
	}

}
void ACC_Character::DetatchSecondary()
{
	if (Secondary != nullptr)
	{
		Secondary->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
	}

}

//primary attack initialized
void ACC_Character::EnablePrimary()
{
	if (!isAlive) return;

	if (this->CanFirePrimary)
	{
		this->CanFirePrimary = false;
		GetWorldTimerManager().SetTimer(CallbackHandlerShort, this, &ACC_Character::PShortCooldownEnd, PrimaryShortCooldown, false);
		//GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Green, "Primary Short CD started");
	}

	isButtonPressed = true;
	/*
	if (HasFiredPrimary == false)
	{
		HasFiredPrimary = true;
		FTimerHandle CallbackHandler;
		GetWorldTimerManager().SetTimer(CallbackHandler, this, &ACC_Character::EnablePrimary, PrimaryCooldown, false);
	}
	*/
}

void ACC_Character::DisablePrimary()
{
	if (!isAlive) return;
	//CanFirePrimary = true;
	isButtonPressed = false;
}

void ACC_Character::EnableSecondary()
{
	if (!isAlive) return;

	if (this->CanFireSecondary)
	{
		SecondaryCooldown = 0;
		this->CanFireSecondary = false;
		//GetWorldTimerManager().SetTimer(CallbackHandlerSecondary, this, &ACC_Character::SCooldownEnd, SecondaryCooldown, false);
		//GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Green, "Secondary CD started");
	}
}

void ACC_Character::DisableSecondary()
{
	if (!isAlive) return;
	//CanFireSecondary = true;s
	//SelectTier();
}
//Tier for charge to see how long you pressed the button
//void ACC_Character::SelectTier()
//{
//	if (HoldTimer >= ThirdChargeTime && SpecialMeter == 3)
//	{
//		GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, "Tier 3", 1);
//		SpawnSecondary(3);
//		SpecialMeter-=3;
//	}
//	else if (HoldTimer >= SecondChargeTime && SpecialMeter >= 2)
//	{
//		GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, "Tier 2", 1);
//		SpawnSecondary(2);
//		SpecialMeter-=2;
//	}
//	else if(SpecialMeter >= 1)
//	{
//		GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, "Tier 1", 1);
//		SpawnSecondary(1);
//		SpecialMeter--; 
//	}
//	else
//	{
//		MisfireEvent();
//	}
//
//	HoldTimer = 0;
//}
//spawn projectile with sound and particle
void ACC_Character::SpawnPrimary()
{
	if (!isAlive) return;

	UWorld* World = this->GetWorld();

	//Spawn Params
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnInfo.Owner = this;
	SpawnInfo.Instigator = this;

	//Spawn Location
	FVector SpawnLocation = this->Shoot->GetComponentLocation();
	SpawnLocation += this->Shoot->GetForwardVector() * FireDistance;
	//SpawnLocation.Z += 40.f;

	//Spawn Rotation
	FRotator SpawnRotation = this->Shoot->GetForwardVector().Rotation();

	if (World == nullptr)
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, "NULL @ Character SpawnPrimary");
	}
	else
	{
		Primary = World->SpawnActor(PrimaryAbility, &SpawnLocation, &SpawnRotation, SpawnInfo);
	}
}

//spawn secondary attack with projectile
void ACC_Character::SpawnSecondary()
{
	if (!isAlive) return;

	UWorld* World = this->GetWorld();
	
	//Spawn Params
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnInfo.Owner = this;
	SpawnInfo.Instigator = this;

	//Spawn Location
	FVector SpawnLocation = this->Charge->GetComponentLocation();

	//Spawn Rotation
	FRotator SpawnRotation = this->Charge->GetForwardVector().Rotation();

	if (World == nullptr)
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, "NULL @ Character SpawnSecondary");
	}
	else
	{
		Secondary = World->SpawnActor(SecondaryAbility, &SpawnLocation, &SpawnRotation, SpawnInfo);
	}

	//AttachSecondary();
	//isCharge = false;
}

void ACC_Character::SpawnMarker()
{
	if (!isAlive) return;

	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnInfo.Owner = this;
	SpawnInfo.Instigator = Instigator;

	FVector SpawnLocation = this->Shoot->GetComponentLocation();
	SpawnLocation += this->Shoot->GetForwardVector() * (FireDistance * ChargeDistance);
	SpawnLocation.Z -= 180.f;

	//Spawn Rotation
	FRotator SpawnRotation = this->Shoot->GetForwardVector().Rotation();

	SpawnedMark = GetWorld()->SpawnActor<ATargetMarker>(TargetMark, SpawnLocation, SpawnRotation, SpawnInfo);

}

void ACC_Character::DestroyMark()
{
	if (SpawnedMark)
	{
		SpawnedMark->Destroy();
		SpawnedMark = nullptr;
	}
}

void ACC_Character::SpawnPause()
{
	if (!PauseSpawned)
	{
		Pause = CreateWidget(GetWorld(), PauseMenu);
		Pause->AddToViewport(9999);
		PauseSpawned = true;
	}
	else 
	{
		UnSpawnPause();
		PauseSpawned = false;
	}
}

void ACC_Character::UnSpawnPause()
{
	Pause->RemoveFromParent();
	Pause = nullptr;
}

void ACC_Character::CallStepsForCharacter()
{
	UWorld* World = this->GetWorld();

	//Spawn Params
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnInfo.Owner = this;
	SpawnInfo.Instigator = this;

	//Spawn Location
	FVector SpawnLocation = GetActorLocation();

	//Spawn Rotation
	FRotator SpawnRotation = GetActorRotation();

	if (World == nullptr)
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, "NULL @ Character SpawnSecondary");
	}
	else
	{
		Dumb = World->SpawnActor(DumbActor, &SpawnLocation, &SpawnRotation, SpawnInfo);
	}

	UAkGameplayStatics::SetSwitch(FName("FS_Surface"), Surface, Dumb);
	UAkGameplayStatics::SetSwitch(FName("MVMT_Character"), Character, Dumb);
	UAkGameplayStatics::PostEvent(footsteps, Dumb);
}

void ACC_Character::EnableSprint()
{
	if (!isAlive) return;

	this->GetCharacterMovement()->MaxWalkSpeed = SprintSpeed;
	isSprinting = true;
}

void ACC_Character::DisableSprint()
{
	if (!isAlive) return;

	this->GetCharacterMovement()->MaxWalkSpeed = DefaultWalkSpeed;
	isSprinting = false;
}

void ACC_Character::PShortCooldownEnd()
{
	this->CanFirePrimary = true;
	isShooting = false;
	isCharge = false;
	//GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, "Primary short CD ended", 1);
	//If at the end of the short cooldown & the button is continued to be held, It starts the longcooldown
	if(isButtonPressed && CanFirePrimary)
	{
		SpawnPrimary();
		GetWorldTimerManager().SetTimer(CallbackHandlerShort, this, &ACC_Character::PShortCooldownEnd, PrimaryShortCooldown, false);
	}
	
	/*if (Timer >= Duration)
	{
		Timer = 0;
		ControlVar = false;
		GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, "TestCD");
	}*/
}

void ACC_Character::SCooldownEnd()
{
	this->CanFireSecondary = true;
	isCharge = false;
	GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Green, "Secondary CD ended", 1);
}

void ACC_Character::AttachSecondary()
{
	if (Secondary != nullptr)
	{
		Secondary->AttachToActor(this, FAttachmentTransformRules::SnapToTargetNotIncludingScale, "None");
	}
}

void ACC_Character::ResetWorldTime()
{
	UGameplayStatics::SetGlobalTimeDilation(GetWorld(), 1);
	GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, "Reset Time");
}
