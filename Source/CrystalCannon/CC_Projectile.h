// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "CC_Projectile.generated.h"

UCLASS()
class CRYSTALCANNON_API ACC_Projectile : public AActor
{
	GENERATED_BODY()
	
public:
	//Projectile speed
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
		float Velocity;

	//Projectile damage
	UPROPERTY(EditAnywhere, Category = "Projectile")
		float MaxRange;

	UPROPERTY(EditAnywhere, Category = "Projectile")
		float Damage;

	UPROPERTY(EditAnywhere, Category = "Collision")
		UCapsuleComponent* CapsuleComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Projectile Movement")
		UProjectileMovementComponent* ProjectileMovementComponent;

	FVector StartLoc;
	FVector CurrentLoc;

protected:
	//Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//On Hit function, handles on hit for all cases except enemies, enemy on hit needs to be overridden in child classes
	UFUNCTION()
	virtual void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit);

	//Called when something enters the sphere
	UFUNCTION()
	virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	//Called when something leaves sphere
	UFUNCTION()
	virtual void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	//Sets initial velocity of projectile and enables movement
	UFUNCTION(BlueprintCallable)
	void InitVelocity(const FVector& Direction);

	//Calculates the square of the magnitude of 2 vectors
	float SqrMag(FVector A, FVector B);

public:

	// Sets default values for this actor's properties
	ACC_Projectile();
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
