// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotify.h"
#include "CC_SocketCrystal.generated.h"

/**
 * 
 */
class UStaticMesh;
UCLASS()
class CRYSTALCANNON_API UCC_SocketCrystal : public UAnimNotify
{
	GENERATED_BODY()

	virtual void Notify(USkeletalMeshComponent* SkeletalMesh, UAnimSequenceBase* Animation) override;

	UPROPERTY(EditAnywhere)
	UStaticMesh* MeshC;

private:
	UPROPERTY()
		UStaticMeshComponent* MeshComponent;
};
