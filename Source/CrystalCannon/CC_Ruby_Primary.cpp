// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_Ruby_Primary.h"
#include "CC_Ruby.h"
#include "TimerManager.h"
#include "CC_Enemy.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Engine/GameEngine.h"
#include "CC_ExplosionDamage.h"

void ACC_Ruby_Primary::OnHit(UPrimitiveComponent * HitComponent, AActor * OtherActor, UPrimitiveComponent * OtherComponent, FVector NormalImpulse, const FHitResult & Hit)
{
	Super::OnHit(HitComponent, OtherActor, OtherComponent, NormalImpulse, Hit);
	ACC_Enemy* Enemy = Cast<ACC_Enemy>(OtherActor);
		if (Enemy != nullptr)
		{
		ACC_Ruby* Ruby = Cast<ACC_Ruby>(GetOwner());
				
		if (Ruby != nullptr)
			{
				if (Ruby->isTargetSet(OtherActor))
				{
					Detonate();
					Ruby->PrimedEnemy = nullptr;

					if (this->GetOwner() != nullptr)
					{
						Enemy->LastHit = this->GetOwner();
						Cast<ACC_Character>(this->GetOwner())->ApplyFFEOnDeath();
					}
				}
				else
				{
					Ruby->PrimedEnemy = OtherActor;
					Enemy->ApplyDamage(Damage);

					if (this->GetOwner() != nullptr)
					{
						Enemy->LastHit = this->GetOwner();
						Cast<ACC_Character>(this->GetOwner())->ApplyFFEOnDeath();
					}
				}
			}
		}			
		Destroy();
}


void ACC_Ruby_Primary::Detonate()
{
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnInfo.Owner = this;
	SpawnInfo.Instigator = GetInstigator();

	FVector Location = GetActorLocation();
	FRotator Rotation = GetActorRotation();

	ACC_ExplosionDamage* tempActor = Cast<ACC_ExplosionDamage>(GetWorld()->SpawnActor(Explosion, &Location, &Rotation, SpawnInfo));
	Destroy();
}

