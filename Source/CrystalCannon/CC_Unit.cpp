// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_Unit.h"
#include "Engine/World.h"
#include "CC_Character.h"
#include "Engine/GameEngine.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Camera/CameraShake.h"

// Sets default values
ACC_Unit::ACC_Unit()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACC_Unit::BeginPlay()
{
	Super::BeginPlay();
	this->CurrentHealth = this->MaxHealth;
}

// Called every frame
void ACC_Unit::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ACC_Unit::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}


void ACC_Unit::ApplyDamage(float Damage)
{
	if (isInvulnerable)
	{
		return;
	}
	else
	{
		Damage = FMath::Abs(Damage);
	
		DamageEvent();
	
		this->CurrentHealth -= Damage;

		DamageTaken = Damage;

		if (CurrentHealth < 0) 
		{
			DamageEvent();
		}
	}
}

void ACC_Unit::ApplyHealing(float HealAmount)
{
	HealAmount = FMath::Abs(HealAmount);

	HealingEvent();

	this->CurrentHealth += HealAmount;

	HealTaken = HealAmount;

	//Caps current health at Max
	if (this->CurrentHealth > this->MaxHealth)
	{
		this->CurrentHealth = this->MaxHealth;
	}
}

void ACC_Unit::SpawnDeathParticle()
{
	UWorld* World = this->GetWorld();

	//Spawn Params
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnInfo.Owner = this;
	SpawnInfo.Instigator = this;

	//Spawn Location
	FVector SpawnLocation = this->GetActorLocation();
	//SpawnLocation.Z += 40.f;

	//Spawn Rotation
	FRotator SpawnRotation = this->GetActorRotation();

	if (World == nullptr)
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, "NULL @ Unit Spawn Death Particle");
	}
	else
	{
		DeathParticlePtr = World->SpawnActor(DeathParticle, &SpawnLocation, &SpawnRotation, SpawnInfo);
	}
}

void ACC_Unit::SpawnExplosionDeathParticle()
{
	UWorld* World = this->GetWorld();

	//Spawn Params
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnInfo.Owner = this;
	SpawnInfo.Instigator = this;

	//Spawn Location
	FVector SpawnLocation = this->GetActorLocation();
	//SpawnLocation.Z += 40.f;

	//Spawn Rotation
	FRotator SpawnRotation = this->GetActorRotation();

	if (World == nullptr)
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, "NULL @ Unit Spawn Death Particle");
	}
	else
	{
		DeathParticlePtr = World->SpawnActor(ExplosionDeathParticle, &SpawnLocation, &SpawnRotation, SpawnInfo);
	}
}


void ACC_Unit::ApplyShakeOnDeath()
{
	if (this->GetWorld() != nullptr)
	{
		UWorld* World = this->GetWorld();

		APlayerController* LocalController = UGameplayStatics::GetPlayerController(GetWorld(), 0);


		if (EnemyKillShake != nullptr)
		{
			LocalController->PlayerCameraManager->PlayCameraShake(EnemyKillShake, 1.f);
		}

	}
}

void ACC_Unit::ApplyFFEOnDeath()
{
	FLatentActionInfo latentInfo;
	latentInfo.CallbackTarget = this;

	if (this->LastHit != nullptr)
	{
		ACC_Character* Character = Cast<ACC_Character>(LastHit);

		if (this->GetWorld() != nullptr)
		{
			UWorld* World = this->GetWorld();
			UGameplayStatics::GetPlayerController(World, Character->PlayerIndex)->ClientPlayForceFeedback(FeedbackEffectEnemyDeath, false, "EnemyKilled");

		}
	}
}

void ACC_Unit::ApplyFFEOnPlayerHit()
{
	FLatentActionInfo latentInfo;
	latentInfo.CallbackTarget = this;

	ACC_Character* Character = Cast<ACC_Character>(this);

	if (this->GetWorld() != nullptr)
	{
		UWorld* World = this->GetWorld();
		UGameplayStatics::GetPlayerController(World, Character->PlayerIndex)->ClientPlayForceFeedback(FeedbackEffectPlayerHit, false, "PlayerTakesDamage");
	}
}

void ACC_Unit::SpawnBlood()
{

	//Spawn Params
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnInfo.Owner = this;
	SpawnInfo.Instigator = this;

	//Spawn Location
	FVector SpawnLocation = this->GetActorLocation();
	//SpawnLocation.Z += 40.f;

	//Spawn Rotation
	FRotator SpawnRotation = this->GetActorRotation();

	UWorld* World = this->GetWorld();

	if (World == nullptr)
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, "NULL @ Enemy SpawnBlood");
	}
	else
	{
		World->SpawnActor(BloodSplatter, &SpawnLocation, &SpawnRotation, SpawnInfo);
	}
}