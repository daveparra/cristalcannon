// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_SplitterController.h"
#include "CC_ShootingCrystal.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/PawnSensingComponent.h."
#include "Runtime/Core/Public/Math/UnrealMathUtility.h"
#include "CC_Character.h"
#include "Engine/GameEngine.h"
#include "Engine.h"
#include "DrawDebugHelpers.h"
#include "CC_SplitterCharacter.h"
#include "Components/ArrowComponent.h"
#include "Runtime/NavigationSystem/Public/NavigationSystem.h"
#include "CC_MobEnemie.h"
#include "Runtime/Engine/Public/TimerManager.h"

ACC_SplitterController::ACC_SplitterController()
{
	DistanceForRange = 1500.0f;
	PrimaryActorTick.bCanEverTick = true;
	PawnSensor = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("Pawn Sensor"));
	SetPerceptionComponent(*CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("AIPerception Component")));
	PawnSensor->SensingInterval = .12f; // 4 times per second
	PawnSensor->bOnlySensePlayers = false;
	PawnSensor->SetPeripheralVisionAngle(40.f);
	PawnSensor->SightRadius = DistanceForRange;
	

	PawnSensor->OnSeePawn.AddDynamic(this, &ACC_SplitterController::OnSeePawn);
	TimerMove = 2;

	radius = 600.0f;
	
	Timer = -1.0f;
	static ConstructorHelpers::FObjectFinder<UParticleSystem> Particle(TEXT("/Game/Art/Particles/PS_DwarfImpact.PS_DwarfImpact"));
	ParticleFX1 = Particle.Object;
}


void ACC_SplitterController::OnSeePawn(APawn * OtherPawn)
{
	ACC_Character* Player = Cast<ACC_Character>(OtherPawn);
	if (Player != nullptr && Player->isAlive)
	{
		CharacterPositon = OtherPawn->GetActorLocation();
		originX = CharacterPositon.X;
		originY = CharacterPositon.Y;
	}
	else 
	{
		CharacterPositon = FVector::ZeroVector;
	}
}

void ACC_SplitterController::BeginPlay()
{
	Super::BeginPlay();
	//GetRandomLocation();
}

void ACC_SplitterController::Possess(APawn * Pawn)
{
	Super::Possess(Pawn);
}

void ACC_SplitterController::GetRandomLocation()
{
	UNavigationSystemV1* NavSys = UNavigationSystemV1::GetCurrent(GetWorld());
	if (!NavSys)
	{
		return;
	}
	if (GetPawn())
	{
		FVector Origin = GetPawn()->GetActorLocation();
		bool BSucces = NavSys->K2_GetRandomPointInNavigableRadius(GetWorld(), Origin, Result, MovementRadius);
	}
}

void ACC_SplitterController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	if ((Cast<ACC_SplitterCharacter>(GetPawn())->GetCurrentHealth() > 0.0f))
	{
		if (Cast<ACC_SplitterCharacter>(GetPawn())->isKanikase)
		{
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ParticleFX1, GetPawn()->GetActorLocation(), GetPawn()->GetActorRotation(), FVector(2.f, 2.f, 2.f));
			GetPawn()->Destroy();
			Destroy();
			CharacterPositon = FVector::ZeroVector;
		}
		if (!CharacterPositon.Equals(FVector::ZeroVector))
		{
			StopMovement();
			FVector VectorRetult = CharacterPositon - GetPawn()->GetActorLocation();
			GetPawn()->SetActorRotation(VectorRetult.Rotation());
			/*angle += DeltaSeconds;
			newX = originX + cos(angle)*radius;
			newY = originY + sin(angle)*radius;
			FVector newVec = FVector(newX, newY, 0.0f);
			MoveToLocation(newVec);*/

			FVector Start = GetPawn()->GetActorLocation();

			FVector ForwardVector = FRotationMatrix(GetPawn()->GetControlRotation()).GetScaledAxis(EAxis::X);
			FVector End = ((ForwardVector * DistanceForRange) + Start);

			DrawDebugLine(GetWorld(), Start, End, FColor::Green);
			if (Timer < 0) 
			{
				Timer = FR;
				FActorSpawnParameters SpawnInfo;
				SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnInfo.Owner = GetPawn();
				SpawnInfo.Instigator = GetPawn();

				//Spawn Location
				FVector SpawnLocation = GetPawn()->GetActorLocation();
				SpawnLocation += GetPawn()->GetActorRotation().Vector() * FireDistance;
				FRotator SpawnRotation;
						
				if (randomNumber == 0)
				{
					SpawnRotation = GetPawn()->GetActorRotation();
					randomNumber++;
				}
				else if (randomNumber == 1)
				{
					SpawnRotation = GetPawn()->GetActorRotation();
					SpawnRotation = FRotator(SpawnRotation.Pitch, SpawnRotation.Yaw -10.f, SpawnRotation.Roll);
					randomNumber++;
				}
				else if (randomNumber == 2) 
				{
					SpawnRotation = GetPawn()->GetActorRotation();
					SpawnRotation = FRotator(SpawnRotation.Pitch, SpawnRotation.Yaw +10.f, SpawnRotation.Roll);
					randomNumber = 0;
				}
				Primary = GetWorld()->SpawnActor(CrystalToUse, &SpawnLocation, &SpawnRotation, SpawnInfo);
				Cast<ACC_SplitterCharacter>(GetPawn())->PlayAttackSound();
			}
			Timer -= DeltaSeconds;
		}
		else
		{
			MoveToLocation(Result);
			TimerMove -= DeltaSeconds;
			if (TimerMove < 0) 
			{
				GetRandomLocation();
				TimerMove = MR;
			}
		}
	}
	else
	{
		Cast<ACC_SplitterCharacter>(GetPawn())->PlayDeadSound();
		GetPawn()->Destroy();
		Destroy();
	}

}

FRotator ACC_SplitterController::GetControlRotation() const
{
	if (GetPawn() == nullptr)
	{
		return FRotator(0.0f, 0.0f, 0.0f);
	}

	return FRotator(0.0f, GetPawn()->GetActorRotation().Yaw, 0.0f);

}



