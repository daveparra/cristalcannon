// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "CC_Topaz_Secondary.generated.h"

UCLASS()
class CRYSTALCANNON_API ACC_Topaz_Secondary : public AActor
{
	GENERATED_BODY()

protected:
	//Overlaping sphere
	UPROPERTY(EditAnywhere, Category = "Collision")
	UBoxComponent* BoxCollisionVertical;

	UPROPERTY(EditAnywhere, Category = "Collision")
	UBoxComponent* BoxCollisionHorizontal;

	UPROPERTY(EditAnywhere, Category = "Collision")
	float Damage = 0;
	
	TArray<AActor*> OverlappingActors;//Array of all actors inside the healing circle

public:
	// Sets default values for this actor's properties
	ACC_Topaz_Secondary();

	//Called when something enters the sphere
	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
};
