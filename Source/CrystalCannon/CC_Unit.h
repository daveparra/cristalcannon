// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Runtime/Engine/Classes/GameFramework/ForceFeedbackEffect.h"
#include "GameFramework/Character.h"
#include "CC_Unit.generated.h"

UCLASS()
class CRYSTALCANNON_API ACC_Unit : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ACC_Unit();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera Shake")
	TSubclassOf<class UCameraShake> EnemyKillShake;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "FFE")
	UForceFeedbackEffect* FeedbackEffectEnemyDeath;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "FFE")
	UForceFeedbackEffect* FeedbackEffectPlayerHit;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Death Particle")
	TSubclassOf<AActor> DeathParticle;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Death Particle")
	TSubclassOf<AActor> ExplosionDeathParticle;

	UPROPERTY(EditAnywhere, Category = "Blood Splatter")
		TSubclassOf<AActor> BloodSplatter;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Unit Health")
	float MaxHealth;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Unit Health")
	bool isInvulnerable = false;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Unit Health")
	float CurrentHealth;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float DamageTaken;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float HealTaken;

public:	
	//Set Health Variables
	bool isAlive = true;

	UFUNCTION(BlueprintCallable)
	void SetCurrentHealth(float Value) { this->CurrentHealth = Value; }
	
	UFUNCTION(BlueprintCallable)
	void SetMaxHealth(float Value) { this->MaxHealth = Value; }
	
	//Get Health Values
	UFUNCTION(BlueprintCallable)
	float GetMaxHealth() { return this->MaxHealth; }

	UFUNCTION(BlueprintCallable)
	float GetCurrentHealth() { return this->CurrentHealth; }

	//Apply Damage (Pass negative values to heal)
	UFUNCTION(BlueprintCallable)
	virtual void ApplyDamage(float Damage);

	UFUNCTION(BlueprintCallable)
	virtual void ApplyHealing(float HealAmount);

	//Spawn blood particle on hit
	UFUNCTION()
	void SpawnBlood();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void DamageEvent();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void HealingEvent();

	void SpawnDeathParticle();

	void SpawnExplosionDeathParticle();

	void ApplyShakeOnDeath();

	void ApplyFFEOnDeath();

	void ApplyFFEOnPlayerHit();

	UPROPERTY(BlueprintReadOnly, Category = "Last Hit")
	AActor* LastHit;

	UPROPERTY(BlueprintReadOnly, Category = "Last Hit")
	
	AActor* DeathParticlePtr;
};
