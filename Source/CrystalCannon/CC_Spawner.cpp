// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_Spawner.h"

// Sets default values
ACC_Spawner::ACC_Spawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACC_Spawner::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACC_Spawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

