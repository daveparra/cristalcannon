// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CC_Wheel.generated.h"

class URotatingMovementComponent;

UCLASS()
class CRYSTALCANNON_API ACC_Wheel : public AActor
{
	GENERATED_BODY()

	

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* MeshComponent;
	
public:	
	// Sets default values for this actor's properties
	ACC_Wheel();

	UPROPERTY(EditAnywhere)
		URotatingMovementComponent* Movement;

	UPROPERTY(EditAnywhere)
		bool BackLeft;

	UPROPERTY(EditAnywhere)
		bool BackRight;

	UPROPERTY(EditAnywhere)
		bool MidLeft;

	UPROPERTY(EditAnywhere)
		bool MidRight;

	UPROPERTY(EditAnywhere)
		bool FrontLeft;

	UPROPERTY(EditAnywhere)
		bool FrontRight;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
