// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_ExplosionDamage.h"
#include "Components/SphereComponent.h"
#include "CC_Character.h"
#include "CC_Enemy.h"

// Sets default values
ACC_ExplosionDamage::ACC_ExplosionDamage()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	VisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("VisionSphere"));
	VisionSphere->AttachTo(RootComponent);
	VisionSphere->SetSphereRadius(VisionRadius);
	VisionSphere->OnComponentBeginOverlap.AddDynamic(this, &ACC_ExplosionDamage::OnSphereBeginOverlap);
}

// Called when the game starts or when spawned
void ACC_ExplosionDamage::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ACC_ExplosionDamage::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Timer -= DeltaTime;
	if (Timer < 0.f) 
	{
		Destroy();
	}
}

void ACC_ExplosionDamage::OnSphereBeginOverlap(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	ACC_Character* Player = Cast<ACC_Character>(OtherActor);
	if (Player)
	{
		Player->ApplyDamage(DamageExplosion);
	}
}
