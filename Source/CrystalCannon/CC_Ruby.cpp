// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_Ruby.h"// Fill out your copyright notice in the Description page of Project Settings.
#include "Engine/GameEngine.h"
#include "CC_Projectile.h"
#include "CC_Jade_Secondary.h"
#include "Components/ArrowComponent.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystem.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemComponent.h"
#include "AKGameplayStatics.h"
#include "Kismet/GameplayStatics.h"
#include "CC_RubySecondary.h"


void ACC_Ruby::BeginPlay()
{
	Super::BeginPlay();
}
void ACC_Ruby::Tick(float DeltaTime)
{
	//GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, "Ruby" + FString::FromInt(HoldTimer));
	Super::Tick(DeltaTime);
}

void ACC_Ruby::EnablePrimary()
{
	if (!this->isAlive) return;
	isShooting = true;
	if (this->CanFirePrimary)
	{
		
		SpawnPrimary();
		Super::EnablePrimary();
	}

}

void ACC_Ruby::EnableSecondary()
{
	//Super::SpawnSecondary();
	if (!this->isAlive) return;
	if (CanFireSecondary)
	{
		this->AimDecal->SetVisibility(true);
	}
}

void ACC_Ruby::DisableSecondary()
{
	
	if(CanFireSecondary)
	{
		isCharge = true;
		SpawnSecondary();
		this->AimDecal->SetVisibility(false);
		Super::EnableSecondary();
	}
	Super::DisableSecondary();
	isCharge = false;
}

void ACC_Ruby::SpawnPrimary()
{
	Super::SpawnPrimary();
	if (Primary && Primary->GetName() != "None")
	{
		UAkGameplayStatics::PostEvent(atksound, Primary);
	}
	isShooting = true;
}

void ACC_Ruby::SpawnSecondary()
{
	if (!isAlive) return;

	FVector MaxLocationLeft = ThirdTitty->GetComponentLocation();
	FVector MaxLocationRight = SecondTitty->GetComponentLocation();
	FVector MinLocationMiddle = MinLocation->GetComponentLocation();

	for (int i = 0; i < NumberGranades; i++)
	{
		float r1 = FMath::RandRange(0.0f, 1.0f);
		float r2 = FMath::RandRange(0.0f, 1.0f);

		float x = 0;
		float y = 0;

		x = (1 - sqrt(r1)) * MinLocationMiddle.X + (sqrt(r1) * (1 - r2)) * MaxLocationLeft.X + (sqrt(r1) * r2) * MaxLocationRight.X;
		y = (1 - sqrt(r1)) * MinLocationMiddle.Y + (sqrt(r1) * (1 - r2)) * MaxLocationLeft.Y + (sqrt(r1) * r2) * MaxLocationRight.Y;

		FVector position = FVector(x, y, 0.0f);

		FActorSpawnParameters SpawnInfo;
		SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		SpawnInfo.Owner = this;
		SpawnInfo.Instigator = this;

		FVector SpawnLocation = Charge->GetComponentLocation();
		SpawnLocation += Charge->GetComponentRotation().Vector() * 50.f;

		const float Gravity = GetWorld()->GetGravityZ();

		float Theta = (40 * PI / 180); // launch angle

		FVector dir = position - SpawnLocation; //direction
		float Sz = dir.Z;// height difference
		dir.Z = 0; // remove hight from direction
		float Sx = dir.Size();// distance
		FVector nEWlOC = SpawnLocation - position;
		FRotator newrot = nEWlOC.Rotation();

		FRotator SpawnRotation = FRotator(0, newrot.Yaw, 0);
		const float V = (Sx / cos(Theta)) * FMath::Sqrt((980.0 * 1) / (2 * (Sx * tan(Theta) - Sz)));
		FVector VelocityOutput = FVector(-V * cos(Theta), 0, V*sin(Theta));


		ACC_RubySecondary* Bomb = Cast<ACC_RubySecondary>(GetWorld()->SpawnActor(SecondaryAbility, &SpawnLocation, &SpawnRotation, SpawnInfo));
		Bomb->ProjectileMovementComponent->SetVelocityInLocalSpace(VelocityOutput);

	}


	//this->SlowCharacter(600);
}

void ACC_Ruby::SpawnParticle(UParticleSystem * SpawningParticle)
{
	FVector SpawnLocation = this->Shoot->GetComponentLocation() + (this->Shoot->GetComponentLocation().ForwardVector + 100.f);
	FRotator SpawnRotation = this->Shoot->GetForwardVector().Rotation();
	SpawnRotation += FRotator(0, 90, 0);

	if (this->GetWorld() != nullptr)
	{
		UWorld* World = this->GetWorld();

		UParticleSystemComponent* BeamComp = UGameplayStatics::SpawnEmitterAtLocation(World, SpawningParticle, SpawnLocation, SpawnRotation, true);
		BeamComp->SetRelativeScale3D(FVector(1, 1, 1)*PrimaryParticleScale);
	}
}

bool ACC_Ruby::isTargetSet(AActor* Target)
{
	if (this->PrimedEnemy != nullptr)
	{
		if (this->PrimedEnemy != Target)
		{
			return false;
		}
		return true;
	}
	
	return false;
}