// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotify.h"
#include "CC_CallSteps.generated.h"

/**
 * 
 */
UCLASS()
class CRYSTALCANNON_API UCC_CallSteps : public UAnimNotify
{
	GENERATED_BODY()

public:
	void Notify(USkeletalMeshComponent * MeshComp, UAnimSequenceBase * Animation) override;
};
