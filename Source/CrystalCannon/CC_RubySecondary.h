// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CC_Projectile.h"
#include "CC_RubySecondary.generated.h"

/**
 * 
 */
class UParticleSystemComponent;
class UParticleSystem;

UCLASS()
class CRYSTALCANNON_API ACC_RubySecondary : public ACC_Projectile
{
	GENERATED_BODY()
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit) override;

public:
	ACC_RubySecondary();

	UPROPERTY(EditAnywhere, Category = Abilities)
		TSubclassOf<class ACC_RubyExplosion>  Explosion;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Abilities")
		class UAkAudioEvent* SecondSound;

	UFUNCTION()
		void MakeExplosion();

	UParticleSystem* LandExplosion;


	virtual void Tick(float DeltaTime) override;
};
