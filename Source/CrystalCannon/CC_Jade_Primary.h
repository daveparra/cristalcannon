// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CC_Projectile.h"
#include "CC_Jade_Primary.generated.h"

/**
 * 
 */
UCLASS()
class CRYSTALCANNON_API ACC_Jade_Primary : public ACC_Projectile
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, Category = "Lifesteal")
	float HealPercent;

protected:
	//Overridden OnHit with custom functionality
	virtual void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit) override;
};
