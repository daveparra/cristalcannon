// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_SocketCrystal.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Runtime/Engine/Classes/Components/SceneComponent.h"

void UCC_SocketCrystal::Notify(USkeletalMeshComponent * SkeletalMesh, UAnimSequenceBase * Animation)
{
	Super::Notify(SkeletalMesh, Animation);
	USceneComponent * SceneComponet = SkeletalMesh->GetChildComponent(0);
	SceneComponet->SetVisibility(true);
}
