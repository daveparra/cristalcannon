// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CC_Enemy.h"
#include "CC_WizzardLizard.generated.h"

/**
 * 
 */
class UArrowComponent;
UCLASS()
class CRYSTALCANNON_API ACC_WizzardLizard : public ACC_Enemy
{
	GENERATED_BODY()

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	ACC_WizzardLizard();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Heitor)
	int MinRange = -9;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Heitor)
		bool isShooting = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Heitor)
		bool isTeleporting = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Heitor)
		bool EndTeleporting = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Heitor)
	int MaxRange = 9;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UArrowComponent* Shoot;
protected:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
