// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SplineComponent.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Engine/StaticMesh.h"
#include "CC_Fish.generated.h"

UCLASS()
class CRYSTALCANNON_API ACC_Fish : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACC_Fish(const FObjectInitializer &ObjectInitializer);

	UPROPERTY(EditAnywhere, Category = YourCategory)
		USplineComponent* SplineComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UStaticMeshComponent* FishMesh;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	float CurrentMovementSpeed = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool start = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float DefaultMovementSpeed = 1.2f;
	UPROPERTY(EditAnywhere)
		float DistanceAlongSpline = 0.f;
	FVector PositionAlongSpline;
	FRotator RotationAlongSpline;

	void MoveAlongSpline();

};
