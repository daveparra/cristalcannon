// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CC_GameMode.generated.h"

/**
 * 
 */
UCLASS()
class CRYSTALCANNON_API ACC_GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:	
	virtual void BeginPlay() override;

protected:
	UFUNCTION()
		void  GetPlayerStartPoints();
	UFUNCTION()
		void SpawnPlayers();

	UFUNCTION(BlueprintCallable)
		void DisablePlayerInput();

	UFUNCTION(BlueprintCallable)
		void EnablePlayerInput();

public:
	ACC_GameMode();
	TArray<AActor*> PlayerStarts;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerStart)
		TArray<APawn*> Players;
	int32 PlayerCount;
	TSubclassOf<APawn> PlayerOneSpawn;
	TSubclassOf<APawn> PlayerTwoSpawn;
	TSubclassOf<APawn> PlayerThreeSpawn;
	TSubclassOf<APawn> PlayerFourSpawn;

	class UCC_GameInstance* GameInstance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerStart)
	int32 JadeTag = 0;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerStart)
	int32 RubyTag = 1;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerStart)
	int32 ObsidianTag = 2;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerStart)
	int32 TopazTag = 3;
};
