// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_Obsidian_Secondary.h"
#include "CC_Enemy.h"
#include "CC_Character.h"
#include "CC_Spawner.h"
#include "TimerManager.h"
#include "Kismet/GameplayStatics.h"
#include "Components/ArrowComponent.h"
#include "DrawDebugHelpers.h"

// Sets default values
ACC_Obsidian_Secondary::ACC_Obsidian_Secondary()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	//1.2 lifetime, 0.4f
	//Super();
}
// Called when the game starts or when spawned
void ACC_Obsidian_Secondary::BeginPlay()
{
	Super::BeginPlay();

	AActor* OwningActor = this->GetOwner();
	ACC_Character* OwningChara = Cast<ACC_Character>(OwningActor);
	
	//OwningChara->SlowCharacter(600);

	if (OwningChara)
	{
		Super::InitVelocity(FVector(0,0,0));
	}

	FTimerHandle CallbackHandle;
	GetWorldTimerManager().SetTimer(CallbackHandle, this, &ACC_Obsidian_Secondary::Fire, FireDelay, false);

	StartPos = this->GetActorLocation();
	APlayerController* PC;

	if (OwningActor)
	{
		ACC_Character* Player = Cast<ACC_Character>(OwningActor);

		//Player->GetCharacterMovement()->StopMovementImmediately();

		//Disable Input
		PC = UGameplayStatics::GetPlayerController(GetWorld(), Player->PlayerIndex);
		this->DisableInput(PC);
	}

	FTimerHandle CallbackHandlerLifetime;
	GetWorldTimerManager().SetTimer(CallbackHandlerLifetime, this, &ACC_Obsidian_Secondary::AbilityEnd, BeamDuration, false);

}

void ACC_Obsidian_Secondary::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	DistTravelled = (this->GetActorLocation() - StartPos).Size();

	if (DistTravelled >= MaxDist)
	{
		this->ProjectileMovementComponent->Velocity = FVector(0, 0, 0);
	}
}

void ACC_Obsidian_Secondary::OnOverlapBegin(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (OtherActor != nullptr)
	{
		if (OtherActor->IsA(ACC_Enemy::StaticClass()))
		{
			Targets.Add(OtherActor);
		}
		else if (OtherActor->IsA(ACC_Spawner::StaticClass()))
		{
			Targets.Add(OtherActor);
		}
	}
	
}

void ACC_Obsidian_Secondary::OnOverlapEnd(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex)
{
}

void ACC_Obsidian_Secondary::Fire()
{
	AActor* OwningActor = this->GetOwner();
	ACC_Character* OwningChara = Cast<ACC_Character>(OwningActor);
	
	if (OwningChara)
	{
		Super::InitVelocity(OwningChara->Charge->GetForwardVector());
		OwningChara->ApplyFFEOnPlayerHit();
		OwningChara->ApplyShakeOnDeath();
	}
}

void ACC_Obsidian_Secondary::DamageTargets()
{
	for (AActor* Target : Targets)
	{
		if (Target != nullptr && Target->GetName() != "None")
		{
			ACC_Enemy* Enemy = Cast<ACC_Enemy>(Target);
			if (Enemy)
			{

				if (this->GetOwner() != nullptr)
				{
					Enemy->LastHit = this->GetOwner();
				}
				Enemy->ApplyDamage(Damage);
			}
			else if(ACC_Spawner* Spawner = Cast<ACC_Spawner>(Target))
			{
				Spawner->DamageSpawner();
			}
		}
	}
}

void ACC_Obsidian_Secondary::AbilityEnd()
{
	DamageTargets();

	AActor* OwningActor = this->GetOwner();
	APlayerController* PC;

	if (OwningActor)
	{
		ACC_Character* Player = Cast<ACC_Character>(OwningActor);
		PC = UGameplayStatics::GetPlayerController(GetWorld(), Player->PlayerIndex);
		this->EnableInput(PC);
		Player->ResetSpeed();
	}

	Destroy();
}
	
//
//void ACC_Obsidian_Secondary::OnOverlapBegin(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
//{
//	counter++;
//	if (counter >= TriggerAmount)
//	{
//	
//		SphereCollisionComponent->SetCollisionProfileName("BlockAll");
//	}
//}
//
//void ACC_Obsidian_Secondary::OnOverlapEnd(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex)
//{
//	counter--;
//	if (counter <= 0)
//	{
//		SphereCollisionComponent->SetCollisionProfileName("OverlapAll");
//	}
//}

