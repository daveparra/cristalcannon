// Fill out your copyright notice in the Description page of Project Settings.
#pragma once
#include "CC_GameMode.h"
#include "UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "GameFramework/Controller.h"
#include "GameFramework/PlayerStart.h"
#include "CC_Ruby.h"
#include "CC_Jade.h"
#include "CC_Obsidian.h"
#include "CC_Topaz.h"
#include "CC_CharacterExample.h"
#include "CC_GameInstance.h"


ACC_GameMode::ACC_GameMode()
{
	static ConstructorHelpers::FClassFinder<APawn> BP_Ruby(TEXT("/Game/Blueprints/BP_Ruby"));
	PlayerOneSpawn = BP_Ruby.Class;
	static ConstructorHelpers::FClassFinder<APawn> BP_Jade(TEXT("/Game/Blueprints/BP_Jade"));
	PlayerTwoSpawn = BP_Jade.Class;
	static ConstructorHelpers::FClassFinder<APawn> BP_Obsidian(TEXT("/Game/Blueprints/BP_Obsidian"));
	PlayerThreeSpawn = BP_Obsidian.Class;
	static ConstructorHelpers::FClassFinder<APawn> BP_Topaz(TEXT("/Game/Blueprints/BP_Topaz"));
	PlayerFourSpawn = BP_Topaz.Class;

	PlayerCount = 4;
}

void ACC_GameMode::BeginPlay()
{
	Super::BeginPlay();
	GameInstance = Cast<UCC_GameInstance>(GetGameInstance());
	GetPlayerStartPoints();
	SpawnPlayers();
} 

void  ACC_GameMode::GetPlayerStartPoints()
{
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayerStart::StaticClass(), PlayerStarts);
}

void  ACC_GameMode::SpawnPlayers()
{
	for (int32 index = 0; index < PlayerCount; index++)
	{
		for (int32 i = 0; i < PlayerStarts.Num(); i++)
		{
			APlayerStart* CurrentPlayerStart = Cast<APlayerStart>(PlayerStarts[i]);
			int32 PlayerStartTag = FCString::Atoi(*CurrentPlayerStart->PlayerStartTag.ToString());

			//Spawns Ruby if PlayerStartTag is 0
			if (index == PlayerStartTag && PlayerStartTag == RubyTag)
			{
				UGameplayStatics::CreatePlayer(GetWorld(), index);
				APlayerController* controller = UGameplayStatics::GetPlayerController(GetWorld(), index);
				ACC_Ruby* CharacterRuby = GetWorld()->SpawnActor<ACC_Ruby>(PlayerOneSpawn, CurrentPlayerStart->GetActorTransform());
				//ACC_Jade* CharacterRuby = GetWorld()->SpawnActor<ACC_Jade>(PlayerTwoSpawn, CurrentPlayerStart->GetActorTransform());
				controller->Possess(CharacterRuby);
				Players.AddUnique(CharacterRuby);
				CharacterRuby->PlayerIndex = index;
			}
			//Spawns Jade if PlayerStartTag is 1
			else if(index == PlayerStartTag && PlayerStartTag == JadeTag)
			{
				UGameplayStatics::CreatePlayer(GetWorld(), index);
				APlayerController* controller = UGameplayStatics::GetPlayerController(GetWorld(), index);
				ACC_Jade* CharacterJade = GetWorld()->SpawnActor<ACC_Jade>(PlayerTwoSpawn, CurrentPlayerStart->GetActorTransform());
				controller->Possess(CharacterJade);
				Players.AddUnique(CharacterJade);
				CharacterJade->PlayerIndex = index;

			}
			//Spawns Obsidian if PlayerStartTag is 2
			else if(index == PlayerStartTag && PlayerStartTag == ObsidianTag)
			{
				UGameplayStatics::CreatePlayer(GetWorld(), index);
				APlayerController* controller = UGameplayStatics::GetPlayerController(GetWorld(), index);
				ACC_Obsidian* CharacterObsidian = GetWorld()->SpawnActor<ACC_Obsidian>(PlayerThreeSpawn, CurrentPlayerStart->GetActorTransform());
				//ACC_Jade* CharacterObsidian = GetWorld()->SpawnActor<ACC_Jade>(PlayerTwoSpawn, CurrentPlayerStart->GetActorTransform());
				controller->Possess(CharacterObsidian);
				Players.AddUnique(CharacterObsidian);
				CharacterObsidian->PlayerIndex = index;
			}
			//Spawns Topaz if PlayerStartTag is 3
			else if(index == PlayerStartTag && PlayerStartTag == TopazTag)
			{
				UGameplayStatics::CreatePlayer(GetWorld(), index);
				APlayerController* controller = UGameplayStatics::GetPlayerController(GetWorld(), index);
				//ACC_Jade* CharacterTopaz = GetWorld()->SpawnActor<ACC_Jade>(PlayerTwoSpawn, CurrentPlayerStart->GetActorTransform());
				ACC_Topaz* CharacterTopaz = GetWorld()->SpawnActor<ACC_Topaz>(PlayerFourSpawn, CurrentPlayerStart->GetActorTransform());
				controller->Possess(CharacterTopaz);
				Players.AddUnique(CharacterTopaz);
				CharacterTopaz->PlayerIndex = index;
			}
		}

	}
}

void ACC_GameMode::EnablePlayerInput()
{
	for (int i = 0; i < Players.Num(); i++)
	{
		Players[i]->EnableInput(UGameplayStatics::GetPlayerController(GetWorld(), i));
	}
}

void ACC_GameMode::DisablePlayerInput()
{
	for (int i = 0; i < Players.Num(); i++)
	{
		Players[i]->DisableInput(UGameplayStatics::GetPlayerController(GetWorld(), i));
	}
}
