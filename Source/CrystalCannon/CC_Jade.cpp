// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CC_Jade.h"
#include "Engine/GameEngine.h"
#include "CC_Projectile.h"
#include "CC_Spawner.h"
#include "CC_Jade_Secondary.h"
#include "CC_Enemy.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/ArrowComponent.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "AKGameplayStatics.h"
#include "Kismet/GameplayStatics.h"

void ACC_Jade::BeginPlay()
{
	Super::BeginPlay();
}
void ACC_Jade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, "JAde" + FString::FromInt(HoldTimer));

	if (PulseActivated)
	{
		Counter += DeltaTime;

		if (Counter >= PulseInterval)
		{
			if (PulseCounter <= PulseCount)
			{
				Counter = 0;
				PulseCounter++;
				ApplyAOEDamage();

			}
			else
			{
				PulseActivated = false;
				PulseCounter = 0;
				firstTime = false;
				DestroyMark();
				this->ResetSpeed();
			}
		}
		
	}
}

void ACC_Jade::EnablePrimary()
{
	if (!this->isAlive) return;
	isShooting = true;
	if (this->CanFirePrimary)
	{
		SpawnPrimary();
		Super::EnablePrimary();
	}
}

void ACC_Jade::EnableSecondary()
{
	if (!this->isAlive) return;
	//Super::SpawnSecondary();
	
	if (CanFireSecondary)
	{
		ShootOnce = false;
		SpawnMarker();
		//this->SlowCharacter(600);
		isMarking = true;
		isCharge = true;
	}
}

void ACC_Jade::DisableSecondary()
{
	PulseActivated = true;
	isCharge = false;
	isMarking = false;
	

	Super::EnableSecondary();
	Super::DisableSecondary();

	/*CanFirePrimary = true;
	Secondary->Destroy();
	ResetSpeed();*/
}

void ACC_Jade::ApplyAOEDamage()
{
	Super::SpawnSecondary();
	if (!ShootOnce && SpawnedMark != nullptr && SpawnedMark->GetName() != "None")
	{
		UAkGameplayStatics::PostEvent(SecondSound, SpawnedMark); // When and under what conditions is this being triggered? Seems like it's triggering more than once.
		ShootOnce = true;
	}
		//Cast OverlapSphere
	TArray<TEnumAsByte<EObjectTypeQuery>> Types;
	TArray<AActor*> ActorsToIgnore;// STORES ACTORS TO BE IGNORED BY SPHERE CAST
	TArray<AActor*> OutActors;//STORES THE ACTORS IN THE SPHERE OVERLAP
	TArray<AActor*> OutActorsSpawner;//STORES THE ACTORS IN THE SPHERE OVERLAP
	TArray<AActor*> FloorArray;//STORES FLOOR ACTOR

	//Get Spawn Location
	if (SpawnedMark != nullptr)
	{
		FVector AttackLoc = this->SpawnedMark->GetActorLocation();
		
		if (ParticleFXRain && !firstTime)
		{
			ArrowRainParticle = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ParticleFXRain, FVector(AttackLoc.X, AttackLoc.Y, (AttackLoc.Z-250)),  this->GetActorRotation());
			ApplyFFEOnPlayerHit();
			firstTime = true;
		}

		UWorld* World = this->GetWorld();

		if (World == nullptr)
		{
			GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, " World NULL @ Obsidian.cpp @ CastOverlapShphere");
		}
		else
		{
			UGameplayStatics::GetAllActorsWithTag(World, "Floor", FloorArray);//GETS ALL INSTANCES WITH FLOOR TAG (WILL NEED SIMILAR STATEMENTS FOR OTHER ENVIRONMENT MESHES)

			ActorsToIgnore.Add(this);

			if (FloorArray.Num() < 1)
			{
				GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, "Floor not tagged as floor");
			}
			else
			{
				ActorsToIgnore.Add(FloorArray[0]);
			}

			UKismetSystemLibrary::SphereOverlapActors(World, AttackLoc, AOERadius, Types, ACC_Enemy::StaticClass(), ActorsToIgnore, OutActors);
			UKismetSystemLibrary::SphereOverlapActors(World, AttackLoc, AOERadius, Types, ACC_Spawner::StaticClass(), ActorsToIgnore, OutActorsSpawner);
			DrawDebugSphere(World, AttackLoc, AOERadius, 26, FColor(181, 0, 0), true, -1);
		}

		for (AActor* Enemy : OutActors)
		{
			ACC_Enemy* Current = Cast<ACC_Enemy>(Enemy);
			Current->ApplyDamage(AOEDamage);
		}

		for (AActor* Spawner : OutActorsSpawner)
		{
			ACC_Spawner* Current = Cast<ACC_Spawner>(Spawner);
			Current->DamageSpawner();
		}
	}
}

void ACC_Jade::SpawnPrimary()
{
	Super::SpawnPrimary();
	if (Primary && Primary->GetName() != "None")
	{
		UAkGameplayStatics::PostEvent(atksound, Primary);
	}
	isShooting = true;
}

void ACC_Jade::SpawnSecondary()
{
	Super::SpawnSecondary();
}

void ACC_Jade::SpawnParticle(UParticleSystem * SpawningParticle)
{
	FVector SpawnLocation = this->Shoot->GetComponentLocation() + (this->Shoot->GetComponentLocation().ForwardVector + 100.f);
	FRotator SpawnRotation = this->Shoot->GetForwardVector().Rotation();
	SpawnRotation += FRotator(0,90,0);

	if (this->GetWorld() != nullptr)
	{
		UWorld* World = this->GetWorld();

		UParticleSystemComponent* BeamComp = UGameplayStatics::SpawnEmitterAtLocation(World, SpawningParticle, SpawnLocation, SpawnRotation, true);
		BeamComp->SetRelativeScale3D(FVector(1,1,1)*PrimaryParticleScale);
	}
	//isShooting = false;
}

