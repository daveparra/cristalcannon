// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_SpawnParticle.h"
#include "CC_WizLizController.h"
#include "Components/SkeletalMeshComponent.h"
#include "CC_WizzardLizard.h"

void UCC_SpawnParticle::Notify(USkeletalMeshComponent * MeshComp, UAnimSequenceBase * Animation)
{
	Super::Notify(MeshComp, Animation);
	AActor* AnimationOwner = MeshComp->GetOwner();
	if (ACC_WizzardLizard* Character = Cast<ACC_WizzardLizard>(AnimationOwner))
	{
		if (ACC_WizLizController* Controller = Cast<ACC_WizLizController>(Character->GetController()))
		{
			Controller->SpawnActorParticle();
		}
	}
}
