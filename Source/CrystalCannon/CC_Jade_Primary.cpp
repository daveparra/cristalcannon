// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_Jade_Primary.h"
#include "CC_Unit.h"
#include "CC_Character.h"
#include "CC_Enemy.h"

void ACC_Jade_Primary::OnHit(UPrimitiveComponent * HitComponent, AActor * OtherActor, UPrimitiveComponent * OtherComponent, FVector NormalImpulse, const FHitResult & Hit)
{
	Super::OnHit(HitComponent, OtherActor, OtherComponent, NormalImpulse, Hit);

	if ((OtherActor != NULL))
	{
		if (OtherActor->IsA(ACC_Enemy::StaticClass()))
		{
			//ACC_Unit* Owner = Cast<ACC_Unit>(this->GetOwner());

			ACC_Enemy* Enemy = Cast<ACC_Enemy>(OtherActor);
			
			Enemy->ApplyDamage(this->Damage);

 
			if (this->GetOwner() != nullptr)
			{
				Enemy->LastHit = this->GetOwner();
				Cast<ACC_Character>(this->GetOwner())->ApplyFFEOnPlayerHit();
			}

			Destroy();
		}
	}
		Destroy();
}
