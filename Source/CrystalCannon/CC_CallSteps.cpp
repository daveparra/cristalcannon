// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_CallSteps.h"
#include "Components/SkeletalMeshComponent.h"
#include "CC_Jade.h"
#include "CC_Ruby.h"
#include "CC_Topaz.h"
#include "CC_Obsidian.h"

void UCC_CallSteps::Notify(USkeletalMeshComponent * MeshComp, UAnimSequenceBase * Animation)
{
	Super::Notify(MeshComp, Animation);
	AActor* AnimationOwner = MeshComp->GetOwner();
	if (ACC_Jade* Character = Cast<ACC_Jade>(AnimationOwner))
	{
		Character->CallStepsForCharacter();
	}
	else if (ACC_Obsidian* Character = Cast<ACC_Obsidian>(AnimationOwner))
	{
		Character->CallStepsForCharacter();
	}
	else if (ACC_Topaz* Character = Cast<ACC_Topaz>(AnimationOwner))
	{
		Character->CallStepsForCharacter();
	}
	else if (ACC_Ruby* Character = Cast<ACC_Ruby>(AnimationOwner))
	{
		Character->CallStepsForCharacter();
	}
}