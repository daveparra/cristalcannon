// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotify.h"
#include "CC_ThrowCrystal.generated.h"

/**
 * 
 */
class ACC_ShootingCrystal;
UCLASS()
class CRYSTALCANNON_API UCC_ThrowCrystal : public UAnimNotify
{
	GENERATED_BODY()

	virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;
};
