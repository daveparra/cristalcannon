// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_WizLizController.h"
#include "CC_ShootingCrystal.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/PawnSensingComponent.h."
#include "CC_Character.h"
#include "CC_WizzardLizard.h"
#include "Engine/GameEngine.h"
#include "Engine.h"
#include "DrawDebugHelpers.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "Runtime/NavigationSystem/Public/NavigationSystem.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemComponent.h"
#include "Components/ArrowComponent.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "CC_MovePlace.h"
#include "Runtime/Engine/Classes/Components/BoxComponent.h"

ACC_WizLizController::ACC_WizLizController()
{
	PrimaryActorTick.bCanEverTick = true;
	PawnSensor = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("Pawn Sensor"));
	SetPerceptionComponent(*CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("AIPerception Component")));
	PawnSensor->SensingInterval = .25f; // 4 times per second
	PawnSensor->bOnlySensePlayers = false;
	PawnSensor->SetPeripheralVisionAngle(35.f);
	PawnSensor->SightRadius = 1000.0f;

	static ConstructorHelpers::FObjectFinder<UParticleSystem> Particle(TEXT("/Game/Art/Particles/PS_LizBeamTest.PS_LizBeamTest"));
	ParticleFX1 = Particle.Object;

	PawnSensor->OnSeePawn.AddDynamic(this, &ACC_WizLizController::OnSeePawn);

	radius = 600.0f;
}

void ACC_WizLizController::OnSeePawn(APawn * OtherPawn)
{
	ACC_Character* Player = Cast<ACC_Character>(OtherPawn);
	if (!Cast<ACC_WizzardLizard>(GetPawn())->isTeleporting && !Cast<ACC_WizzardLizard>(GetPawn())->isShooting)
	{
		if (Player != nullptr && Player->isAlive)
		{
			CharacterPositon = OtherPawn->GetActorLocation();
		}
		else
		{
			CharacterPositon = FVector::ZeroVector;
		}
	}
}

void ACC_WizLizController::Possess(APawn * Pawn)
{
	Super::Possess(Pawn);
}

FRotator ACC_WizLizController::GetControlRotation() const
{
	if (GetPawn() == nullptr)
	{
		return FRotator(0.0f, 0.0f, 0.0f);
	}

	return FRotator(0.0f, GetPawn()->GetActorRotation().Yaw, 0.0f);
}

void ACC_WizLizController::BeginPlay()
{
	Super::BeginPlay();
}

void ACC_WizLizController::ResetParms()
{
	Spawner->Destroy();
	TArray<AActor*> MovingPlaces;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ACC_MovePlace::StaticClass(), MovingPlaces);
	if (MovingPlaces.Num() > 0)
	{
		int RandomPlace = FMath::RandRange(0, MovingPlaces.Num() - 1);
		GetPawn()->SetActorLocation(MovingPlaces[RandomPlace]->GetActorLocation());
		GetPawn()->SetActorRotation(MovingPlaces[RandomPlace]->GetActorRotation());
		Cast<ACC_WizzardLizard>(GetPawn())->EndTeleporting = false;
		CharacterPositon = FVector::ZeroVector;
	}
	Cast<ACC_WizzardLizard>(GetPawn())->isTeleporting = false;
	CharacterPositon = FVector::ZeroVector;
}

void ACC_WizLizController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	if ((Cast<ACC_WizzardLizard>(GetPawn())->GetCurrentHealth() > 0.0f))
	{
		FRotator Rotation = GetPawn()->GetActorRotation();
		FRotator NewRotation;
		if (!CharacterPositon.Equals(FVector::ZeroVector))
		{
			FVector VectorRetult = CharacterPositon - GetPawn()->GetActorLocation();

			GetPawn()->SetActorRotation(VectorRetult.Rotation());
			Cast<ACC_WizzardLizard>(GetPawn())->isShooting = true;
			CharacterPositon = FVector::ZeroVector;
			ACC_WizzardLizard* Character = Cast<ACC_WizzardLizard>(GetPawn());
			Beam = UGameplayStatics::SpawnEmitterAttached(
				ParticleFX1, //UParticleSystem*  
				Character->GetMesh(),
				FName("Beam"),
				FVector(0, 0, 0),
				FRotator(0, 0, 0),
				EAttachLocation::SnapToTarget,
				true
			);
		}
		else if(!Cast<ACC_WizzardLizard>(GetPawn())->isTeleporting && !Cast<ACC_WizzardLizard>(GetPawn())->isShooting)
		{
			if (timer < 0)
			{
				if (times == Cast<ACC_WizzardLizard>(GetPawn())->MaxRange)
				{
					goback = true;
				}
				if (times < Cast<ACC_WizzardLizard>(GetPawn())->MaxRange && !goback)
				{
					NewRotation =FRotator(Rotation.Pitch, Rotation.Yaw - 10.f, Rotation.Roll);
					times += 1;
				}
				else if(times <= Cast<ACC_WizzardLizard>(GetPawn())->MaxRange && goback)
				{ 
					NewRotation = FRotator(Rotation.Pitch, Rotation.Yaw + 10.f, Rotation.Roll);
					times -= 1;
				}
				if (times == Cast<ACC_WizzardLizard>(GetPawn())->MinRange && goback)
				{
					goback = false;
				}
				timer = 0.5f;
				GetPawn()->SetActorRotation(NewRotation);
			}
			timer -= DeltaSeconds;
		}
		else if (Cast<ACC_WizzardLizard>(GetPawn())->EndTeleporting)
		{
			timer = 0.6f;
		}
		else if (Cast<ACC_WizzardLizard>(GetPawn())->isShooting)
		{
			ACC_WizzardLizard* Character = Cast<ACC_WizzardLizard>(GetPawn());
			
			FHitResult OutHit;
			FVector Start = Character->GetMesh()->GetSocketLocation("Beam") + Character->GetMesh()->GetSocketRotation("Beam").Vector();

			// alternatively you can get the camera location
			//FRotator Rotation = Character->GetMesh()->GetSocketRotation("Beam");
			//FVector ForwardVector = FRotationMatrix(GetPawn()->GetControlRotation()).GetScaledAxis(EAxis::X);
			FVector End = Character->GetMesh()->GetSocketLocation("Beam") + Character->GetMesh()->GetSocketRotation("Beam").Vector() * 1850;
			FCollisionQueryParams CollisionParams(FName(""),false,GetOwner());

			//DrawDebugLine(GetWorld(), Start, End, FColor::Green, false, 1, 0, 1);

			if (GetWorld()->LineTraceSingleByChannel(OUT OutHit,Start,End,ECollisionChannel::ECC_WorldDynamic, CollisionParams))
			{
				if (OutHit.bBlockingHit)
				{
					if (Cast<ACC_Character>(OutHit.GetActor()))
					{
						Cast<ACC_Character>(OutHit.GetActor())->ApplyDamage(BeamDamage);
					}
					//GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, FString::Printf(TEXT("You are hitting: %s"), *OutHit.GetActor()->GetName()));
				}
			}
		}
		
	}
	else
	{
		GetPawn()->Destroy();
		Destroy();
	}

}

void ACC_WizLizController::SpawnActorParticle()
{


	FActorSpawnParameters SpawnInfo;
	FVector SpawnLocation = GetPawn()->GetActorLocation();
	FRotator SpawnRotation = GetPawn()->GetActorRotation();
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnInfo.Owner = GetPawn();
	SpawnInfo.Instigator = GetInstigator();

	Spawner = GetWorld()->SpawnActor(Particle, &SpawnLocation, &SpawnRotation, SpawnInfo);
	times = 0;
	
	FTimerDelegate Timer;
	FTimerHandle TimerH;
	Timer.BindUFunction(this, FName("ResetParms"));
	GetWorldTimerManager().SetTimer(TimerH, Timer, 2.8f, false);
}