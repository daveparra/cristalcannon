// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_SplitterCharacter.h"
#include "Components/ArrowComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "CC_Character.h"
#include "AKGameplayStatics.h"

// Sets default values
ACC_SplitterCharacter::ACC_SplitterCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;
	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 600.0f, 0.0f);
	Shoot = Cast<UArrowComponent>(GetDefaultSubobjectByName(TEXT("Shooting")));

	VisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("VisionSphere"));
	VisionSphere->AttachTo(RootComponent);
	VisionSphere->SetSphereRadius(VisionRadius);

	VisionSphere->OnComponentBeginOverlap.AddDynamic(this, &ACC_SplitterCharacter::OnBoxBeginOverlap);
	VisionSphere->OnComponentEndOverlap.AddDynamic(this, &ACC_SplitterCharacter::OnBoxEndOverlap);
}

void ACC_SplitterCharacter::DoExplosion(AActor * OtherActor)
{
	isKanikase = true;
	Cast<ACC_Character>(OtherActor)->ApplyDamage(500.0f);
}

void ACC_SplitterCharacter::OnBoxBeginOverlap(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (Cast<ACC_Character>(OtherActor) && Cast<ACC_Character>(OtherActor)->GetCurrentHealth() >= 0.0f)
	{
		UStaticMeshComponent* MEshComp = Cast<UStaticMeshComponent>(GetDefaultSubobjectByName(TEXT("StaticMesh2")));
		MEshComp->SetMaterial(0, ExplosionMaterial);

		FTimerDelegate Timer;
		FTimerHandle TimerH;
		Timer.BindUFunction(this, FName("DoExplosion"), OtherActor);
		GetWorldTimerManager().SetTimer(TimerH, Timer, 0.4f, false);
	}
}

void ACC_SplitterCharacter::OnBoxEndOverlap(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex)
{
	if (Cast<ACC_Character>(OtherActor) && Cast<ACC_Character>(OtherActor)->GetCurrentHealth() >= 0.0f)
	{
		isKanikase = false;
	}
}

// Called when the game starts or when spawned
void ACC_SplitterCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACC_SplitterCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ACC_SplitterCharacter::PlayDeadSound()
{
	FString SwitchGroup = "Enemy_Feedback";
	FString SwitchState = "Death";
	UAkGameplayStatics::SetOutputBusVolume(15.0F, this);
	UAkGameplayStatics::SetSwitch(FName(*SwitchGroup), FName(*SwitchState), this);
	UAkGameplayStatics::PostEvent(SplitterSound, this);
}

void ACC_SplitterCharacter::PlayAttackSound()
{
	FString SwitchGroup = "Enemy_Feedback";
	FString SwitchState = "Attack";
	UAkGameplayStatics::SetOutputBusVolume(15.0F, this);
	UAkGameplayStatics::SetSwitch(FName(*SwitchGroup), FName(*SwitchState), this);
	UAkGameplayStatics::PostEvent(SplitterSound, this);
}

// Called to bind functionality to input
void ACC_SplitterCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

