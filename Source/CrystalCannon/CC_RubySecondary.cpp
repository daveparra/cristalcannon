// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_RubySecondary.h"
#include "CC_Ruby.h"
#include "Components/StaticMeshComponent.h"
#include "CC_Enemy.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemComponent.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Kismet/KismetMathLibrary.h"
#include "CC_RubyExplosion.h"
#include "AKGameplayStatics.h"

ACC_RubySecondary::ACC_RubySecondary()
{
	static ConstructorHelpers::FObjectFinder<UParticleSystem> ParticleTwo(TEXT("/Game/Art/Particles/Ruby/PS_RubyExplosion_P.PS_RubyExplosion_P"));
	LandExplosion = ParticleTwo.Object;
}

void ACC_RubySecondary::BeginPlay()
{
	Super::BeginPlay();

}

void ACC_RubySecondary::OnHit(UPrimitiveComponent * HitComponent, AActor * OtherActor, UPrimitiveComponent * OtherComponent, FVector NormalImpulse, const FHitResult & Hit)
{
	MakeExplosion();
}


void ACC_RubySecondary::MakeExplosion()
{
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnInfo.Owner = this;
	SpawnInfo.Instigator = GetInstigator();

	UAkGameplayStatics::PostEvent(SecondSound, this);

	FVector SpawnLocation = GetActorLocation();
	FRotator SpawnRotation = GetActorRotation();
	ACC_RubyExplosion* Bomb = Cast<ACC_RubyExplosion>(GetWorld()->SpawnActor(Explosion, &SpawnLocation, &SpawnRotation, SpawnInfo));
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), LandExplosion, FVector(SpawnLocation.X, SpawnLocation.Y, SpawnLocation.Z +100.f), GetActorRotation(), FVector(1.f, 1.f, 1.f), true);
	
	//FFE and screenshake
	if (ACC_Ruby* Ruby = Cast<ACC_Ruby>(this->GetOwner()))
	{
		Ruby->ApplyFFEOnPlayerHit();
		Ruby->ApplyShakeOnDeath();
		Ruby->ResetSpeed();
	}

	Destroy();
}

void ACC_RubySecondary::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
