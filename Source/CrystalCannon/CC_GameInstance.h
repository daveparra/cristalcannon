// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "CC_GameInstance.generated.h"

/**
 * 
 */

USTRUCT(Blueprintable)
struct FPlayerData
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 Kills;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float DamageDealt;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 Revives;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 Deaths;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float DamageTaken;

	FPlayerData()
	{
		Kills = 0;
		DamageDealt = 0.f;
		Revives = 0;
		Deaths = 0;
		DamageTaken = 0.f;
	}
};

UCLASS()
class CRYSTALCANNON_API UCC_GameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	
	UCC_GameInstance();

	UPROPERTY(BlueprintReadOnly)
	int32 DeadPlayerCount = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerStart)
	int32 JadeTag = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerStart)
	int32 RubyTag = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerStart)
	int32 ObsidianTag = 2;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerStart)
	int32 TopazTag = 3;

	UPROPERTY(BlueprintReadWrite)
	TArray<FPlayerData> Data;
	
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void GameOverEvent();

	UFUNCTION(BlueprintCallable)
	int32 GetDeadPlayerCount() { return DeadPlayerCount; }
	
	void IncrementDeadPlayerCount();
	
	void DecrementDeadPlayerCount() { this->DeadPlayerCount--; }

	UFUNCTION()
	void UpdateKills(int32 index);

	UFUNCTION()
	void UpdateDamageDealt(int32 index, float Damage);
	
	UFUNCTION()
	void UpdateRevives(int32 index);
	
	UFUNCTION()
	void UpdateDeaths(int32 index);
	
	UFUNCTION()
	void UpdateDamageTaken(int32 index, float Damage);

};
