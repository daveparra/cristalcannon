// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_ShootingCrystal.h"
#include "Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "EngineUtils.h"
#include "CC_Unit.h"

ACC_ShootingCrystal::ACC_ShootingCrystal() 
{
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->AttachTo(RootComponent);

	static ConstructorHelpers::FObjectFinder<UParticleSystem> Particle(TEXT("/Game/Art/Particles/PS_ElfImpact.PS_ElfImpact"));
	ParticleFX1 = Particle.Object;
	
}
void ACC_ShootingCrystal::BeginPlay()
{
	Super::BeginPlay();
}

void ACC_ShootingCrystal::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ACC_ShootingCrystal::OnHit(UPrimitiveComponent * HitComponent, AActor * OtherActor, UPrimitiveComponent * OtherComponent, FVector NormalImpulse, const FHitResult & Hit)
{
	//Super::OnHit(HitComponent, OtherActor, OtherComponent, NormalImpulse, Hit);
	if ((OtherActor != NULL))
	{
		ACC_Unit* Enemy = Cast<ACC_Unit>(OtherActor);
		if (Enemy != nullptr)
		{
			Enemy->ApplyDamage(this->Damage);
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ParticleFX1, GetActorLocation());
			Destroy();

		}
	}
	Destroy();
}
