// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_Projectile.h"
#include "CC_Character.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/ArrowComponent.h"
// Sets default values
ACC_Projectile::ACC_Projectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CapsuleComponent = CreateDefaultSubobject<UCapsuleComponent>(TEXT("CapsuleComponent"));

	RootComponent = CapsuleComponent;

	CapsuleComponent->InitCapsuleSize(90, 170);
	CapsuleComponent->SetCollisionProfileName("Destructable");
	CapsuleComponent->OnComponentHit.AddDynamic(this, &ACC_Projectile::OnHit);
	CapsuleComponent->OnComponentBeginOverlap.AddDynamic(this, &ACC_Projectile::OnOverlapBegin);
	CapsuleComponent->OnComponentEndOverlap.AddDynamic(this, &ACC_Projectile::OnOverlapEnd);
	

	CapsuleComponent->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
	CapsuleComponent->CanCharacterStepUpOn = ECB_No;

	CapsuleComponent->MoveIgnoreActors.Add(this);

	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovementComponent"));
	ProjectileMovementComponent->UpdatedComponent = CapsuleComponent;
	ProjectileMovementComponent->MaxSpeed = Velocity;
	ProjectileMovementComponent->InitialSpeed = Velocity;
	ProjectileMovementComponent->ProjectileGravityScale = 0.f;
	ProjectileMovementComponent->bRotationFollowsVelocity = true;
	ProjectileMovementComponent->bShouldBounce = false;
}

// Called every frame
void ACC_Projectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	CurrentLoc = GetActorLocation();
	
	//RANGE CODE WIP
	//if (SqrMag(StartLoc, CurrentLoc) > MaxRange*MaxRange)
	//{
		//Destroy();
	//}
}


// Called when the game starts or when spawned
void ACC_Projectile::BeginPlay()
{
	Super::BeginPlay();

	StartLoc = GetActorLocation();
	CurrentLoc = GetActorLocation();

	ACC_Character* Owner = Cast<ACC_Character>(this->GetOwner());
	
	//USkeletalMeshComponent* myMesh = Owner->GetMesh();
	//FRotator rot = myMesh->GetComponentRotation()
	InitVelocity(this->GetActorRotation().Vector());
}


void ACC_Projectile::OnHit(UPrimitiveComponent * HitComponent, AActor * OtherActor, UPrimitiveComponent * OtherComponent, FVector NormalImpulse, const FHitResult & Hit)
{
	if ((OtherActor != NULL))
	{
		//if (HitComponent->IsSimulatingPhysics())
		//{
		//	this->CapsuleComponent->AddImpulseAtLocation(FVector(100,100,100), this->GetActorLocation());
		//	Destroy();
		//}

		if (OtherActor->IsA(ACC_Character::StaticClass()))
		{	
			if (this->GetOwner() != nullptr)
			{
				return;
			}
			else
			{
				Destroy();
			}
		}
	}
}


void ACC_Projectile::InitVelocity(const FVector & Direction)
{
	this->ProjectileMovementComponent->InitialSpeed = Velocity;
	this->ProjectileMovementComponent->Velocity = Direction * this->ProjectileMovementComponent->InitialSpeed;
}

float ACC_Projectile::SqrMag(FVector A, FVector B)
{
	float MagA = A.Size2D();
	float MagB = B.Size2D();
	return MagA*MagB;
}

void ACC_Projectile::OnOverlapBegin(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
}

void ACC_Projectile::OnOverlapEnd(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex)
{
}

