// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "CC_WizLizController.generated.h"

/**
 * 
 */
class ACC_ShootingCrystal;
class UPawnSensingComponent;
class AActor;
class UParticleSystem;
class UParticleSystemComponent;
UCLASS()
class CRYSTALCANNON_API ACC_WizLizController : public AAIController
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

	virtual void Possess(APawn* Pawn) override;

	virtual FRotator GetControlRotation() const override;

	float timer = 0.5f;
	int times = 0;
	bool goback = false;

public:
	ACC_WizLizController();


	FVector CharacterPositon;
	AActor* Spawner;
	UParticleSystem* ParticleFX1;
	UParticleSystemComponent* Beam;

	virtual void Tick(float DeltaSeconds) override;

	UPROPERTY(EditAnywhere, Category = Debug)
		float FR = 3.0f;
	UPROPERTY(EditAnywhere, Category = Debug)
		float MR = 1.5f;
	UPROPERTY(EditAnywhere, Category = Debug)
		float BeamDamage = 100.f;

	UPROPERTY(EditAnywhere, Category = Abilities)
		float MovementRadius = 1000.f;

	UPROPERTY(EditAnywhere, Category = Abilities)
		float FireDistance = 150.f;

	UPROPERTY(EditAnywhere, Category = Abilities)
		TSubclassOf<ACC_ShootingCrystal> CrystalToUse;
	

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Awareness)
		float radius = 0.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Awareness)
	UPawnSensingComponent* PawnSensor;

	UPROPERTY(EditAnywhere, Category = Debug)
		float DistanceForRange = 1500.f;

	UFUNCTION()
		void OnSeePawn(APawn *OtherPawn);

	UFUNCTION()
		void ResetParms();

	UFUNCTION()
		void SpawnActorParticle();

	UPROPERTY(EditAnywhere)
		TSubclassOf<AActor> Particle;
};
