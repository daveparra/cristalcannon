// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CC_Projectile.h"
#include "CC_Topaz_Primary.generated.h"

/**
 * 
 */
UCLASS()
class CRYSTALCANNON_API ACC_Topaz_Primary : public ACC_Projectile
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, Category = "Ablities")
		float Duration = 5.f;

	UPROPERTY(EditAnywhere, Category = "Ablities")
		float CallbackSpeed = 0;

	bool isReturning;
	FVector TargetDir;
	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:

	virtual void BeginPlay() override;

	//Called when something enters sphere  
		virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	
	//Called when something leaves sphere
		virtual void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
	void ReturnToOwner();//Returns projectile to owner;
};
