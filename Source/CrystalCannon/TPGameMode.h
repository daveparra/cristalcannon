// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TPGameMode.generated.h"

/**
 * 
 */
UCLASS()
class CRYSTALCANNON_API ATPGameMode : public AGameModeBase
{
	GENERATED_BODY()
	virtual void StartPlay() override;
	ATPGameMode(const FObjectInitializer& ObjectInitializer);
};
