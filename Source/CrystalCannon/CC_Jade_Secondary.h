// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CC_Projectile.h"
#include "GameFramework/Actor.h"
#include "Components/SphereComponent.h"
#include "CC_Jade_Secondary.generated.h"

UCLASS()
class CRYSTALCANNON_API ACC_Jade_Secondary : public ACC_Projectile
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere)
	bool isTier3 = false;

	virtual void BeginPlay() override;

	//Called when something enters sphere  
	virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

	//Called when something leaves sphere
	virtual void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) override;


};
