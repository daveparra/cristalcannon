// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_GruntCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstanceDynamic.h"
#include "Engine/GameEngine.h"

ACC_GruntCharacter::ACC_GruntCharacter()
{
	PrimaryActorTick.bCanEverTick = true;
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;
	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 600.0f, 0.0f);
}

void ACC_GruntCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	DynamicMatInstance = GetMesh()->CreateAndSetMaterialInstanceDynamic(0);
}

void ACC_GruntCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	if (Change)
	{
		lerpValue += 0.01f;
		DynamicMatInstance->SetScalarParameterValue(FName("Erosion"), lerpValue);
		GetMesh()->SetMaterial(0,DynamicMatInstance);
		FString TheFloatStr = FString::SanitizeFloat(lerpValue);
		GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, TheFloatStr);
	}
	if (lerpValue >= 4.5f)
	{
		Change = false;
		Destroy();
	}
	
}

void ACC_GruntCharacter::SetupPlayerInputComponent(UInputComponent * PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}


