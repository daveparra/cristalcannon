// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CC_Avoid.generated.h"

UCLASS()
class CRYSTALCANNON_API ACC_Avoid : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACC_Avoid();
	// Sets default values for this actor's properties
	UPROPERTY(Category = AI, EditAnywhere, BlueprintReadWrite)
		float Value;

	bool vanish = false;
	float timer = 1.f;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
