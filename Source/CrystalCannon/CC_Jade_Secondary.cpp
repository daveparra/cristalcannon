// Fill out your copyright notice in the Description page of Project Settings.
#pragma once

#include "CC_Jade_Secondary.h"
#include "CC_Character.h"
#include "CC_Enemy.h"
#include "Engine/GameEngine.h"

void ACC_Jade_Secondary::BeginPlay()
{
	Super::BeginPlay();
}

void ACC_Jade_Secondary::OnOverlapBegin(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	
	if ((OtherActor != NULL))
	{
		if (OtherActor->IsA(ACC_Enemy::StaticClass()))
		{
			//ACC_Unit* Owner = Cast<ACC_Unit>(this->GetOwner());

			ACC_Enemy* Enemy = Cast<ACC_Enemy>(OtherActor);

			Enemy->ApplyDamage(this->Damage);


			if (this->GetOwner() != nullptr)
			{
				Enemy->LastHit = this->GetOwner();
			}

			Destroy();
		}

		if (isTier3)
		{
			return;
		}
	}


}

void ACC_Jade_Secondary::OnOverlapEnd(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex)
{

}
