// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CC_Enemy.h"
#include "CC_GruntCharacter.generated.h"

/**
 * 
 */
UCLASS()
class CRYSTALCANNON_API ACC_GruntCharacter : public ACC_Enemy
{
	GENERATED_BODY()
public:
	ACC_GruntCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:

	bool Change = false;
	float lerpValue = 0.0f;
	class UMaterialInstanceDynamic* DynamicMatInstance;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool IsAttacking;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool IsIdle;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		bool IsWalking;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		bool IsDead;
	

};
