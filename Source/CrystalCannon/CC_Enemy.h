// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CC_Unit.h"
#include "CC_Character.h"
#include "CC_Enemy.generated.h"

/**
 * 
 */
UCLASS()
class CRYSTALCANNON_API ACC_Enemy : public ACC_Unit
{
	GENERATED_BODY()

public:

	bool killedByExplosion = false;

	int32 KillIndex;

	ACC_Character* KillingCharacter;

	virtual void BeginPlay();

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) ;
	
	virtual void ApplyDamage(float Damage) override;
};
