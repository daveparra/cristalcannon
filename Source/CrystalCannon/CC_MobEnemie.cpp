// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_MobEnemie.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/SphereComponent.h"
#include "Runtime/Engine/Classes/Kismet/KismetSystemLibrary.h"
#include "CC_Character.h"
#include "CC_Avoid.h"
#include "Components/ArrowComponent.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "AKGameplayStatics.h"


// Sets default values
ACC_MobEnemie::ACC_MobEnemie()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;
	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 600.0f, 0.0f);
	BaseMovementSpeed = 200.0f;
	MaxMovementSpeed = 300.0f;
	AlignmentWeight = 1.0f;
	CohesionWeight = 0.5f;
	SeparationWeight = 4.0f;
	VisionRadius = 300.0f;

	AgentPhysicalRadius = 45.0f;

	// Create vision sphere
	VisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("VisionSphere"));
	VisionSphere->AttachTo(RootComponent);
	VisionSphere->SetSphereRadius(VisionRadius);

	

	Timer = TimertoRandom;

	NewDirection = FVector::ZeroVector;
	Deflection = FVector::ZeroVector;
}

// Called when the game starts or when spawned
void ACC_MobEnemie::BeginPlay()
{
	Super::BeginPlay();
	Left = Cast<UArrowComponent>(GetDefaultSubobjectByName(TEXT("Arrow1")));
	Right = Cast<UArrowComponent>(GetDefaultSubobjectByName(TEXT("Arrow2")));
	Back = Cast<UArrowComponent>(GetDefaultSubobjectByName(TEXT("Arrow3")));
	// Initialize move vector
	NewMoveVector = GetActorRotation().Vector().GetSafeNormal();
}

void ACC_MobEnemie::PlayDeadSound()
{
	FString SwitchGroup = "Enemy_Feedback";
	FString SwitchState = "Death";
	UAkGameplayStatics::SetOutputBusVolume(15.0F, this);
	UAkGameplayStatics::SetSwitch(FName(*SwitchGroup),FName(*SwitchState), this);
	UAkGameplayStatics::PostEvent(MobSound, this);
}

void ACC_MobEnemie::PlayAttackSound()
{
	FString SwitchGroup = "Enemy_Feedback";
	FString SwitchState = "Attack";
	UAkGameplayStatics::SetSwitch(FName(*SwitchGroup), FName(*SwitchState), this);
	UAkGameplayStatics::PostEvent(MobSound, this);
}

// Called every frame
void ACC_MobEnemie::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (Timer < 0) 
	{
		RandomMove(FMath::RandRange(0,2));
		Timer = TimertoRandom;
	}
	Timer -= DeltaTime;
	if (!isAttacking) 
	{
		CurrentMoveVector = NewMoveVector;

		BeforeCalculate();

		NewDirection = (NewMoveVector * BaseMovementSpeed * DeltaTime).GetClampedToMaxSize(MaxMovementSpeed * DeltaTime);
		NewRotation = NewMoveVector.Rotation();

		FHitResult Hit(1.f);
		RootComponent->MoveComponent(NewDirection, NewRotation, true, &Hit);
		FLatentActionInfo LatentInfo;
		LatentInfo.CallbackTarget = this;
		//UKismetSystemLibrary::MoveComponentTo(RootComponent, NewDirection, NewRotation, false,false,0.1f,false,EMoveComponentAction::Type::Move,LatentInfo);
		if (Hit.IsValidBlockingHit())
		{
			const FVector Normal2D = Hit.Normal.GetSafeNormal2D();
			Deflection = FVector::VectorPlaneProject(NewDirection, Normal2D) * (1.f - Hit.Time);
			RootComponent->MoveComponent(Deflection, NewRotation, true);
			//AddMovementInput(Deflection);
		}

	}
	if (CurrentHealth < 0)
	{
		if (killedByExplosion)
		{

			PlayDeadSound();
			SpawnExplosionDeathParticle();
			Destroy();
		}

		SpawnDeathParticle();
		//ApplyShakeOnDeath();
		PlayDeadSound();
		Destroy();
		
	}
}

// Called to bind functionality to input
void ACC_MobEnemie::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ACC_MobEnemie::ResetComponents()
{
	AlignmentComponent = FVector::ZeroVector;
	CohesionComponent = FVector::ZeroVector;
	SeparationComponent = FVector::ZeroVector;
	NegativeStimuliComponent = FVector::ZeroVector;
	NegativeStimuliMaxFactor = 0.0f;
	PositiveStimuliComponent = FVector::ZeroVector;
	PositiveStimuliMaxFactor = 0.0f;
}

void ACC_MobEnemie::BeforeCalculate()
{
	ResetComponents();
	VisionSphere->GetOverlappingActors(Neighbourhood, ClassFilter);
	for (int32 i = 0; i < Neighbourhood.Num(); i++)
	{
		//Alignment componennt
		ACC_MobEnemie* Mob = Cast<ACC_MobEnemie>(Neighbourhood[i]);
		if (Mob)
		{
			Mob->CurrentMoveVector.Normalize();
			AlignmentComponent =  Mob->CurrentMoveVector + AlignmentComponent;
		}
	}
	AlignmentComponent.Normalize();
	int32 Exist = Neighbourhood.Find(this);
	Neighbourhood.RemoveAt(Exist);
	if (Neighbourhood.Num() > 0)
	{
		//Cohesion component
		for (int32 i = 0; i < Neighbourhood.Num(); i++)
		{
			CohesionComponent = CohesionComponent + (Neighbourhood[i]->GetActorLocation() - this->GetActorLocation());
		}
		CohesionComponent = ((CohesionComponent / float(Neighbourhood.Num())) / 100.0f);
		//separation component
		for (int32 i = 0; i < Neighbourhood.Num(); i++)
		{
			FVector res = this->GetActorLocation() - Neighbourhood[i]->GetActorLocation();
			float division = abs((res.Size() - (2 * AgentPhysicalRadius)));
			res.Normalize();
			SeparationComponent = SeparationComponent + (res / division);
		}
		SeparationComponent = ((SeparationComponent * 100.0f) + ((SeparationComponent * 100.0f) * (5.0f / float(Neighbourhood.Num()))));
		GetAvoidOrEnemie();
		
	}
	else
	{
		GetAvoidOrEnemie();
	}
}

void ACC_MobEnemie::GetAvoidOrEnemie()
{
	VisionSphere->GetOverlappingActors(Others);
	for (int32 i = 0; i < Others.Num(); i++)
	{
		ACC_Character* Enemy = Cast<ACC_Character>(Others[i]);
		if (Enemy)
		{
			if (Enemy->GetCurrentHealth() > 0)
			{
				GetEnemy(Enemy);
			}
		}
		else
		{
			ACC_Avoid* Avoid = Cast<ACC_Avoid>(Others[i]);
			GetAvoid(Avoid);
		}
	}
	CalculateNewMoveVector();
}

void ACC_MobEnemie::RandomMove(int Place)
{
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnInfo.Owner = this;
	SpawnInfo.Instigator = GetInstigator();
	FVector Location;
	FRotator Rotation;
	if (Place == 0) 
	{
		Location = Left->GetComponentLocation();
		Rotation = Left->GetComponentRotation();
		ACC_Avoid* tempActor = Cast<ACC_Avoid>(GetWorld()->SpawnActor(SpawnAvoid, &Location, &Rotation, SpawnInfo));
		tempActor->vanish = true;
	}
	else if (Place == 1) 
	{
		Location = Right->GetComponentLocation();
		Rotation = Right->GetComponentRotation();
		ACC_Avoid* tempActor = Cast<ACC_Avoid>(GetWorld()->SpawnActor(SpawnAvoid, &Location, &Rotation, SpawnInfo));
		tempActor->vanish = true;
	}
	else if (Place == 2)
	{
		Location = Back->GetComponentLocation();
		Rotation = Back->GetComponentRotation();
		ACC_Avoid* tempActor = Cast<ACC_Avoid>(GetWorld()->SpawnActor(SpawnAvoid, &Location, &Rotation, SpawnInfo));
		tempActor->vanish = true;
	}
}

void ACC_MobEnemie::CalculateNewMoveVector()
{
	NegativeStimuliComponent.Normalize();
	NegativeStimuliComponent = NegativeStimuliComponent * NegativeStimuliMaxFactor;
	if (PositiveStimuliComponent.Equals(FVector::ZeroVector))
	{
		isRunning = false;
		isAttacking = false;
	}
	else 
	{
		isAttacking = false;
	}
	FVector Result;
	FVector AlignmentNoZ = FVector(AlignmentComponent.X, AlignmentComponent.Y, 0.0f);
	AlignmentNoZ *= AlignmentWeight;
	FVector CohesionNoZ = FVector(CohesionComponent.X, CohesionComponent.Y, 0.0f);
	CohesionNoZ *= CohesionWeight;
	FVector SeparationNoZ = FVector(SeparationComponent.X, SeparationComponent.Y, 0.0f);
	SeparationNoZ *= SeparationWeight;
	FVector PositiveNoZ = FVector(PositiveStimuliComponent.X, PositiveStimuliComponent.Y, 0.0f);
	FVector NegativeNoZ = FVector(NegativeStimuliComponent.X, NegativeStimuliComponent.Y, 0.0f);

	Result = AlignmentNoZ + CohesionNoZ + SeparationNoZ + PositiveNoZ + NegativeNoZ;
	NewMoveVector = Result;
}

void ACC_MobEnemie::GetAvoid(ACC_Avoid* Avoid)
{
	if (Avoid)
	{
		FVector Location = Avoid->GetActorLocation() - this->GetActorLocation();
		float ResAbs = abs(Location.Size() - AgentPhysicalRadius);
		Location.Normalize();
		Location /= ResAbs;
		Location *= 100.0f;
		Location *= Avoid->Value;
		NegativeStimuliComponent = Location + NegativeStimuliComponent;
		NegativeStimuliMaxFactor = FMath::Max3(Location.Size(), NegativeStimuliMaxFactor, 0.0f);
	}
}

void ACC_MobEnemie::GetEnemy(ACC_Character* Enemy)
{
	FVector Location = Enemy->GetActorLocation() - this->GetActorLocation();
	float res = Enemy->Value / Location.Size();
	if (res > PositiveStimuliMaxFactor)
	{
		Location.Normalize();
		PositiveStimuliComponent = Location * Enemy->Value;
		PositiveStimuliMaxFactor = res;
		isRunning = true;
	}
}

