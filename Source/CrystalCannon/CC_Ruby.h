// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CC_Character.h"
#include "CC_Ruby.generated.h"

/**
 *
 */
UCLASS()
class CRYSTALCANNON_API ACC_Ruby : public ACC_Character
{
	GENERATED_BODY()

public:

	virtual void Tick(float DeltaTime) override;
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Abilities")
		class UAkAudioEvent* atksound;

	

	UPROPERTY(EditAnywhere, Category = "Abilities")
		class UParticleSystem* PrimaryFiringParticle;

	UPROPERTY(EditAnywhere, Category = "Abilities")
		float PrimaryParticleScale = 10.f;

	UPROPERTY(EditAnywhere, Category = "Abilities")
		int NumberGranades = 6;

protected:

	//ACC_Jade::ACC_Jade();

	virtual void EnablePrimary() override;
	virtual void EnableSecondary() override;
	virtual void DisableSecondary() override;
	void SpawnPrimary() override;
	void SpawnSecondary() override;
	void SpawnParticle(UParticleSystem* SpawningParticle);

public: 
	bool isTargetSet(AActor* Target);

	//Target primed for detonation
	AActor* PrimedEnemy;
};
