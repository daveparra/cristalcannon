// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_WayPoint.h"
#include "Components/BoxComponent.h"
#include "CC_MobEnemie.h"
// Sets default values
ACC_WayPoint::ACC_WayPoint()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	SetRootComponent(Root);

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger Box"));
	BoxComponent->SetupAttachment(GetRootComponent());
	BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &ACC_WayPoint::OnPLayerEnter);

}

// Called when the game starts or when spawned
void ACC_WayPoint::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACC_WayPoint::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ACC_WayPoint::OnPLayerEnter(UPrimitiveComponent * OverlapComponent, AActor * OtherActor, UPrimitiveComponent * OtherComponent, int32 OtherBodyIndex, bool bfromSweep, const FHitResult & Sweep)
{
	ACC_MobEnemie* Character = nullptr;
	if (OtherActor != nullptr) 
	{
		Character = Cast<ACC_MobEnemie>(OtherActor);
		if (Character != nullptr)
		{
			
		}
	}
}

