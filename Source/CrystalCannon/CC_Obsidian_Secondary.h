// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CC_Projectile.h"
#include "CC_Obsidian_Secondary.generated.h"

UCLASS()
class CRYSTALCANNON_API ACC_Obsidian_Secondary : public ACC_Projectile
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACC_Obsidian_Secondary();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//UPROPERTY(EditAnywhere, Category = "Beam")
	//float BeamDistance;

	//Beam Range
	UPROPERTY(EditAnywhere, Category = "Beam")
	float BeamWidth;

	UPROPERTY(EditAnywhere, Category = "Beam")
	float MaxDist;

	UPROPERTY(EditAnywhere, Category = "Beam")
	float BeamDuration = 1;

	UPROPERTY(EditAnywhere, Category = "Beam")
	float FireDelay = 1;

	UPROPERTY(BlueprintReadOnly)
	float DistTravelled = 0;

	TArray<AActor*> Targets;

	FVector StartPos;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//Called when something enters sphere  
	virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

	//Called when something leaves sphere
	virtual void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) override;

	void Fire(); //uses Box overlap and gets all targets to be damaged 

	void DamageTargets();//Applies damage to targets in array

	void AbilityEnd();
};
