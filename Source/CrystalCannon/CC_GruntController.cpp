// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_GruntController.h"
#include "Components/ArrowComponent.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/NavigationSystem/Public/NavigationSystem.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Sight.h"
#include "CC_Character.h"
#include "CC_GruntCharacter.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemComponent.h"
#include "Runtime/Engine/Classes/Kismet/KismetMathLibrary.h"
#include "CC_CrystalBall.h"
#include "UObject/ConstructorHelpers.h"

ACC_GruntController::ACC_GruntController()
{
	PrimaryActorTick.bCanEverTick = true;

	SightConfig = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("Sight Config"));
	SetPerceptionComponent(*CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("Perception Component")));

	SightConfig->SightRadius = AISightRadius;
	SightConfig->LoseSightRadius = AILoseSightRadius;
	SightConfig->PeripheralVisionAngleDegrees = AIFieldOfView;
	SightConfig->SetMaxAge(AISightAge);

	SightConfig->DetectionByAffiliation.bDetectEnemies = true;
	SightConfig->DetectionByAffiliation.bDetectFriendlies = true;
	SightConfig->DetectionByAffiliation.bDetectNeutrals = true;

	GetPerceptionComponent()->SetDominantSense(*SightConfig->GetSenseImplementation());
	GetPerceptionComponent()->OnPerceptionUpdated.AddDynamic(this, &ACC_GruntController::OnPawnDetected);
	GetPerceptionComponent()->ConfigureSense(*SightConfig);

	static ConstructorHelpers::FObjectFinder<UParticleSystem> ParticleThree(TEXT("/Game/Art/Particles/PS_EnemyReticle.PS_EnemyReticle"));
	ParticleFX1 = ParticleThree.Object;
}

void ACC_GruntController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	if (Cast<ACC_GruntCharacter>(GetPawn())->GetCurrentHealth() > 0.0f) 
	{
		if (!Cast<ACC_GruntCharacter>(GetPawn())->IsIdle) 
		{
			if (!Cast<ACC_GruntCharacter>(GetPawn())->IsAttacking)
			{
				Cast<ACC_GruntCharacter>(GetPawn())->IsWalking = true;
				TimerMove -= DeltaSeconds;
				if (TimerMove < 0)
				{
					Cast<ACC_GruntCharacter>(GetPawn())->IsWalking = false;
					GetRandomLocation();
					MoveToLocation(Result);
					TimerMove = MR;
				}
				CoolDown -= DeltaSeconds;
			}
			else 
			{
				//Cast<ACC_GruntCharacter>(GetPawn())->IsAttacking = false;
				ShootTimer -= DeltaSeconds;
				if (ShootTimer < 0)
				{
					Cast<ACC_GruntCharacter>(GetPawn())->IsAttacking = false;
				}
			}
		}
		else {
			Cast<ACC_GruntCharacter>(GetPawn())->IsWalking = false;
		}
	}
	else
	{
		//GetPawn()->Destroy();
		Cast<ACC_GruntCharacter>(GetPawn())->IsDead = true;
		Destroy();
	}
	
}

void ACC_GruntController::SpawnCrystal(FVector SocketLocation)
{
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnInfo.Owner = GetPawn();
	SpawnInfo.Instigator = GetPawn();

	//Spawn Location
	//FVector SpawnLocation = GetPawn()->GetActorLocation();
	FVector SpawnLocation = SocketLocation;
	SpawnLocation += GetPawn()->GetActorRotation().Vector() * 160.f;
	//FRotator SpawnRotation = GetPawn()->GetActorRotation();
	
	const float Gravity = GetWorld()->GetGravityZ();

	float Theta = (32 * PI / 180); // launch angle
	
	FVector dir = EnemyLocation - SpawnLocation; //direction
	float Sz = dir.Z;// height difference
	dir.Z = 0; // remove hight from direction
	float Sx = dir.Size();// distance
	FVector nEWlOC = SpawnLocation - EnemyLocation;
	FRotator newrot = nEWlOC.Rotation();

	FRotator SpawnRotation = FRotator(0, newrot.Yaw, 0);
	const float V = (Sx / cos(Theta)) * FMath::Sqrt((980.0 * 1) / (2 * (Sx * tan(Theta) - Sz)));
	FVector VelocityOutput = FVector(-V*cos(Theta), 0, V*sin(Theta));

	ACC_CrystalBall* Ball = Cast<ACC_CrystalBall>(GetWorld()->SpawnActor(CrystalToUse, &SpawnLocation, &SpawnRotation, SpawnInfo));
	Ball->Landing = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ParticleFX1, EnemyLocation, EnemyLocation.Rotation(), FVector(1.f, 1.f, 1.f),false);
	Ball->ProjectileMovementComponent->SetVelocityInLocalSpace(VelocityOutput);

	
}

void ACC_GruntController::BeginPlay()
{
	Super::BeginPlay();
	//Cast<ACC_GruntCharacter>(GetPawn())->IsIdle = true;
}

void ACC_GruntController::Possess(APawn * Pawn)
{
	Super::Possess(Pawn);
}

void ACC_GruntController::GetRandomLocation()
{
	UNavigationSystemV1* NavSys = UNavigationSystemV1::GetCurrent(GetWorld());
	if (!NavSys)
	{
		return;
	}
	FVector Origin = GetPawn()->GetActorLocation();
	bool BSucces = NavSys->K2_GetRandomPointInNavigableRadius(GetWorld(), Origin, Result, MovementRadius);
}

void ACC_GruntController::OnPawnDetected(const TArray<AActor*>& DetectedPawns)
{
	if (CoolDown < 0)
	{
		if (DetectedPawns.Num() > 0)
		{
			ACC_Character* Pawn = nullptr;
			for (int i = 0; i < DetectedPawns.Num(); i++)
			{
				Pawn = Cast<ACC_Character>(DetectedPawns[i]);
				if (Pawn != nullptr && Pawn->GetCurrentHealth() > 0)
				{
					EnemyLocation = Pawn->FirstTitty->GetComponentLocation(); //Pawn->GetActorLocation(); 
					FVector VectorRetult = Pawn->GetActorLocation() - GetPawn()->GetActorLocation();
					if (FVector::Distance(EnemyLocation, GetPawn()->GetActorLocation()) > 400.f)
					{
						GetPawn()->SetActorRotation(VectorRetult.Rotation());
						StopMovement();
						Cast<ACC_GruntCharacter>(GetPawn())->IsAttacking = true;
						ShootTimer = 4.867f;
					}
				}
			}
		}
		CoolDown = 3.0f;
	}
}

FRotator ACC_GruntController::GetControlRotation() const
{
	if (GetPawn() == nullptr)
	{
		return FRotator(0.0f, 0.0f, 0.0f);
	}

	return FRotator(0.0f, GetPawn()->GetActorRotation().Yaw, 0.0f);
}
