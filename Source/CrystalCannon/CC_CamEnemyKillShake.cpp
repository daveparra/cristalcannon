// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_CamEnemyKillShake.h"

UCC_CamEnemyKillShake::UCC_CamEnemyKillShake()
{
	OscillationDuration = 0.25f;
	OscillationBlendInTime = 0.25f;
	OscillationBlendOutTime = 0.25f;

	RotOscillation.Pitch.Amplitude = FMath::RandRange(AmplitudeMin, AmplitudeMax);
	RotOscillation.Pitch.Frequency = FMath::RandRange(FrequencyMin, FrequencyMax);

	RotOscillation.Yaw.Amplitude = FMath::RandRange(AmplitudeMin, AmplitudeMax);
	RotOscillation.Yaw.Frequency = FMath::RandRange(FrequencyMin, FrequencyMax);
}