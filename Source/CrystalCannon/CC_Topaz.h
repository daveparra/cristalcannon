// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CC_Character.h"
#include "CC_Topaz.generated.h"

/**
 *
 */
UCLASS()
class CRYSTALCANNON_API ACC_Topaz : public ACC_Character
{
	GENERATED_BODY()

	virtual void Tick(float DeltaTime) override;
	virtual void EnablePrimary() override;
	virtual void EnableSecondary() override;
	virtual void DisableSecondary() override;
	void SpawnPrimary() override;
	void SpawnSecondary() override;


public:

	UPROPERTY(EditAnywhere)
	float ParticleOffset = 50.f;
 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Abilities")
	class UAkAudioEvent* atksound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Abilities")
	UAkAudioEvent* SecondSound;

	UPROPERTY(EditAnywhere)
	UParticleSystem* ParticleFXSlam;

	UPROPERTY()
	UParticleSystemComponent* GroundSlamParticle;

	FVector StartPos;

};
