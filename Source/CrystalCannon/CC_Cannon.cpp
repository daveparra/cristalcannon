// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_Cannon.h"
#include "Components/SphereComponent.h"
#include "Engine/GameEngine.h"
#include "CC_Wheel.h"
#include "Runtime/Engine/Classes/GameFramework/RotatingMovementComponent.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstanceDynamic.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
ACC_Cannon::ACC_Cannon(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SplineComponent = CreateDefaultSubobject<USplineComponent>(TEXT("SplineComponent"));
	this->RootComponent = SplineComponent;

	CannonMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CannonMesh"));
	CannonMesh->AttachTo(RootComponent);

	CircleMaterial = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CircleMesh"));
	CircleMaterial->AttachTo(CannonMesh);

	VisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("VisionSphere"));
	VisionSphere->SetSphereRadius(SphereRadius);
	VisionSphere->AttachTo(CannonMesh);

	VisionSphere->OnComponentBeginOverlap.AddDynamic(this, &ACC_Cannon::OnBoxBeginOverlap);
	VisionSphere->OnComponentEndOverlap.AddDynamic(this, &ACC_Cannon::OnBoxEndOverlap);


}

void ACC_Cannon::OnBoxBeginOverlap(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (OtherActor->IsA(ACC_Character::StaticClass()))
	{
		OverlappingActors.AddUnique(OtherActor);
	}
}

void ACC_Cannon::OnBoxEndOverlap(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex)
{	
	if (OverlappingActors.Contains(OtherActor))
	{
		OverlappingActors.Remove(OtherActor);
	}
}

// Called when the game starts or when spawned
void ACC_Cannon::BeginPlay()
{
	Super::BeginPlay();
	FirePos = Cast<UArrowComponent>(GetDefaultSubobjectByName(TEXT("Shoot")));

	this->GetAttachedActors(MyActor_Children);
	DynamicMatInstance = CircleMaterial->CreateAndSetMaterialInstanceDynamic(0);
	//color = FVector(1.0f, 0.0f, 0.0f);
	//DynamicMatInstance->SetVectorParameterValue(FName("Color"), color);
	//CircleMaterial->SetMaterial(0, DynamicMatInstance);
	//GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, FString::FromInt(OverlappingActors.Num()));

}

// Called every frame
void ACC_Cannon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (ParticleActivated)
	{
		timer += DeltaTime;

		if (timer >= 4)
		{
			ParticleActivated = false;
			timer = 0;

			if (CannonBall)
			{
				UWorld* World = this->GetWorld();

				//Spawn Params
				FActorSpawnParameters SpawnInfo;
				SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnInfo.Owner = this;

				//Spawn Location
				FVector SpawnLocation = this->FirePos->GetComponentLocation();
				SpawnLocation += this->FirePos->GetForwardVector() * FiringDistance;
				//SpawnLocation.Z += 40.f;

				//Spawn Rotation
				FRotator SpawnRotation = this->FirePos->GetForwardVector().Rotation();

				if (World == nullptr)
				{
					GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, "NULL @ Cannon FireCannon");
				}
				else
				{
					World->SpawnActor(CannonBall, &SpawnLocation, &SpawnRotation, SpawnInfo);
				}
			}
		}
	}

	//GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, FString::FromInt(OverlappingActors.Num()));
	MoveAlongSpline();

	if (CanMove && CurrentMovementSpeed != 0.f)
	{
		for (size_t i = 0; i < MyActor_Children.Num(); i++)
		{
			ACC_Wheel* Wheel = Cast<ACC_Wheel>(MyActor_Children[i]);
			if (Wheel)
			{
				if (Wheel->BackLeft) Wheel->Movement->RotationRate = FRotator(0.0f, 0.0f, -180.0f);
				if (Wheel->BackRight) Wheel->Movement->RotationRate = FRotator(0.0f, 0.0f, 180.0f);
				if (Wheel->MidLeft) Wheel->Movement->RotationRate = FRotator(0.0f, 0.0f, 80.0f);
				if (Wheel->MidRight) Wheel->Movement->RotationRate = FRotator(0.0f, 0.0f, 80.0f);
				if (Wheel->FrontLeft) Wheel->Movement->RotationRate = FRotator(0.0f, 0.0f, 140.0f);
				if (Wheel->FrontRight) Wheel->Movement->RotationRate = FRotator(0.0f, 0.0f, 140.0f);
			}
		}
	}
	else {
		for (size_t i = 0; i < MyActor_Children.Num(); i++)
		{
			ACC_Wheel* Wheel = Cast<ACC_Wheel>(MyActor_Children[i]);
			if (Wheel)
			{
				if (Wheel->BackLeft) Wheel->Movement->RotationRate = FRotator(0.0f, 0.0f, 0.0f);
				if (Wheel->BackRight) Wheel->Movement->RotationRate = FRotator(0.0f, 0.0f, 0.0f);
				if (Wheel->MidLeft) Wheel->Movement->RotationRate = FRotator(0.0f, 0.0f, 0.0f);
				if (Wheel->MidRight) Wheel->Movement->RotationRate = FRotator(0.0f, 0.0f, 0.0f);
				if (Wheel->FrontLeft) Wheel->Movement->RotationRate = FRotator(0.0f, 0.0f, 0.0f);
				if (Wheel->FrontRight) Wheel->Movement->RotationRate = FRotator(0.0f, 0.0f, 0.0f);
			}
		}
	}

}

void ACC_Cannon::FireCannon()
{
	UWorld* World = this->GetWorld();

	//Spawn Params
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnInfo.Owner = this;

	//Spawn Location
	FVector SpawnLocation = this->FirePos->GetComponentLocation();
	SpawnLocation += this->FirePos->GetForwardVector() * FiringDistance;
	//SpawnLocation.Z += 40.f;

	//Spawn Rotation
	FRotator SpawnRotation = this->GetActorForwardVector().Rotation();

	if (World == nullptr)
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, "NULL @ Cannon FireCannon");
	}
	else
	{
		if (FireParticle)
		{
			World->SpawnActor(FireParticle, &SpawnLocation, &SpawnRotation, SpawnInfo);
			ParticleActivated = true;
		}

	}
}

void ACC_Cannon::MoveAlongSpline()
{
	if (CanMove)
	{

		if (OverlappingActors.Num() == 2)
		{
			this->CurrentMovementSpeed = DefaultMovementSpeed;
			color = FVector(1.0f, 1.0f, 0.0f);
			DynamicMatInstance->SetVectorParameterValue(FName("Color"), color);
			CircleMaterial->SetMaterial(0, DynamicMatInstance);
		}

		else if (OverlappingActors.Num() == 3)
		{
			color = FVector(1.0f, 1.0f, 0.0f);
			DynamicMatInstance->SetVectorParameterValue(FName("Color"), color);
			CircleMaterial->SetMaterial(0, DynamicMatInstance);
			this->CurrentMovementSpeed = (DefaultMovementSpeed * 1.3f);
		}

		else if (OverlappingActors.Num() == 4)
		{
			color = FVector(0.0f, 1.0f, 0.0f);
			DynamicMatInstance->SetVectorParameterValue(FName("Color"), color);
			CircleMaterial->SetMaterial(0, DynamicMatInstance);
			this->CurrentMovementSpeed = (DefaultMovementSpeed * 2.0f);
		}

		else
		{
			color = FVector(1.0f, 0.0f, 0.0f);
			DynamicMatInstance->SetVectorParameterValue(FName("Color"), color);
			CircleMaterial->SetMaterial(0, DynamicMatInstance);
			this->CurrentMovementSpeed = 0.f;
		}

		DistanceAlongSpline += CurrentMovementSpeed;
		PositionAlongSpline = SplineComponent->GetLocationAtDistanceAlongSpline(DistanceAlongSpline, ESplineCoordinateSpace::Local);
		RotationAlongSpline = SplineComponent->GetRotationAtDistanceAlongSpline(DistanceAlongSpline, ESplineCoordinateSpace::Local);
		RotationAlongSpline += FRotator(0, 90, 0);

		this->CannonMesh->SetRelativeLocationAndRotation(PositionAlongSpline, RotationAlongSpline, false);
	}

}
