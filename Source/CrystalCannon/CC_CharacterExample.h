// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CC_Unit.h"
#include "CC_CharacterExample.generated.h"

class UCameraComponent;
class UArrowComponent;

UCLASS()
class CRYSTALCANNON_API ACC_CharacterExample : public ACC_Unit
{
	GENERATED_BODY()

public:
	ACC_CharacterExample();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseLookUpRate;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UArrowComponent* FirstTitty;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UArrowComponent* SecondTitty;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UArrowComponent* ThirdTitty;

protected:

	bool stop1, stop2;
	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/**
	 * Called via input to turn at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */

	void BeginPlay() override;
	void Tick(float DeltaTime) override;

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface
	
	FRotator FirstROt;
	bool Flag;
public:
	float XAxis = 0.0f;
	float YAxis = 0.0f;
	float LXAxis = 0.0f;
	float LYAxis = 0.0f;

	bool isFirstTitty = false;
	bool isSecondTitty = false;
	bool isThirdTitty = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		bool IsWalking;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		bool IsLeft;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		bool IsRight;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		bool IsBack;
};
