// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CC_Enemy.h"
#include "CC_SplitterCharacter.generated.h"

UCLASS()
class CRYSTALCANNON_API ACC_SplitterCharacter : public ACC_Enemy
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties

	ACC_SplitterCharacter();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UArrowComponent* Shoot;

	UPROPERTY(Category = AI, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class USphereComponent* VisionSphere;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
		float VisionRadius;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Abilities)
		class UMaterialInterface* ExplosionMaterial;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Abilities)
		class UAkAudioEvent* SplitterSound;

	UFUNCTION()
		void DoExplosion(AActor * OtherActor);

	UFUNCTION()
		void OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void OnBoxEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	bool isKanikase;
	
	UFUNCTION()
		void PlayAttackSound();

	UFUNCTION()
		void PlayDeadSound();
};
