// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_Avoid.h"

// Sets default values
ACC_Avoid::ACC_Avoid()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Value = -4.0f;
}

// Called when the game starts or when spawned
void ACC_Avoid::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACC_Avoid::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (vanish)
	{
		timer -= DeltaTime;
		if (timer < 0) 
		{
			Destroy();
		}
	}
}

