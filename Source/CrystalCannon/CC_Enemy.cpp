// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_Enemy.h"
#include "CC_Character.h"
#include "CC_GameMode.h"
#include "CC_GameInstance.h"
#include "Runtime/Engine/Classes/GameFramework/GameMode.h"
#include "Engine/GameEngine.h"

void ACC_Enemy::BeginPlay()
{
	Super::BeginPlay();
}


void ACC_Enemy::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	UWorld* world = this->GetWorld();
	ACC_Character* Chara = Cast<ACC_Character>(this->LastHit);
	UCC_GameInstance* Instance = world->GetGameInstance<UCC_GameInstance>();
	if (Chara && Instance)
	{
		Instance->UpdateKills(Chara->PlayerIndex);
	}
}

void ACC_Enemy::ApplyDamage(float Damage)
{
	Super::ApplyDamage(Damage);
	SpawnBlood();

	UWorld* world = this->GetWorld();
	ACC_Character* Chara = Cast<ACC_Character>(this->LastHit);
	UCC_GameInstance* Instance = world->GetGameInstance<UCC_GameInstance>();
	if (Chara && Instance)
	{
		Instance->UpdateDamageDealt(Chara->PlayerIndex, Damage);
	}
}

//void ACC_Enemy::MaxDamageDealer()//Calcultes which player deals most damage
//{
//	float max1 = 0.f;
//	float max2 = 0.f;
//	int index = 0;
//
//	if (PlayerScores[0] > PlayerScores[1])
//	{
//		max1 = PlayerScores[0];
//		index = 0;
//	}
//	else
//	{
//		max1 = PlayerScores[1];
//		index = 1;
//	}
//
//	if (PlayerScores[2] > PlayerScores[3])
//	{
//		max2 = PlayerScores[2];
//		KillIndex = 2;
//	}
//	else
//	{
//		max2 = PlayerScores[3];
//		KillIndex = 3;
//	}
//
//	if (max1 > max2)
//	{
//		KillIndex = index;
//	}
//	else
//	{
//		return;
//	}
//}
