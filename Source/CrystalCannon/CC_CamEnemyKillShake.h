// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraShake.h"
#include "CC_CamEnemyKillShake.generated.h"

/**
 * 
 */
UCLASS()
class CRYSTALCANNON_API UCC_CamEnemyKillShake : public UCameraShake
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditAnywhere, Category = "Camera Shake")
	float AmplitudeMin = 5.f;
	
	UPROPERTY(EditAnywhere, Category = "Camera Shake")
	float AmplitudeMax = 10.f;

	UPROPERTY(EditAnywhere, Category = "Camera Shake")
	float FrequencyMin = 25.f;

	UPROPERTY(EditAnywhere, Category = "Camera Shake")
	float FrequencyMax = 35.f;

	UCC_CamEnemyKillShake();
};
