// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_Topaz_Primary.h"
#include "TimerManager.h"
#include "CC_Character.h"
#include "CC_Enemy.h"
#include "Engine/GameEngine.h"

void ACC_Topaz_Primary::Tick(float DeltaTime)
{
	if (isReturning)
	{
		if (this->GetOwner() != nullptr)
		{
			this->ProjectileMovementComponent->Velocity = FMath::Lerp(this->ProjectileMovementComponent->Velocity, TargetDir, 0.1);
			TargetDir = this->GetOwner()->GetActorLocation() - this->GetActorLocation();
			TargetDir.Normalize();
			TargetDir *= CallbackSpeed;
			CallbackSpeed += 10;
		}
		else
		{
			GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, TEXT("Owning Actor null @ topaz primary 1"));
			Destroy();
		}
	}
}

void ACC_Topaz_Primary::BeginPlay()
{
	Super::BeginPlay();
	//this->SphereCollisionComponent->SetCollisionProfileName("OverlapAll");
	FTimerHandle CallbackHandler;
	GetWorldTimerManager().SetTimer(CallbackHandler, this, &ACC_Topaz_Primary::ReturnToOwner, Duration, false);
}

void ACC_Topaz_Primary::OnOverlapBegin(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{

	if ((OtherActor != NULL))
	{
		//GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, OtherActor->GetName());

		//CHECKS IF HIT ACTOR IS AN A CHILD OF THE ENEMY CLASS
		if (OtherActor->IsA(ACC_Enemy::StaticClass()))
		{
			//GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, TEXT("Checking for enemy"));
			ACC_Enemy* Enemy = Cast<ACC_Enemy>(OtherActor);

			Enemy->ApplyDamage(this->Damage);

			if (this->GetOwner() != nullptr)
			{
				Enemy->LastHit = this->GetOwner();
				Cast<ACC_Character>(this->GetOwner())->ApplyFFEOnDeath();
			}
		}

		//CHECKS IF HIT ACTOR IS THE OWNING ACTOR (i.e. The class which spawned the projectile)

		if (this->GetOwner() != nullptr)
		{
			if (OtherActor == this->GetOwner())
			{
				//GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, TEXT("Returned"));
				Destroy();
			}
			else if (OtherActor->IsA(ACC_Character::StaticClass()))
			{
				//GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, TEXT("Checking for character"));
				return;
			}
		}
		else
		{
			GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, TEXT("Owning Actor null @ topaz primary 2"));
		}


		//Destroy();
	}
}

void ACC_Topaz_Primary::OnOverlapEnd(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex)
{
	//GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, OtherActor->GetName());
}

void ACC_Topaz_Primary::ReturnToOwner()
{
	isReturning = true;
	//GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, "HOMING Called");
	//this->ProjectileMovementComponent->bIsHomingProjectile = true;
	//this->ProjectileMovementComponent->HomingAccelerationMagnitude = CallbackSpeed;
	if (this->GetOwner() != nullptr)
	{
		//TWeakObjectPtr<USceneComponent> ObjectObserver(this->GetOwner()->GetRootComponent());
		TargetDir = GetOwner()->GetActorLocation() - this->GetActorLocation();
		TargetDir.Normalize();
		TargetDir *= CallbackSpeed;
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, TEXT("Owning Actor null @ topaz primary 3"));
	}
}
