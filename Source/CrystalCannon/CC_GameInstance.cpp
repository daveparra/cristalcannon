// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_GameInstance.h"

void UCC_GameInstance::IncrementDeadPlayerCount()
{
	this->DeadPlayerCount++;
	if (DeadPlayerCount > 3)
	{
		GameOverEvent();
	}
}

UCC_GameInstance::UCC_GameInstance()
{
	FPlayerData Player;

	//Populating array
	Data.Add(Player);
	Data.Add(Player);
	Data.Add(Player);
	Data.Add(Player);
}

void UCC_GameInstance::UpdateKills(int32 index)
{
	Data[index].Kills++;
}

void UCC_GameInstance::UpdateDamageDealt(int32 index, float Damage)
{
	Data[index].DamageDealt += Damage;
}

void UCC_GameInstance::UpdateRevives(int32 index)
{
	Data[index].Revives++;
}

void UCC_GameInstance::UpdateDeaths(int32 index)
{
	Data[index].Deaths++;
}

void UCC_GameInstance::UpdateDamageTaken(int32 index, float Damage)
{
	Data[index].DamageTaken += Damage;
}