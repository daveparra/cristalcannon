// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_ThrowCrystal.h"
#include "Components/SkeletalMeshComponent.h"
#include "CC_GruntCharacter.h"
#include "CC_GruntController.h"

void UCC_ThrowCrystal::Notify(USkeletalMeshComponent * MeshComp, UAnimSequenceBase * Animation)
{
	Super::Notify(MeshComp, Animation);
	AActor* AnimationOwner = MeshComp->GetOwner();
	if (ACC_GruntCharacter* Character = Cast<ACC_GruntCharacter>(AnimationOwner))
	{
		if (ACC_GruntController* Controller = Cast<ACC_GruntController>(Character->GetController()))
		{
			USceneComponent * SceneComponet = MeshComp->GetChildComponent(0);
			SceneComponet->SetVisibility(false);
			Controller->SpawnCrystal(MeshComp->GetSocketLocation("RightHandSocket"));

		}
	}
}
