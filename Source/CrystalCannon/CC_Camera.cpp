#include "CC_Camera.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "EngineUtils.h"
#include "UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "CC_GameMode.h"
#include "Runtime/Core/Public/Math/Vector.h"
#include "Components/BoxComponent.h"
#include "Engine/GameEngine.h"
#include "CC_Jade.h"
#include "CC_Ruby.h"
#include "CC_Topaz.h"
#include "CC_Obsidian.h"
#include "CC_Character.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemComponent.h"
#include "Runtime/Core/Public/Math/UnrealMathUtility.h"
#include "CC_Cannon.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstanceDynamic.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
ACC_Camera::ACC_Camera()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneRoot = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultSceneRoot"));
	SceneRoot->SetupAttachment(RootComponent);

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(SceneRoot);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	 // Camera does not rotate relative to arm

	static ConstructorHelpers::FObjectFinder<UParticleSystem> Particle(TEXT("/Game/Art/Particles/PS_Teleport.PS_Teleport"));
	ParticleFX1 = Particle.Object;
	LengthForCastle = 1050.f;
	minLength = 850.0f;
	maxLength = 1100.0f;
	Result = FVector(0.0f, 0.0f, 0.0f);
	TargetPos = FVector(0.0f, 0.0f, 0.0f);
	PreviousPos = FVector(0.0f, 0.0f, 0.0f);
	inteSpeed = 0.5f;
	PlayerMoved = true;
}
//function to get all players pointers, for then to set the controller to each pawn and spawn the players for the game to begin
void ACC_Camera::ClassSetUp()
{
	ACC_GameMode * myGameMode = (ACC_GameMode *)UGameplayStatics::GetGameMode(GetWorld());
	PlayersReference = myGameMode->Players;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayerController::StaticClass(), PlayerControllers);
	for (int32 y = 0; y < PlayerControllers.Num(); y++)
	{
		APlayerController* Player = Cast<APlayerController>(PlayerControllers[y]);
		Player->SetViewTargetWithBlend(this);
	}
	for (int32 i = 0; i < PlayersReference.Num(); i++) {
		APawn* PlayerPawn = PlayersReference[i];
		switch (i)
		{
		case 0:
			P1 = PlayerPawn;
			break;
		case 1:
			P2 = PlayerPawn;
			break;
		case 2:
			P3 = PlayerPawn;
			break;
		case 3:
			P4 = PlayerPawn;
			break;
		default:
			break;
		}
	}
	
}
//Function that is going to be called every frame update for moving the camera
void ACC_Camera::UpdateCamera()
{
	//if (!TornadoActive)
	
		SetCameraPos();
		SetArmLenght();
}

//Function to set the height of the camera vision
void ACC_Camera::SetArmLenght()
{
	if (FollowCannon)
	{
		CameraBoom->TargetArmLength = FMath::Clamp<float>(LengthForCastle, minLength, maxLength);
	}
	else if (SinglePlayer)
	{
		Distances.Add(CalculateDistance(P1, P1));
		CameraBoom->TargetArmLength = FMath::Clamp<float>(FMath::Max<float>(Distances), minLength, maxLength);
		Distances.Empty();
	}
	else
	{
		Distances.Add(CalculateDistance(P1, P2));
		Distances.Add(CalculateDistance(P1, P3));
		Distances.Add(CalculateDistance(P1, P4));
		Distances.Add(CalculateDistance(P2, P3));
		Distances.Add(CalculateDistance(P2, P4));
		Distances.Add(CalculateDistance(P3, P4));
		//FMath::Max<float>(Distances)
		if (PlayerMoved)
		{
			CameraBoom->TargetArmLength = FMath::Clamp<float>(Result.Size(), minLength, maxLength);
		}
		Distances.Empty();
	}
}

void ACC_Camera::ActivateController(APlayerController* PC, AActor* Character)
{
	PlayerMoved = true;
	if (changeJadeColor)
	{
		changeJadeColor = false;
		lerpJadeValue = 1.0f;
	}else if (changeObsColor)
	{
		changeObsColor = false;
		lerpObsValue = 1.0f;
	}else if (changeRubyColor)
	{
		changeRubyColor = false;
		lerpRubyValue = 1.0f;
	}else if (changeTopazColor)
	{
		changeTopazColor = false;
		lerpTopazValue = 1.0f;
	}
	//Enable player input

	//Character->EnableInput(PC);
}

//Get the distance between two players on the world
float ACC_Camera::CalculateDistance(APawn * Player1, APawn * Player2)
{
	if (Player1 != nullptr && Player2 != nullptr) {
		return Player1->GetDistanceTo(Player2);
	}
	return 0.0f;
}

FVector ACC_Camera::GetLocation(APawn * Player)
{
	if (Player != nullptr) {
		return Player->GetActorLocation();
	}
	return FVector(0.0f, 0.0f, 0.0f);
}
//Move camera to location the was calculated by the 4 players position
void ACC_Camera::SetCameraPos()
{
	if (!CharacterOutofBounds)
	{
		if(FollowCannon)
		{
			//this->SetActorLocation(Cast<ACC_Cannon>(Cannons[0])->CannonMesh->GetComponentLocation());
			TargetPos = Cast<ACC_Cannon>(Cannons[0])->CannonMesh->GetComponentLocation();
			//Cast<ACC_Cannon>(Cannons[0])->CanMove = true;
		}
		else if (SinglePlayer)
		{
			//this->SetActorLocation(P1->GetActorLocation());
			TargetPos = P1->GetActorLocation();
		}
		else
		{
			//this->SetActorLocation(((P1->GetActorLocation() + P2->GetActorLocation() + P2->GetActorLocation() + P3->GetActorLocation())) / 4.0);
			if (PlayerMoved) 
			{
				Result = P1->GetActorLocation() + P2->GetActorLocation();
				Result += P3->GetActorLocation();
				Result += P4->GetActorLocation();
				Result /= 4.0f;
				//Result += P1->GetActorLocation();
				TargetPos = Result;
				//by average and last location of the camera in the other frame it will be lerping until it get to the selected point
				//this->SetActorLocation(FMath::Lerp(PreviousPos, TargetPos, inteSpeed));
			}
		
		}
		// reset location for the next update
		PreviousPos = this->GetActorLocation();
	}
}
void ACC_Camera::ChangeCollisionParam()
{
	//if (Debug) {
	//	BoxComponentBack->SetCollisionProfileName(TEXT("DebugCamera"));
	//	BoxComponentFront->SetCollisionProfileName(TEXT("DebugCamera"));
	//	BoxComponentLeft->SetCollisionProfileName(TEXT("DebugCamera"));
	//	BoxComponentRight->SetCollisionProfileName(TEXT("DebugCamera"));
	//	BoxComponentReset->SetCollisionProfileName(TEXT("DebugCamera"));
	//}else{
	//	BoxComponentBack->SetCollisionProfileName(TEXT("Camera"));
	//	BoxComponentFront->SetCollisionProfileName(TEXT("Camera"));
	//	BoxComponentLeft->SetCollisionProfileName(TEXT("Camera"));
	//	BoxComponentRight->SetCollisionProfileName(TEXT("Camera"));
	//	BoxComponentReset->SetCollisionProfileName(TEXT("Camera"));
	//}
}
//Spawn player to a sight view
void ACC_Camera::MoveActorToCenter(AActor * ActorToMove, AActor * Nearactor, APlayerController* PC)
{
	//ActorToMove->TeleportTo(Nearactor->GetActorLocation(), Nearactor->GetActorRotation());
	
	ActorToMove->SetActorLocation(Nearactor->GetActorLocation());
	
	if (Cast<ACC_Ruby>(ActorToMove))
	{
		RubyCharac = Cast<ACC_Character>(ActorToMove);
		changeRubyColor = true;
		FTimerDelegate TimerR;
		FTimerHandle TimerHR;
		TimerR.BindUFunction(this, FName("ActivateController"), PC, RubyCharac);
		GetWorldTimerManager().SetTimer(TimerHR, TimerR, 2.8f, false);
	}else if (Cast<ACC_Jade>(ActorToMove))
	{
		JadeCharac = Cast<ACC_Character>(ActorToMove);
		changeJadeColor = true;
		FTimerDelegate TimerJ;
		FTimerHandle TimerHJ;
		TimerJ.BindUFunction(this, FName("ActivateController"), PC, JadeCharac);
		GetWorldTimerManager().SetTimer(TimerHJ, TimerJ, 2.8f, false);
	}else if (Cast<ACC_Topaz>(ActorToMove))
	{
		TopazCharac = Cast<ACC_Character>(ActorToMove);
		changeJadeColor = true;
		FTimerDelegate TimerT;
		FTimerHandle TimerHT;
		TimerT.BindUFunction(this, FName("ActivateController"), PC, TopazCharac);
		GetWorldTimerManager().SetTimer(TimerHT, TimerT, 2.8f, false);
	}else if (Cast<ACC_Obsidian>(ActorToMove))
	{
		ObsCharac = Cast<ACC_Character>(ActorToMove);
		changeJadeColor = true;
		FTimerDelegate TimerO;
		FTimerHandle TimerHO;
		TimerO.BindUFunction(this, FName("ActivateController"), PC, ObsCharac);
		GetWorldTimerManager().SetTimer(TimerHO, TimerO, 2.8f, false);
	}
	
}

void ACC_Camera::OnCompHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if ((OtherActor != NULL) && (OtherActor != this) && (OtherComp != NULL))
	{
		/*if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, FString::Printf(TEXT("I Hit: %s"), *OtherActor->GetName()));*/
	}
	
	if (Cast<ACC_Character>(OtherActor))
	{
		if (!Debug)
		{
			CharacterOutofBounds = true;
			if(!Arena) Cast<ACC_Cannon>(Cannons[0])->CanMove = false;

			FActorSpawnParameters SpawnInfo;
			SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			SpawnInfo.Owner = this;
			FVector SpawnLocation = OtherActor->GetActorLocation();
			FRotator SpawnRotation = FRotator(90.0f,0.0f,0.0f);
			AActor * Wall;
			Wall = GetWorld()->SpawnActor(Shield, &SpawnLocation, &SpawnRotation, SpawnInfo);
		}
	}
}

void ACC_Camera::OnBoxBeginOverlap(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (Cast<ACC_Character>(OtherActor))
	{
		if (!Debug)
		{
			CharacterOutofBounds = false;
			if (!Arena) Cast<ACC_Cannon>(Cannons[0])->CanMove = true;
		}
	}
	/*GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Red, FString("Begin++++++++++++ ") + OtherActor->GetName());*/
}

//if any player get out of bounds, he will get teleported and respawn back to center 
void ACC_Camera::OnBoxEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (!Debug)
	{
		if (Cast<ACC_Character>(OtherActor))
		{
			if (Cast<ACC_Character>(OtherActor)->GetCurrentHealth() >= 0.f)
			{
				APlayerController* PC;
				ACC_Character* Char = Cast<ACC_Character>(OtherActor);
				PC = UGameplayStatics::GetPlayerController(GetWorld(), Char->PlayerIndex);
				//Char->DisableInput(PC);
				if (Char->PlayerIndex == 2) DynamicObsMatInstance = Char->GetMesh()->CreateAndSetMaterialInstanceDynamic(2);
				else if (Char->PlayerIndex == 3) DynamicTopazMatInstance = Char->GetMesh()->CreateAndSetMaterialInstanceDynamic(1);
				else if (Char->PlayerIndex == 1) DynamicRubyMatInstance = Char->GetMesh()->CreateAndSetMaterialInstanceDynamic(0);
				else DynamicJadeMatInstance = Char->GetMesh()->CreateAndSetMaterialInstanceDynamic(0);
				//AirParticle = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ParticleFX1, GetActorLocation(), GetActorRotation(), FVector(1.f, 1.f, 1.f));
				PlayerMoved = false;
				FTimerDelegate TimerDel;
				FTimerHandle TimerHandle;
				TimerDel.BindUFunction(this, FName("MoveActorToCenter"), OtherActor, this, PC);
				GetWorldTimerManager().SetTimer(TimerHandle, TimerDel, 0.2f, false);
				GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Red, FString("End++++++++++++ ") + OtherActor->GetName());
			}
			else {
				//CharacterOutofBounds = true;
				Cast<ACC_Character>(OtherActor)->ResetCharacter();
			}
			//Disable player input
		}

	}
}


// Called when the game starts or when spawned
void ACC_Camera::BeginPlay()
{
	Super::BeginPlay();
	PreviousPos = GetActorLocation();

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ACC_Cannon::StaticClass(), Cannons);

	BoxComponentBack = Cast<UBoxComponent>(GetDefaultSubobjectByName(TEXT("Box")));
	BoxComponentFront = Cast<UBoxComponent>(GetDefaultSubobjectByName(TEXT("Box1")));
	BoxComponentLeft = Cast<UBoxComponent>(GetDefaultSubobjectByName(TEXT("Box2")));
	BoxComponentRight = Cast<UBoxComponent>(GetDefaultSubobjectByName(TEXT("Box3")));
	BoxComponentReset = Cast<UBoxComponent>(GetDefaultSubobjectByName(TEXT("Reset")));
	BoxTeleport = Cast<UBoxComponent>(GetDefaultSubobjectByName(TEXT("Teleport")));

	//BoxComponent->SetSimulatePhysics(true);
	//BoxComponent->SetNotifyRigidBodyCollision(true);

	//BoxComponent->BodyInstance.SetCollisionProfileName("BlockAllDynamic");

	//BoxTeleport->OnComponentBeginOverlap.AddDynamic(this, &ACC_Camera::OnBoxBeginOverlap);
	//BoxTeleport->OnComponentEndOverlap.AddDynamic(this, &ACC_Camera::OnBoxEndOverlap);

	/*BoxComponentRight->OnComponentBeginOverlap.AddDynamic(this, &ACC_Camera::OnBoxBeginOverlap);
	BoxComponentRight->OnComponentEndOverlap.AddDynamic(this, &ACC_Camera::OnBoxEndOverlap);

	BoxComponentLeft->OnComponentBeginOverlap.AddDynamic(this, &ACC_Camera::OnBoxBeginOverlap);
	BoxComponentLeft->OnComponentEndOverlap.AddDynamic(this, &ACC_Camera::OnBoxEndOverlap);

	BoxComponentFront->OnComponentBeginOverlap.AddDynamic(this, &ACC_Camera::OnBoxBeginOverlap);
	BoxComponentFront->OnComponentEndOverlap.AddDynamic(this, &ACC_Camera::OnBoxEndOverlap);

	BoxComponentBack->OnComponentBeginOverlap.AddDynamic(this, &ACC_Camera::OnBoxBeginOverlap);
	BoxComponentBack->OnComponentEndOverlap.AddDynamic(this, &ACC_Camera::OnBoxEndOverlap);*/

	BoxComponentReset->OnComponentBeginOverlap.AddDynamic(this, &ACC_Camera::OnBoxBeginOverlap);
	BoxComponentRight->OnComponentHit.AddDynamic(this, &ACC_Camera::OnCompHit);
	BoxComponentLeft->OnComponentHit.AddDynamic(this, &ACC_Camera::OnCompHit);
	BoxComponentFront->OnComponentHit.AddDynamic(this, &ACC_Camera::OnCompHit);
	BoxComponentBack->OnComponentHit.AddDynamic(this, &ACC_Camera::OnCompHit);

	//call class set up a mili second after the game started
	FTimerDelegate TimerDel;
	FTimerHandle TimerHandle;
	FTimerHandle TimerHandle1;
	FTimerHandle TimerHandle2;
	//Binding the function with specific variables
	TimerDel.BindUFunction(this, FName("ClassSetUp"));
	GetWorldTimerManager().SetTimer(TimerHandle, TimerDel, 0.0001f, false);

	//Call function in a loop for updating camera
	TimerDel.BindUFunction(this, FName("UpdateCamera"));
	//Calling MyUsefulFunction after 5 seconds without looping
	GetWorldTimerManager().SetTimer(TimerHandle1, TimerDel, 0.001f, true);

	/*TimerDel.BindUFunction(this, FName("ChangeCollisionParam"));
	GetWorldTimerManager().SetTimer(TimerHandle2, TimerDel, 0.1f, true);*/
}

// Called every frame
void ACC_Camera::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	this->SetActorLocation(FMath::Lerp(PreviousPos, TargetPos, 0.1f));
	/*FString move = Cast<ACC_Cannon>(Cannons[0])->CanMove ? "True" : "False";
	FString bounds = CharacterOutofBounds ? "True" : "False";
	GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, "Cannon Move: " + move + " CharacterOutofBounds : " + bounds);*/

	//if (changeRubyColor)
	//{
	//	lerpRubyValue -= 0.01f;
	//	FString TheFloatStr = FString::SanitizeFloat(lerpRubyValue);
	//	GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, TheFloatStr);
	//	DynamicRubyMatInstance->SetScalarParameterValue(FName("Power"), lerpRubyValue);
	//	RubyCharac->GetMesh()->SetMaterial(0, DynamicRubyMatInstance);
	//}else if (changeJadeColor)
	//{
	//	lerpJadeValue -= 0.01f;
	//	FString TheFloatStr = FString::SanitizeFloat(lerpJadeValue);
	//	GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, TheFloatStr);
	//	DynamicJadeMatInstance->SetScalarParameterValue(FName("Power"), lerpJadeValue);
	//	JadeCharac->GetMesh()->SetMaterial(0, DynamicJadeMatInstance);
	//}else if (changeTopazColor)
	//{
	//	lerpTopazValue -= 0.01f;
	//	FString TheFloatStr = FString::SanitizeFloat(lerpTopazValue);
	//	GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, TheFloatStr);
	//	DynamicTopazMatInstance->SetScalarParameterValue(FName("Power"), lerpTopazValue);
	//	TopazCharac->GetMesh()->SetMaterial(1, DynamicTopazMatInstance);
	//}else if (changeObsColor)
	//{
	//	lerpObsValue -= 0.01f;
	//	FString TheFloatStr = FString::SanitizeFloat(lerpObsValue);
	//	GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, TheFloatStr);
	//	DynamicObsMatInstance->SetScalarParameterValue(FName("Power"), lerpObsValue);
	//	ObsCharac->GetMesh()->SetMaterial(2, DynamicObsMatInstance);
	//}

}

