// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CC_RubyExplosion.generated.h"

UCLASS()
class CRYSTALCANNON_API ACC_RubyExplosion : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACC_RubyExplosion();

	UPROPERTY(Category = AI, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class USphereComponent* VisionSphere;

	UPROPERTY(EditAnywhere, Category = Abilities)
		float VisionRadius;

	UPROPERTY(EditAnywhere, Category = Abilities)
		float DamageExplosion;

	UFUNCTION()
		void OnSphereBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	float Timer = 1.f;
public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
