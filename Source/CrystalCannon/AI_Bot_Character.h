// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CC_Character.h"
#include "AI_Bot_Character.generated.h"

/**
 * 
 */
class ACC_WayPoint;
UCLASS()
class CRYSTALCANNON_API AAI_Bot_Character : public ACC_Character
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AAI_Bot_Character();
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ACC_WayPoint* NextWayPoint;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;



};
