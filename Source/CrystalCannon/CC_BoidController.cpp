// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_BoidController.h"
#include "CC_MobEnemie.h"

ACC_BoidController::ACC_BoidController()
{
	PrimaryActorTick.bCanEverTick = true;
}

FRotator ACC_BoidController::GetControlRotation() const
{
	if (GetPawn() == nullptr)
	{
		return FRotator(0.0f, 0.0f, 0.0f);
	}

	return FRotator(0.0f, GetPawn()->GetActorRotation().Yaw, 0.0f);

}

void ACC_BoidController::BeginPlay()
{
	Super::BeginPlay();
	//GetRandomLocation();

}

void ACC_BoidController::Possess(APawn * Pawn)
{
	Super::Possess(Pawn);
}

void ACC_BoidController::Tick(float DeltaSeconds)
{
	//if(Cast<ACC_MobEnemie>(GetPawn())->Deflection.Equals(FVector::ZeroVector))
	//	MoveToLocation(Cast<ACC_MobEnemie>(GetPawn())->NewDirection);
	//else
	FVector Location = Cast<ACC_MobEnemie>(GetPawn())->NewDirection;
	MoveToLocation(Location);

}
