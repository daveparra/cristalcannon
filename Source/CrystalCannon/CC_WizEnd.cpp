// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_WizEnd.h"
#include "CC_WizzardLizard.h"
#include "CC_WizLizController.h"
#include "Components/SkeletalMeshComponent.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemComponent.h"

void UCC_WizEnd::Notify(USkeletalMeshComponent * MeshComp, UAnimSequenceBase * Animation)
{
	Super::Notify(MeshComp, Animation);
	AActor* AnimationOwner = MeshComp->GetOwner();
	if (ACC_WizzardLizard* Character = Cast<ACC_WizzardLizard>(AnimationOwner))
	{
		Character->isShooting = false;
		ACC_WizLizController* Controller = Cast<ACC_WizLizController>(Character->GetController());
		Controller->CharacterPositon = FVector::ZeroVector;
		Controller->Beam->DestroyComponent();
		Character->isTeleporting = true;
	}
}
