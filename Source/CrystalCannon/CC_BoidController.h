// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "CC_BoidController.generated.h"

/**
 * 
 */
UCLASS()
class CRYSTALCANNON_API ACC_BoidController : public AAIController
{
	GENERATED_BODY()
public:
	ACC_BoidController();

	virtual void BeginPlay() override;

	virtual void Possess(APawn* Pawn) override;

	virtual void Tick(float DeltaSeconds) override;

	virtual FRotator GetControlRotation() const override;
};
