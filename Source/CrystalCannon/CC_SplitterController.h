// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "CC_SplitterController.generated.h"

/**
 * 
 */
class ACC_MobEnemie;
class ACC_ShootingCrystal;
class UPawnSensingComponent;
class AActor;
class UParticleSystem;

UCLASS()
class CRYSTALCANNON_API ACC_SplitterController : public AAIController
{
	GENERATED_BODY()

protected:
	
	virtual void BeginPlay() override;

	virtual void Possess(APawn* Pawn) override;

	virtual FRotator GetControlRotation() const override;

	
public:
	ACC_SplitterController();
	FVector CharacterPositon;
	float originX = 0.0f;
	float originY = 0.0f;
	float newX = 0.0f;
	float newY = 0.0f;
	float Timer = 3.0f;
	float TimerMove = 2.5f;
	int randomNumber = 2;

	AActor* Primary;
	FVector Result;
	bool Ontime = false;
	void GetRandomLocation();
	virtual void Tick(float DeltaSeconds) override;

	UPROPERTY(EditAnywhere, Category = Debug)
		bool isDead = true;
	UPROPERTY(EditAnywhere, Category = Debug)
		float FR = 3.0f;
	UPROPERTY(EditAnywhere, Category = Debug)
		float MR = 1.5f;

	UPROPERTY(EditAnywhere, Category = Debug)
		float MovementRadius = 1000.f;

	UPROPERTY(EditAnywhere, Category = Debug)
		float FireDistance = 150.f;

	UPROPERTY(EditAnywhere, Category = Debug)
		float DistanceForRange = 150.f;

	UPROPERTY(EditAnywhere, Category = Abilities)
		TSubclassOf<ACC_ShootingCrystal> CrystalToUse;

	UPROPERTY(EditAnywhere, Category = Debug)
	float radius = 0.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Awareness)
		UPawnSensingComponent* PawnSensor;

	UFUNCTION()
		void OnSeePawn(APawn *OtherPawn);

	UParticleSystem* ParticleFX1;

};
