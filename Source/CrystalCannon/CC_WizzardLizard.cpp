// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_WizzardLizard.h"
#include "Components/ArrowComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

ACC_WizzardLizard::ACC_WizzardLizard()
{
	PrimaryActorTick.bCanEverTick = true;
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;
	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 600.0f, 0.0f);
	Shoot = Cast<UArrowComponent>(GetDefaultSubobjectByName(TEXT("Shooting")));
}

void ACC_WizzardLizard::BeginPlay()
{
	Super::BeginPlay();
}


void ACC_WizzardLizard::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
