// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_Fish.h"

// Sets default values
ACC_Fish::ACC_Fish(const FObjectInitializer &ObjectInitializer)
	:Super(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SplineComponent = CreateDefaultSubobject<USplineComponent>(TEXT("SplineComponent"));
	this->RootComponent = SplineComponent;

	FishMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("FishMesh"));
	FishMesh->AttachTo(RootComponent);

}

// Called when the game starts or when spawned
void ACC_Fish::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACC_Fish::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if(start) MoveAlongSpline();
}

void ACC_Fish::MoveAlongSpline()
{
	this->CurrentMovementSpeed = DefaultMovementSpeed;
	DistanceAlongSpline += CurrentMovementSpeed;
	PositionAlongSpline = SplineComponent->GetLocationAtDistanceAlongSpline(DistanceAlongSpline, ESplineCoordinateSpace::Local);
	RotationAlongSpline = SplineComponent->GetRotationAtDistanceAlongSpline(DistanceAlongSpline, ESplineCoordinateSpace::Local);
	//RotationAlongSpline += FRotator(0, 0, 0);
	this->FishMesh->SetRelativeLocationAndRotation(PositionAlongSpline, RotationAlongSpline, false);
}

