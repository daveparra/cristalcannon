// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_CrystalBall.h"
#include "Components/StaticMeshComponent.h"
#include "CC_Character.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemComponent.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Kismet/KismetMathLibrary.h"
#include "CC_ExplosionDamage.h"
#include "CC_GruntCharacter.h"


void ACC_CrystalBall::TakeGravity()
{
	//ProjectileMovementComponent->SetVelocityInLocalSpace(FVector::ZeroVector);
	Landing->DestroyComponent();
	if (this)
	{
		FVector Location = GetActorLocation();
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), LandExplosion, FVector(Location.X - 10.f, Location.Y - 10.f, Location.Z - 100.f), GetActorRotation(), FVector(2.f, 2.f, 2.f), true);
	}
}

void ACC_CrystalBall::TikingExplosion()
{
	MeshComponent->SetMaterial(0, ExplosionMaterial);
	FVector Location = GetActorLocation();
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ParticleExplosion, FVector(Location.X, Location.Y ,Location.Z - 120.f), GetActorRotation(), FVector(0.9f, 0.9f, 0.9f),true);
	FTimerDelegate Timer;
	FTimerHandle TimerH;
	Timer.BindUFunction(this, FName("MakeExplosion"));
	GetWorldTimerManager().SetTimer(TimerH, Timer, 1.9f, false);
}

void ACC_CrystalBall::MakeExplosion()
{
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnInfo.Owner = this;
	SpawnInfo.Instigator = GetInstigator();

	FVector Location = GetActorLocation();
	FRotator Rotation = GetActorRotation();

	ACC_ExplosionDamage* tempActor = Cast<ACC_ExplosionDamage>(GetWorld()->SpawnActor(Explosion, &Location, &Rotation, SpawnInfo));
	Destroy();
}


ACC_CrystalBall::ACC_CrystalBall()
{
	

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->AttachTo(RootComponent);

	static ConstructorHelpers::FObjectFinder<UParticleSystem> Particle(TEXT("/Game/Art/Particles/PS_BallExplosion.PS_BallExplosion"));
	ParticleExplosion = Particle.Object;

	static ConstructorHelpers::FObjectFinder<UParticleSystem> ParticleTwo(TEXT("/Game/Art/Particles/PS_TopazDrop.PS_TopazDrop"));
	LandExplosion = ParticleTwo.Object;
}

void ACC_CrystalBall::BeginPlay()
{
	Super::BeginPlay();

	
}

void ACC_CrystalBall::OnHit(UPrimitiveComponent * HitComponent, AActor * OtherActor, UPrimitiveComponent * OtherComponent, FVector NormalImpulse, const FHitResult & Hit)
{
	//Destroy();
	ACC_Character* Player = Cast<ACC_Character>(OtherActor);
	if (Player)
	{
		Player->ApplyDamage(Damage);
	}
	ACC_GruntCharacter* Me = Cast<ACC_GruntCharacter>(OtherActor);
	if (Me) 
	{

	}
	else 
	{
		TakeGravity();
		FTimerDelegate TimerDel;
		FTimerHandle TimerHandle;
		TimerDel.BindUFunction(this, FName("TikingExplosion"));
		GetWorldTimerManager().SetTimer(TimerHandle, TimerDel, 1.5f, false);
	}
}

void ACC_CrystalBall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
