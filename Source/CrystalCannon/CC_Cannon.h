// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SplineComponent.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "CC_Character.h"
#include "Components/ArrowComponent.h"
#include "Runtime/Engine/Classes/Engine/StaticMesh.h"
#include "CC_Cannon.generated.h"

UCLASS()
class CRYSTALCANNON_API ACC_Cannon : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACC_Cannon(const FObjectInitializer &ObjectInitializer);

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly)
	class USphereComponent* VisionSphere;

	UPROPERTY(EditAnywhere, Category = YourCategory)
	USplineComponent* SplineComponent;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* CannonMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UStaticMeshComponent* CircleMaterial;

	class UMaterialInstanceDynamic* DynamicMatInstance;
	FVector color;

	float CurrentMovementSpeed = 0.f;

	float timer = 0;
	bool ParticleActivated = false;

	//float DistanceAlongSpline = 0.f;

	UPROPERTY(EditAnywhere)
		int PlayersNeeded = 2;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float DefaultMovementSpeed = 1.2f;

	UPROPERTY(EditAnywhere)
		float SphereRadius = 1000.f;

	UPROPERTY(EditAnywhere)
		float DistanceAlongSpline = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool CanMove = true;

	UPROPERTY(EditAnywhere, Category = "Abilities")
		float FiringDistance = 200;

	UPROPERTY(EditAnywhere, Category = "Abilities")
		UArrowComponent* FirePos;

	UPROPERTY(EditAnywhere, Category = "Abilities")
	TSubclassOf<AActor> CannonBall;
	
	UPROPERTY(EditAnywhere, Category = "Abilities")
	TSubclassOf<AActor> FireParticle;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TArray<AActor*> OverlappingActors;

	FVector PositionAlongSpline;
	FRotator RotationAlongSpline;
	TArray<AActor*> MyActor_Children;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void OnBoxBeginOverlap(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

	UFUNCTION()
	void OnBoxEndOverlap(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex);
	
	UFUNCTION(BlueprintCallable)
	void  FireCannon();

	void MoveAlongSpline();

};
