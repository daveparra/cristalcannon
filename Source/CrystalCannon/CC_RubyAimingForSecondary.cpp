// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_RubyAimingForSecondary.h"

// Sets default values
ACC_RubyAimingForSecondary::ACC_RubyAimingForSecondary()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACC_RubyAimingForSecondary::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACC_RubyAimingForSecondary::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

