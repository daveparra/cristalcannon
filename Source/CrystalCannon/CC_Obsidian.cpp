// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CC_Obsidian.h"
#include "CC_Obsidian_Secondary.h"
#include "CC_Enemy.h"
#include "Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemComponent.h"
#include "Components/ArrowComponent.h"
#include "Engine/GameEngine.h"
#include "AKGameplayStatics.h"

ACC_Obsidian::ACC_Obsidian()
	:Super()
{
}

void ACC_Obsidian::BeginPlay()
{
	Super::BeginPlay();
}

void ACC_Obsidian::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, "Obsidian" + FString::FromInt(HoldTimer));
}

void ACC_Obsidian::EnablePrimary()
{
	if (!this->isAlive) return;
	isShooting = true;
	if (this->CanFirePrimary)
	{
		SpawnPrimary();
		Super::EnablePrimary();
	}
}

void ACC_Obsidian::SpawnPrimary()
{
	Super::SpawnPrimary();
	//GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, " World NULL @ Obsidian.cpp @ CastOverlapShphere");
	if (Primary && Primary->GetName() != "None")
	{
		UAkGameplayStatics::PostEvent(atksound, Primary);
	}
	isShooting = true;
}

void ACC_Obsidian::EnableSecondary()
{
	if (!this->isAlive) return;
	if (CanFireSecondary)
	{
		this->AimDecal->SetVisibility(true);
	}
}

void ACC_Obsidian::DisableSecondary()
{
	if (CanFireSecondary)
	{
		isCharge = true;
		SpawnSecondary();
		this->AimDecal->SetVisibility(false);
		Super::EnableSecondary();
	}
	Super::DisableSecondary();
	isCharge = false;
} 

//void ACC_Obsidian::CastOverlapSphere(FVector Location, int32 ChainTargets)
//{
//
//	TArray<TEnumAsByte<EObjectTypeQuery>> Types;
//	TArray<AActor*> ActorsToIgnore;// STORES ACTORS TO BE IGNORED BY SPHERE CAST
//	TArray<AActor*> OutActors;//STORES THE ACTORS IN THE SPHERE OVERLAP
//	TArray<AActor*> FloorArray;//STORES FLOOR ACTOR
//
//	UWorld* World = this->GetWorld();
//
//	if (World == nullptr)
//	{
//	}
//	else
//	{
//		UGameplayStatics::GetAllActorsWithTag(World, "Floor", FloorArray);//GETS ALL INSTANCES WITH FLOOR TAG (WILL NEED SIMILAR STATEMENTS FOR OTHER ENVIRONMENT MESHES)
//		
//		ActorsToIgnore.Add(this);
//
//		ActorsToIgnore.Add(LightningTarget);
//
//		if (FloorArray.Num() < 1)
//		{
//			GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, "Floor not tagged as floor");
//		}
//		else
//		{
//			ActorsToIgnore.Add(FloorArray[0]);
//		}
//		bool isOverlap = UKismetSystemLibrary::SphereOverlapActors(World, Location, ChainRadius, Types, ACC_Enemy::StaticClass(), ActorsToIgnore, OutActors);
//		//DrawDebugSphere(World, Location, ChainRadius, 26, FColor(181, 0, 0), true, -1);
//	}
//
//	OutActors = SortArray(OutActors);
//
//	for (int j = 0; j < OutActors.Num(); j++)
//	{
//		if (j > ChainTargets)
//		{
//			break;
//		}
//
//		//DrawDebugLine(World, Target->GetActorLocation(), OutActors[i]->GetActorLocation(), FColor(225, 45, 10), true);
//			if (LightningTarget == nullptr)
//			{
//				GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, "Target NULL @ Obsidian.cpp @ CastOverlapShphere");
//			}
//			else
//			{
//				ShootParticleBeam(LightningTarget->GetActorLocation(), OutActors[j]->GetActorLocation());
//				GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Blue, LightningTarget->GetActorLocation().ToString());
//				ACC_Unit* Unit = Cast<ACC_Unit>(OutActors[j]);
//				Unit->ApplyDamage(LightningDamage*0.2f);
//			}
//	}
//
//
//	//ACC_Unit* Unit = Cast<ACC_Unit>(OutActors[0]);
//	//Unit->ApplyDamage(5);
//
//	//ShootParticleBeam(Location, OutActors[1]->GetActorLocation());
//	//Unit = Cast<ACC_Unit>(OutActors[1]);
//	//Unit->ApplyDamage(5);
//}

//TArray<AActor*> ACC_Obsidian::SortArray(TArray<AActor*> Actors)
//{
//	float Size = Actors.Num();//Size of array
//	float Dist1, Dist2;//Variables to hold distance between actors
//	FVector StartLoc = this->LightningTarget->GetActorLocation();//Location of the overlapsphere 
//	AActor* Temp;//Temporary Actor pointer, used for swapping
//
//	for (int i = 1; i < Size; i++)
//	{
//		for (int j = 0; j < (Size - 1); j++)
//		{
//			Dist1 = FVector::Dist2D(StartLoc, Actors[j]->GetActorLocation());
//			Dist2 = FVector::Dist2D(StartLoc, Actors[j + 1]->GetActorLocation());
//
//			if (Dist1 > Dist2)
//			{
//				Temp = Actors[j];
//				Actors[j] = Actors[j + 1];
//				Actors[j + 1] = Temp;
//			}
//		}
//	}
//	return Actors;
//}

void ACC_Obsidian::SpawnSecondary()
{
	Super::SpawnSecondary();
	UAkGameplayStatics::PostEvent(SecondSound, Secondary);
}

//void ACC_Obsidian::ShootRaycast()
//{
//	FHitResult* HitResult = new FHitResult();
//	FVector StartTrace = this->GetActorLocation() + this->Shoot->GetForwardVector() * 50;
//	FVector EndTrace = StartTrace + (this->Shoot->GetForwardVector()*LightningRange);
//	FCollisionQueryParams* CQP = new FCollisionQueryParams();
//
//	UWorld* World = this->GetWorld();
//
//	if (World == nullptr)
//	{
//		GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, " World NULL @ Obsidian.cpp @ ShootRaycast");
//	}
//	else
//	{
//		//Raycast
//		World->LineTraceSingleByChannel(*HitResult, StartTrace, EndTrace, ECC_Visibility, *CQP);
//
//		//Debug
//		//DrawDebugLine(World, StartTrace, EndTrace, FColor(225, 0, 0), true);
//	}
//
//	if (HitResult->GetActor() != NULL && HitResult->GetActor()->IsA(ACC_Enemy::StaticClass()))
//	{
//		LightningTarget = HitResult->GetActor();
//		ACC_Unit* Unit = Cast<ACC_Unit>(LightningTarget);
//		ShootParticleBeam(StartTrace, LightningTarget->GetActorLocation());
//		Unit->ApplyDamage(LightningDamage);
//		GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Blue, LightningTarget->GetName());
//	}
//	else
//	{
//		LightningTarget = nullptr;
//		ShootParticleBeam(StartTrace, EndTrace);
//		//GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, " HitResult NULL @ Obsidian.cpp @ ShootRaycast");
//	}
//}
//
//void ACC_Obsidian::ShootParticleBeam(FVector Point1, FVector Point2)
//{
//	//GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Blue, Point1.ToString());
//	//GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Blue, Point2.ToString());
//
//	BeamEmitterComponent = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), BeamEmitterSystem, Point1, FRotator::ZeroRotator);
//	BeamEmitterComponent->SetBeamSourcePoint(0, Point1, 0);
//	BeamEmitterComponent->SetBeamTargetPoint(0, Point2, 0);
//}

//void ACC_Obsidian::PShortCooldownEnd()
//{
//	this->CanFirePrimary = true;
//	isShooting = false;
//	//GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, "Primary short CD ended", 1);
//	//If at the end of the short cooldown & the button is continued to be held, It starts the longcooldown
//	if (isButtonPressed && CanFirePrimary)
//	{
//		GetWorldTimerManager().SetTimer(CallbackHandlerShort, this, &ACC_Character::PShortCooldownEnd, PrimaryShortCooldown, false);
//	}
//
//}
