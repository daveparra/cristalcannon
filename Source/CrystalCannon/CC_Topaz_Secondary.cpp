// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_Topaz_Secondary.h"
#include "CC_Enemy.h"
#include "CC_MobController.h"
#include "CC_Spawner.h"
#include "CC_SplitterCharacter.h"

// Sets default values
ACC_Topaz_Secondary::ACC_Topaz_Secondary()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BoxCollisionVertical = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollisionVertical"));
	BoxCollisionHorizontal = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollisionHorizontal"));

	RootComponent = BoxCollisionVertical;
	BoxCollisionHorizontal->AttachTo(RootComponent);
	//BoxCollisionComponent->UpdateBodySetup();
	BoxCollisionVertical->SetCollisionProfileName("OverlapAll");
	BoxCollisionHorizontal->SetCollisionProfileName("OverlapAll");

	BoxCollisionVertical->bAbsoluteLocation = false;
	BoxCollisionHorizontal->bAbsoluteLocation = false;

	BoxCollisionVertical->OnComponentBeginOverlap.AddDynamic(this, &ACC_Topaz_Secondary::OnOverlapBegin);
	BoxCollisionHorizontal->OnComponentBeginOverlap.AddDynamic(this, &ACC_Topaz_Secondary::OnOverlapBegin);

}

void ACC_Topaz_Secondary::OnOverlapBegin(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (OtherActor->IsA(ACC_Enemy::StaticClass()))
	{
		ACC_Enemy* Current = Cast<ACC_Enemy>(OtherActor);
		if (Current)
		{
			if (this->GetOwner() != nullptr)
			{
				Current->LastHit = this->GetOwner();
			}

			Current->ApplyDamage(Damage);
		}
	}

	else if (OtherActor->IsA(ACC_Spawner::StaticClass()))
	{
		ACC_Spawner* Current = Cast<ACC_Spawner>(OtherActor);
		Current->DamageSpawner();
	}
}


