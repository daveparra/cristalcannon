// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_GruntDie.h"
#include "CC_GruntCharacter.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Components/CapsuleComponent.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstanceDynamic.h"

void UCC_GruntDie::Notify(USkeletalMeshComponent * MeshComp, UAnimSequenceBase * Animation)
{
	Super::Notify(MeshComp, Animation);
	AActor* AnimationOwner = MeshComp->GetOwner();
	if (ACC_GruntCharacter* Character = Cast<ACC_GruntCharacter>(AnimationOwner))
	{
		UCapsuleComponent* Capsule = Character->GetCapsuleComponent();
		Capsule->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		Character->Change = true;
		Character->ApplyShakeOnDeath();
	}
}
