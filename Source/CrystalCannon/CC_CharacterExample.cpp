// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_CharacterExample.h"
#include "Engine.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Runtime/Engine/Classes/Kismet/KismetMathLibrary.h"
#include "Components/ArrowComponent.h"

// Sets default values
ACC_CharacterExample::ACC_CharacterExample()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate


	
	
	
	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
}


// Called when the game starts or when spawned
void ACC_CharacterExample::BeginPlay()
{
	Super::BeginPlay();
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, TEXT("We are using FPSCharacter!"));
	}
	USkeletalMeshComponent* myMesh = GetMesh();
	FirstROt = myMesh->GetComponentRotation();
	FirstTitty = Cast<UArrowComponent>(GetDefaultSubobjectByName(TEXT("Arrow1")));
	SecondTitty = Cast<UArrowComponent>(GetDefaultSubobjectByName(TEXT("Arrow2")));
	ThirdTitty = Cast<UArrowComponent>(GetDefaultSubobjectByName(TEXT("Arrow3")));

	
}

// Called every frame
void ACC_CharacterExample::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (stop1 && stop2) {
		IsWalking = false;
	}
	USkeletalMeshComponent* myMesh = GetMesh();
	if ((XAxis != 0.0f) || (YAxis != 0.0f)) {
		Flag = true;
		bUseControllerRotationYaw = true;
		FVector Target = FVector(XAxis, YAxis, 0.0f);
		//FVector STart = GetActorLocation() + Target;
		FRotator NewRot = Target.Rotation();
		//UKismetMathLibrary::FindLookAtRotation(STart, Target);

		FRotator oldRotation = myMesh->GetComponentRotation();

		myMesh->SetWorldRotation(FMath::Lerp(oldRotation, NewRot, 0.1f));
		//going left with each rotation
		if (XAxis < 0) {
			if (LYAxis > 0) {
				IsRight = true;
				IsLeft = false;
				IsWalking = false;
				IsBack = false;
			}
			else if (LYAxis < 0) {
				IsRight = false;
				IsLeft = true;
				IsWalking = false;
				IsBack = false;
			}
			else if (LXAxis < 0) {
				IsRight = false;
				IsLeft = false;
				IsWalking = true;
				IsBack = false;
			}
			else if (LXAxis > 0) {
				IsRight = false;
				IsLeft = false;
				IsWalking = false;
				IsBack = true;
			}
			else {
				IsRight = false;
				IsLeft = false;
				IsWalking = false;
				IsBack = false;
			}
			//going rigth with each rotation
		}
		else if (XAxis > 0) {
			if (LYAxis > 0) {
				IsRight = false;
				IsLeft = true;
				IsWalking = false;
				IsBack = false;
			}
			else if (LYAxis < 0) {
				IsRight = true;
				IsLeft = false;
				IsWalking = false;
				IsBack = false;
			}
			else if (LXAxis < 0) {
				IsRight = false;
				IsLeft = false;
				IsWalking = false;
				IsBack = true;
			}
			else if (LXAxis > 0) {
				IsRight = false;
				IsLeft = false;
				IsWalking = true;
				IsBack = false;
			}
			else {
				IsRight = false;
				IsLeft = false;
				IsWalking = false;
				IsBack = false;
			}
		}
		//going up with each rotation
		else if (YAxis > 0) {
			if (LYAxis > 0) {
				IsRight = false;
				IsLeft = false;
				IsWalking = false;
				IsBack = true;
			}
			else if (LYAxis < 0) {
				IsRight = false;
				IsLeft = false;
				IsWalking = true;
				IsBack = false;
			}
			else if (LXAxis < 0) {
				IsRight = true;
				IsLeft = false;
				IsWalking = false;
				IsBack = false;
			}
			else if (LXAxis > 0) {
				IsRight = false;
				IsLeft = true;
				IsWalking = false;
				IsBack = false;
			}
			else {
				IsRight = false;
				IsLeft = false;
				IsWalking = false;
				IsBack = false;
			}
		}
		//going down with each rotation
		else if (YAxis < 0) {
			if (LYAxis > 0) {
				IsRight = false;
				IsLeft = false;
				IsWalking = true;
				IsBack = false;
			}
			else if (LYAxis < 0) {
				IsRight = false;
				IsLeft = false;
				IsWalking = false;
				IsBack = true;
			}
			else if (LXAxis < 0) {
				IsRight = false;
				IsLeft = true;
				IsWalking = false;
				IsBack = false;
			}
			else if (LXAxis > 0) {
				IsRight = true;
				IsLeft = false;
				IsWalking = false;
				IsBack = false;
			}
			else {
				IsRight = false;
				IsLeft = false;
				IsWalking = false;
				IsBack = false;
			}
		}
	}
	else {
		bUseControllerRotationYaw = false;
		if (Flag) {
			IsRight = false;
			IsLeft = false;
			IsWalking = false;
			IsBack = false;
			Flag = false;
			myMesh->SetWorldRotation(FirstROt);
		}
	}

}

// Called to bind functionality to input
void ACC_CharacterExample::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	check(PlayerInputComponent);
	PlayerInputComponent->BindAxis("MoveForward", this, &ACC_CharacterExample::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ACC_CharacterExample::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("FaceEast", this, &ACC_CharacterExample::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("FaceNorth", this, &ACC_CharacterExample::LookUpAtRate);
}

void ACC_CharacterExample::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
		XAxis = Rate;	
}

void ACC_CharacterExample::LookUpAtRate(float Rate)
{
	YAxis = Rate;

}


void ACC_CharacterExample::MoveForward(float Value)
{
	LYAxis = Value;
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
		IsWalking = true;
		stop1 = false;
		LYAxis = Value;
	}
	else {
		stop1 = true;
	}
}

void ACC_CharacterExample::MoveRight(float Value)
{
	LXAxis = Value;
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
		IsWalking = true;
		stop2 = false;
	}
	else {
		stop2 = true;

	}
}
