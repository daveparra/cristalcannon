// Fill out your copyright notice in the Description page of Project Settings.

#include "CC_RubyExplosion.h"
#include "Components/SphereComponent.h"
#include "CC_Ruby.h"
#include "CC_Enemy.h"
#include "CC_Spawner.h"

// Sets default values
ACC_RubyExplosion::ACC_RubyExplosion()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	VisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("VisionSphere"));
	VisionSphere->AttachTo(RootComponent);
	VisionSphere->SetSphereRadius(VisionRadius);
	VisionSphere->OnComponentBeginOverlap.AddDynamic(this, &ACC_RubyExplosion::OnSphereBeginOverlap);
}

void ACC_RubyExplosion::OnSphereBeginOverlap(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	ACC_Enemy* Enemy = Cast<ACC_Enemy>(OtherActor);
	if (Enemy)
	{
		if (this->GetOwner() != nullptr)
		{
			Enemy->LastHit = this->GetOwner();
		}
		Enemy->ApplyDamage(DamageExplosion);
		Enemy->killedByExplosion = true;
	}

	ACC_Spawner* Spawner = Cast<ACC_Spawner>(OtherActor);
	if (Spawner)
	{
		Spawner->DamageSpawner();
	}
}

// Called when the game starts or when spawned
void ACC_RubyExplosion::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ACC_RubyExplosion::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Timer -= DeltaTime;
	if (Timer < 0.f)
	{
		Destroy();
	}
}

