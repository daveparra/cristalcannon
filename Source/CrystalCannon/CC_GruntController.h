// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "CC_GruntController.generated.h"

/**
 * 
 */
class UAISenseConfig_Sight;
class ACC_CrystalBall;

UCLASS()
class CRYSTALCANNON_API ACC_GruntController : public AAIController
{
	GENERATED_BODY()
	
public:
	ACC_GruntController();
	virtual void Tick(float DeltaSeconds) override;
	void GetRandomLocation();
	FVector Result;
	float ShootTimer = 0.0f;
	float TimerMove = 1.5f;
	FVector EnemyLocation;
	UParticleSystem* ParticleFX1;
	UPROPERTY(EditAnywhere, Category = Debug)
		float CoolDown = 3.0f;

	UPROPERTY(EditAnywhere, Category = Debug)
		float MR = 1.5f;

	UPROPERTY(EditAnywhere, Category = Debug)
		float MovementRadius = 1000.f;

	UFUNCTION()
		void OnPawnDetected(const TArray<AActor*>& DetectedPawns);

	UPROPERTY(EditAnywhere, Category = AI)
		float AISightRadius = 2000.f;

	UPROPERTY(EditAnywhere, Category = AI)
		float AISightAge = 2.5f;

	UPROPERTY(EditAnywhere, Category = AI)
		float AILoseSightRadius = AISightRadius + 100.f;

	UPROPERTY(EditAnywhere, Category = AI)
		float AIFieldOfView = 180.f;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
		UAISenseConfig_Sight* SightConfig;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
		bool bIsPlayerDetected;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
		float DistancetoPlayer = 0.0f;

	UPROPERTY(EditAnywhere, Category = Abilities)
		TSubclassOf<ACC_CrystalBall> CrystalToUse;

	UFUNCTION()
		void SpawnCrystal(FVector SocketLocation);

protected:

	virtual void BeginPlay() override;

	virtual void Possess(APawn* Pawn) override;

	virtual FRotator GetControlRotation() const override;
};
