// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CC_Enemy.h"
#include "CC_MobEnemie.generated.h"

class ACC_WayPoint;
class UArrowComponent;
class UAkAudioEvent;

UCLASS()
class CRYSTALCANNON_API ACC_MobEnemie : public ACC_Enemy
{
	GENERATED_BODY()

		UPROPERTY(Category = AI, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class USphereComponent* VisionSphere;
public:
	// Sets default values for this character's properties
	ACC_MobEnemie();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Abilities")
		UAkAudioEvent* MobSound;

	FVector* EnemieLocation;
	FVector NewDirection;
	FRotator NewRotation;
	FVector Deflection;
	float Timer = 0;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
		void PlayDeadSound();

public:	

	UFUNCTION(BlueprintCallable)
		void PlayAttackSound();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UArrowComponent* Left;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UArrowComponent* Right;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UArrowComponent* Back;

	UPROPERTY(EditAnywhere)
		float TimertoRandom = 3;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<ACC_MobEnemie> ClassFilter;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector AlignmentComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector CohesionComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector SeparationComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector NegativeStimuliComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector PositiveStimuliComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float NegativeStimuliMaxFactor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float PositiveStimuliMaxFactor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<AActor*> Neighbourhood;

	UPROPERTY(EditAnywhere, Category = AI)
		TSubclassOf<ACC_Avoid> SpawnAvoid;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<AActor *> Others;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float InertiaWeigh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float AgentPhysicalRadius;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool isRunning;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool isAttacking;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
		FVector NewMoveVector;
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = UnitDamage)
		float Damage = 20.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = NotAgressive)
	bool NonAgressive = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
		float AlignmentWeight;

	/* The weight of the Cohesion vector component */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
		float CohesionWeight;

	/* The weight of the Separation vector component */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
		float SeparationWeight;

	/* The base movement speed for the Agents */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
		float BaseMovementSpeed;

	/* The maximum movement speed the Agents can have */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
		float MaxMovementSpeed;

	/* The maximum radius at which the Agent can detect other Agents */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
		float VisionRadius;

	/* This event is called every tick, before applying the calculated move vector to move the Agent */

	UFUNCTION()
		void RandomMove(int Place);

	UFUNCTION()
		void CalculateNewMoveVector();

	UFUNCTION()
		void ResetComponents();

	UFUNCTION()
		void BeforeCalculate();

	UFUNCTION()
		void GetAvoidOrEnemie();

	UFUNCTION()
		void GetAvoid(class ACC_Avoid* Avoid);

	UFUNCTION()
		void GetEnemy(class ACC_Character* Enemy);

protected:
	/* The movement vector (in local) this agent should move this tick. */


	/* The movement vector (in local) this agent had last tick. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = AI)
		FVector CurrentMoveVector;

};
