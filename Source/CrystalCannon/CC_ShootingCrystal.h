// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CC_Projectile.h"
#include "CC_ShootingCrystal.generated.h"

/**
 * 
 */
class UStaticMeshComponent;
class UParticleSystem;
class AParticleSpawner;

UCLASS()
class CRYSTALCANNON_API ACC_ShootingCrystal : public ACC_Projectile
{
	GENERATED_BODY()
	
public:

	// Sets default values for this actor's properties
	ACC_ShootingCrystal();

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* MeshComponent;

	UParticleSystem* ParticleFX1;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit) override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
