// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CC_Camera.generated.h"

class UBoxComponent;
class UParticleSystem;
class UParticleSystemComponent;
class ACC_Character;
UCLASS()
class CRYSTALCANNON_API ACC_Camera : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ACC_Camera();

	bool changeObsColor = false;
	bool changeJadeColor = false;
	bool changeTopazColor = false;
	bool changeRubyColor = false;

	ACC_Character* ObsCharac;
	ACC_Character* JadeCharac;
	ACC_Character* TopazCharac;
	ACC_Character* RubyCharac;

	float lerpObsValue = 1.0f;
	class UMaterialInstanceDynamic* DynamicObsMatInstance;
	float lerpTopazValue = 1.0f;
	class UMaterialInstanceDynamic* DynamicTopazMatInstance;
	float lerpRubyValue = 1.0f;
	class UMaterialInstanceDynamic* DynamicRubyMatInstance;
	float lerpJadeValue = 1.0f;
	class UMaterialInstanceDynamic* DynamicJadeMatInstance;

	UPROPERTY(EditAnywhere)
		class USceneComponent* SceneRoot;
	/** Camera boom positioning the camera behind the character */
	UPROPERTY(EditAnywhere)
		class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(EditAnywhere)
		class UCameraComponent* FollowCamera;

	//Function to grab all the player starts and set them into the 4 differents characters
	UFUNCTION()
		void ClassSetUp();
	//Function that is gonna get called every frame to modify the position of the camera depending on the characters location
	UFUNCTION()
		void UpdateCamera();
	UFUNCTION()
		void SetArmLenght();
	UFUNCTION()
	void ActivateController(APlayerController* PC, AActor* Character);
	//Get the distance between two players on the world
	UFUNCTION()
		float CalculateDistance(APawn* Player1, APawn* Player2);
	UFUNCTION()
		FVector GetLocation(APawn* Player);
	//Move camera to location the was calculated by the 4 players position
	UFUNCTION()
		void SetCameraPos();

	UFUNCTION()
		void ChangeCollisionParam();

	//Move the player to the center if he get out of the camera sight
	UFUNCTION()
		void MoveActorToCenter(AActor* ActorToMove, AActor* Nearactor, APlayerController* PC);

	UFUNCTION()
		void OnCompHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	UFUNCTION()
		void OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void OnBoxEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	bool PlayerMoved;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	//variables for update the players setup
	TArray<APawn*> PlayersReference;
	TArray<AActor*> PlayerControllers;
	TArray<AActor*> Cannons;
	TArray<float> Distances;

	FVector Result;
	FVector TargetPos;
	FVector PreviousPos;
	
	APawn* P1;
	APawn* P2;
	APawn* P3;
	APawn* P4;

	UParticleSystem* ParticleFX1;


	UPROPERTY() 
	UParticleSystemComponent* AirParticle;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UBoxComponent* BoxComponentBack;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<AActor> Shield;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UBoxComponent* BoxComponentFront;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UBoxComponent* BoxComponentLeft;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UBoxComponent* BoxComponentRight;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UBoxComponent* BoxComponentReset;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UBoxComponent* BoxTeleport;

	//variables for the level designer to change values
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Heitor)
		float minLength;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Heitor)
		float LengthForCastle;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Heitor)
		float maxLength;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Heitor)
		bool SinglePlayer;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Heitor)
		float inteSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Heitor)
		bool Debug = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Heitor)
		bool Arena = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Heitor)
		bool CharacterOutofBounds = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Heitor)
		bool FollowCannon = false;
	
	UPROPERTY(BlueprintReadWrite)
		bool TornadoActive;
};
