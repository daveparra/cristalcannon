// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CC_Projectile.h"
#include "CC_Ruby_Primary.generated.h"

/**
 * 
 */
UCLASS()
class CRYSTALCANNON_API ACC_Ruby_Primary : public ACC_Projectile
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, Category = "Ablities")
		float Duration = 3.f;

	UPROPERTY(EditAnywhere, Category = "Ablities")
		float SplashDamage;
		
	UPROPERTY(EditAnywhere, Category = "Ablities")
		float SplashRadius;

	UPROPERTY(EditAnywhere, Category = Abilities)
		TSubclassOf<class ACC_ExplosionDamage>  Explosion;

	//Overridden OnHit with custom functionality
	virtual void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit) override;

	void Detonate();

};
