// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "CC_MobController.generated.h"

/**
 * 
 */
class UPawnSensingComponent;
class ACC_Character;

UCLASS()
class CRYSTALCANNON_API ACC_MobController : public AAIController
{
	GENERATED_BODY()

protected:
	

public:
	ACC_MobController();

	APawn* MyPawn = nullptr;
	bool bNotGoing = false;
	bool Stop = true;
	FVector Result;
	float TimerMove = 1.5f;
	float TimerDamge = 2.0f;
	void GetRandomLocation();
	float TittyNumber = 0.0f;
	TArray<AActor*> EnemiesAround;

	virtual void BeginPlay() override;

	virtual void Possess(APawn* Pawn) override;

	virtual void Tick(float DeltaSeconds) override;

	virtual FRotator GetControlRotation() const override;

	void GoingToEnemie();

	UPROPERTY(EditAnywhere, Category = AI)
		float MR = 1.2f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Awareness)
		float radius = 0.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Awareness)
		UPawnSensingComponent* PawnSensor;

	UFUNCTION()
		void OnHearNoise(APawn *OtherActor, const FVector &Location, float Volume);

	UFUNCTION()
		void OnSeePawn(APawn *OtherPawn);

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
		bool bIsPlayerDetected = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
		float DistanceToPlayer = 20.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
	float DamageTimer = 2.0f;

	UPROPERTY(EditAnywhere, Category = Abilities)
		float MovementRadius = 1000.f;
	
	UPROPERTY(EditAnywhere, Category = Abilities)
		float AISightRadius = 500.0f;

};
