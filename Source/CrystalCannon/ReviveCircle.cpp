// Fill out your copyright notice in the Description page of Project Settings.

#include "ReviveCircle.h"
#include "Engine/GameEngine.h"


// Sets default values
AReviveCircle::AReviveCircle(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere"));
	RootComponent = SphereComponent;

	SphereComponent->OnComponentBeginOverlap.AddDynamic(this, &AReviveCircle::OnOverlapBegin);
	SphereComponent->OnComponentEndOverlap.AddDynamic(this, &AReviveCircle::OnOverlapEnd);
}

void AReviveCircle::OnOverlapBegin(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (OtherActor->IsA(ACC_Character::StaticClass()))
	{
		if (this->GetOwner() && OtherActor != this->GetOwner())
		{
			ACC_Character* Chara = Cast<ACC_Character>(this->GetOwner());

			if (Chara)
			{
				if (Chara->inReviveRange)
				{
					return;
				}
				else
				{
					Chara->inReviveRange = true;
				
					ACC_Character* Last = Cast<ACC_Character>(OtherActor);

					LastChara = Last;
					Chara->Reviver = Last;
					GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, LastChara->GetName());
				}
			}	
		}
	}
}

void AReviveCircle::OnOverlapEnd(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor->IsA(ACC_Character::StaticClass()))
	{
		ACC_Character* Last = Cast<ACC_Character>(OtherActor);
		
		if (Last == LastChara)
		{
			if (IsValid(this->GetOwner()))
			{
				ACC_Character* Chara = Cast<ACC_Character>(this->GetOwner());
		
				if (Chara)
				{
					Chara->inReviveRange = false;
				}
			}
		}
	}
}

// Called when the game starts or when spawned
void AReviveCircle::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AReviveCircle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

